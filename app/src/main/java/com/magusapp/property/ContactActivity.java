package com.magusapp.property;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.costum.android.widget.LoadMoreListView;
import com.magusapp.property.utility.Alert;
import com.magusapp.property.utility.ConnectionDetector;
import com.magusapp.property.utility.DecodeEntity;
import com.magusapp.property.utility.ServiceHandler;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ContactActivity extends Activity {

	private static final String TAG_type = "type";

	JSONObject object;
	ProgressDialog pDialog;

	LoadMoreListView lv;

	ArrayList<MainDetails> details = new ArrayList<MainDetails>();

	JSONArray jarray;

	String jsonStr;

	TextView title, location, mail, call, call2, url, fb, wc;
	RelativeLayout rl1, rl2, rl3, rl4, rl5, rl6, rl7;

	String name, add, longitude, latitude, web, email, company_phone, mobile_phone, facebook, website, wechat;

	Boolean isInternetPresent;
	ConnectionDetector connection_detector;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contact);

		connection_detector = new ConnectionDetector(ContactActivity.this);
		isInternetPresent = connection_detector.isConnectingToInternet();

		title = (TextView) findViewById(R.id.Name);
		location = (TextView) findViewById(R.id.locationT);
		mail = (TextView) findViewById(R.id.mailT);
		call = (TextView) findViewById(R.id.phoneT);
		call2 = (TextView) findViewById(R.id.phoneT2);
		url = (TextView) findViewById(R.id.websiteT);
		fb = (TextView) findViewById(R.id.facebookT);
		wc = (TextView) findViewById(R.id.wechatT);

		rl1 = (RelativeLayout) findViewById(R.id.layout1);
		rl2 = (RelativeLayout) findViewById(R.id.layout2);
		rl3 = (RelativeLayout) findViewById(R.id.layout3);
		rl4 = (RelativeLayout) findViewById(R.id.layout4);
		rl5 = (RelativeLayout) findViewById(R.id.layout5);
		rl6 = (RelativeLayout) findViewById(R.id.layout6);
		rl7 = (RelativeLayout) findViewById(R.id.layout7);

		if (isInternetPresent) {
			new GetContent().execute();
		} else {
			Alert.ShowAlertMessage("No internet connection.", "Information", "Finish", null, ContactActivity.this,
					ContactActivity.this);
		}

		onClick();
	}

	private class GetContent extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(ContactActivity.this);
			pDialog.setMessage("Loading...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@SuppressWarnings("deprecation")
		@Override
		protected Void doInBackground(Void... arg0) {

			ServiceHandler sh = new ServiceHandler();
			HashMap<String, String> params = new HashMap<>();
			params.put(TAG_type, "setting");
			jsonStr = sh.makeServiceCall("GET", params);

			if (jsonStr != null || !jsonStr.equals("")) {
				try {

					jarray = new JSONArray(jsonStr);

					for (int k = 0; k < jarray.length(); k++) {

						JSONObject c2 = jarray.getJSONObject(k);

						name = DecodeEntity.decodeEntities(c2.getString("CompanyName"));
						add = DecodeEntity.decodeEntities(c2.getString("Address"));
						company_phone = DecodeEntity.decodeEntities(c2.getString("CompanyPhone"));
						mobile_phone = DecodeEntity.decodeEntities(c2.getString("MobilePhone"));
						email = DecodeEntity.decodeEntities(c2.getString("Email"));
						web = DecodeEntity.decodeEntities(c2.getString("Website"));
						facebook = DecodeEntity.decodeEntities(c2.getString("Fblink"));
						wechat = c2.getString("Module14");
						latitude = c2.getString("Longitude");
						longitude = c2.getString("Latitude");

						if (!web.startsWith("http://") && !web.startsWith("https://")) {
							website = "http://" + web;
						} else {
							website = web;
						}

						if (!facebook.startsWith("http://") && !facebook.startsWith("https://")) {
							facebook = "http://" + facebook;
						}

					}

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (pDialog.isShowing())
				pDialog.dismiss();

			if (jsonStr == null || jsonStr.equals("")) {

				Alert.alertDialog("Information", "No content for this page. We are working on it.",
						ContactActivity.this);

			} else {
				title.setText(name);
				location.setText(add);
				call.setText(company_phone);
				call2.setText(mobile_phone);
				mail.setText(email);
				url.setText(web);
			}
		}
	}

	public void onClick() {
		rl1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(ContactActivity.this, MapActivity.class);
				i.putExtra("longitude", longitude);
				i.putExtra("latitude", latitude);
				i.putExtra("address", add);
				i.putExtra("name", name);
				startActivity(i);
			}
		});

		rl2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i11 = new Intent(Intent.ACTION_DIAL);
				i11.setData(Uri.parse("tel:" + company_phone));
				startActivity(i11);

			}
		});

		rl3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i11 = new Intent(Intent.ACTION_DIAL);
				i11.setData(Uri.parse("tel:" + mobile_phone));
				startActivity(i11);
			}
		});

		rl4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i2 = new Intent(Intent.ACTION_SEND);
				i2.setType("message/rfc822");
				i2.putExtra(Intent.EXTRA_EMAIL, new String[] { email });

				startActivity(Intent.createChooser(i2, "Send Email"));

			}
		});

		rl5.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i3 = new Intent(Intent.ACTION_VIEW, Uri.parse(website));
				startActivity(i3);
			}
		});

		rl6.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i3 = new Intent(Intent.ACTION_VIEW, Uri.parse(facebook));
				startActivity(i3);
			}
		});
	}

}
