package com.magusapp.property;

import java.util.ArrayList;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class GalleryAdapter extends BaseAdapter {

	ArrayList<MainDetails> details;
	Context context;
	ImageLoader imageLoader = ImageLoader.getInstance();
	DisplayImageOptions options;
	String jsonStr;

	public GalleryAdapter(Context activity, ArrayList<MainDetails> det, String json) {

		details = det;
		context = activity;
		jsonStr = json;
		options = new DisplayImageOptions.Builder().showImageOnFail(R.drawable.no_image)
				.showImageForEmptyUri(R.drawable.no_image).cacheInMemory(false).cacheOnDisk(true)
				.considerExifParams(true).bitmapConfig(Bitmap.Config.RGB_565).build();

		if (!imageLoader.isInited()) {
			imageLoader.init(ImageLoaderConfiguration.createDefault(context));
		}

	}

	@Override
	public int getCount() {
		return details.size();
	}

	@Override
	public Object getItem(int position) {
		return details.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View view = convertView;
		final MainDetails content = details.get(position);

		if (view == null) {
			LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = vi.inflate(R.layout.grid_image, null);
		}

		ImageView img = (ImageView) view.findViewById(R.id.icon);
		TextView tv = (TextView) view.findViewById(R.id.title);
		TextView description = (TextView) view.findViewById(R.id.description);

		imageLoader.displayImage(content.image, img, options);
		tv.setText(content.name);
		description.setText(content.details);

		view.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent = new Intent(context, GalleryDetailsActivity.class);
				intent.putExtra("title", content.name);
				intent.putExtra("detail", content.details);
				intent.putExtra("image", content.image);

				((Activity) context).startActivity(intent);
			}
		});

		return view;
	}

}
