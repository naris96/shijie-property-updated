package com.magusapp.property;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.magusapp.property.utility.Alert;
import com.magusapp.property.utility.ConnectionDetector;
import com.magusapp.property.utility.DatabaseAdapter;
import com.magusapp.property.utility.DecodeEntity;
import com.magusapp.property.utility.ServiceHandler;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.PageIndicator;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class LandDetailsActivity extends Activity {

	private static final String TAG_id = "id";
	private static final String TAG_type = "type";

	String zoning, contact, email, agent, heading, description, road, mode, newmode, id = "", price, location, project,
			landed_area, buildup_area, tenure, date, property_type;
	String json = "", json2 = "", staff_pic, newdate;

	TextView agent_t, contact_t, email_t, heading_t, description_t, road_t, location_t, project_t, price_t,
			landed_area_t, buildup_area_t, tenure_t, date_t, property_type_t, zoning_t;

	ImageButton choose;
	ImageView bk;
	TextView bookmark;
	ImageView img;
	ImageView staff;

	JSONObject object;
	JSONArray jarray;

	boolean value;

	DatabaseAdapter db;

	ProgressDialog pDialog;

	ImageLoader imageLoader = ImageLoader.getInstance();
	DisplayImageOptions options;
	ViewPager pager;
	PageIndicator mIndicator;
	CirclePageIndicator indicator;
	ArrayList<MainDetails> details = new ArrayList<MainDetails>();

	Boolean isInternetPresent;
	ConnectionDetector connection_detector;

	int selected;

	DecimalFormat df;

	File file;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_land_details);

		df = new DecimalFormat("#,###,##0.00");

		options = new DisplayImageOptions.Builder().cacheInMemory(false).cacheOnDisk(true).considerExifParams(true)
				.showImageForEmptyUri(R.drawable.no_image).bitmapConfig(Bitmap.Config.RGB_565).build();

		if (!imageLoader.isInited()) {
			imageLoader.init(ImageLoaderConfiguration.createDefault(this));
		}

		db = new DatabaseAdapter(this);

		Intent i = getIntent();
		mode = i.getStringExtra("mode");
		json = i.getStringExtra("object");

		if (mode.startsWith("S")) {
			newmode = "Sales";
		} else {
			newmode = "Rent";
		}

		staff = (ImageView) findViewById(R.id.icon);
		pager = (ViewPager) findViewById(R.id.gallery_pager);
		indicator = (CirclePageIndicator) findViewById(R.id.indicator);
		img = (ImageView) findViewById(R.id.imageView1);
		location_t = (TextView) findViewById(R.id.location);
		project_t = (TextView) findViewById(R.id.project);
		property_type_t = (TextView) findViewById(R.id.property);
		price_t = (TextView) findViewById(R.id.price);
		road_t = (TextView) findViewById(R.id.road);
		agent_t = (TextView) findViewById(R.id.name);
		contact_t = (TextView) findViewById(R.id.contact);
		email_t = (TextView) findViewById(R.id.email);
		heading_t = (TextView) findViewById(R.id.title);
		description_t = (TextView) findViewById(R.id.description);
		landed_area_t = (TextView) findViewById(R.id.land);
		buildup_area_t = (TextView) findViewById(R.id.buildup);
		tenure_t = (TextView) findViewById(R.id.tenure);
		date_t = (TextView) findViewById(R.id.date);
		bk = (ImageView) findViewById(R.id.star);
		zoning_t = (TextView) findViewById(R.id.zon);

		connection_detector = new ConnectionDetector(LandDetailsActivity.this);
		isInternetPresent = connection_detector.isConnectingToInternet();

		if (isInternetPresent) {
			new GetContent().execute();
		} else {
			Alert.ShowAlertMessage("No internet connection.", "Information", "Finish", null, LandDetailsActivity.this,
					LandDetailsActivity.this);
		}

		if (db.checkForTables()) {
			value = db.checkBookmark(id);
		}

		if (value == true) {
			bk.setImageResource(R.drawable.star);
		}

		onClick();
	}

	private class GetContent extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(LandDetailsActivity.this);
			pDialog.setMessage("Loading...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@SuppressWarnings("deprecation")
		@Override
		protected Void doInBackground(Void... arg0) {

			try {
				object = new JSONObject(json);

				id = DecodeEntity.decodeEntities(object.getString("propertyid"));
				location = DecodeEntity.decodeEntities(object.getString("location"));
				if (json.contains("\"Project\"")) {
					project = DecodeEntity.decodeEntities(object.getString("Project"));
				}
				property_type = DecodeEntity.decodeEntities(object.getString("type"));
				if (mode.startsWith("S")) {
					price = object.getString("SalePrice");

				} else if (mode.startsWith("R")) {
					price = object.getString("RentPrice");

				}
				zoning = DecodeEntity.decodeEntities(object.getString("Zoning"));
				landed_area = DecodeEntity.decodeEntities(object.getString("LandSqrtFt"));
				buildup_area = DecodeEntity.decodeEntities(object.getString("BuildUp"));
				tenure = DecodeEntity.decodeEntities(object.getString("Tenure"));
				agent = DecodeEntity.decodeEntities(object.getString("agentname"));
				contact = DecodeEntity.decodeEntities(object.getString("mobile"));
				email = DecodeEntity.decodeEntities(object.getString("email"));
				heading = DecodeEntity.decodeEntities(object.getString("LongHeading"));
				description = DecodeEntity.decodeEntities(object.getString("PropertyDesc"));
				road = DecodeEntity.decodeEntities(object.getString("road"));
				if (json.contains("UpdatedDate")) {
					date = DecodeEntity.decodeEntities(object.getString("UpdatedDate"));
				} else {
					date = DecodeEntity.decodeEntities(object.getString("UpdateDate"));
				}

				staff_pic = DecodeEntity.decodeEntities(object.getString("StaffPicURL"));

				if (!date.equals("")) {
					newdate = date.replace(date.substring(date.length() - 14), "");
				} else {
					newdate = "";
				}

				ServiceHandler sh4 = new ServiceHandler();
				HashMap<String, String> params4 = new HashMap<>();
				params4.put(TAG_type, "picture");
				params4.put(TAG_id, id);

				if (isInternetPresent) {
					json2 = sh4.makeServiceCall("GET", params4);

					if (!json2.equals("")) {

						jarray = new JSONArray(json2);

						if (!json2.contains("@ResultMessage")) {

							for (int j = 0; j < jarray.length(); j++) {
								JSONObject c = jarray.getJSONObject(j);
								String img = c.getString("pictureURL");

								MainDetails md = new MainDetails();
								md.setImage(img);
								md.setCover(json2);
								details.add(md);
							}
						}

					} else {
						Log.e("empty", "empty");
					}
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (pDialog.isShowing())
				pDialog.dismiss();

			location_t.setText(location);
			project_t.setText(project);
			property_type_t.setText(property_type);
			if (!price.matches("")) {

				price_t.setText("RM " + df.format(Double.parseDouble(price)));
			} else {
				price_t.setText("-");
			}
			road_t.setText(road);
			agent_t.setText(agent);
			contact_t.setText(contact);
			email_t.setText(email);
			heading_t.setText(heading);
			description_t.setText(description);
			landed_area_t.setText(landed_area);
			buildup_area_t.setText(buildup_area);
			tenure_t.setText(tenure);
			date_t.setText(newdate);
			zoning_t.setText(zoning);
			imageLoader.displayImage(staff_pic, staff, options);

			if (!json2.contains("@ResultMessage") && !json2.equals("") && jarray.length() != 0) {
				pager.setAdapter(new ImagePagerAdapter(details));
				mIndicator = indicator;

				indicator.setViewPager(pager);
				indicator.setSnap(true);

				img.setVisibility(View.GONE);
			} else {
				pager.setVisibility(View.GONE);
				indicator.setVisibility(View.GONE);
				img.setVisibility(View.VISIBLE);
			}
		}

	}

	public void onClick() {
		choose = (ImageButton) findViewById(R.id.SMSIcon);
		choose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.putExtra("address", contact);
				intent.putExtra("sms_body",
						"Hi, \n\nI'm searching on your mobile app and found this listing. Please send me more information about property at "
								+ location + ". \nProject Name: " + project + "\nProperty Road: " + road + "\nType: "
								+ property_type + "\nPrice: RM" + price);
				intent.setType("vnd.android-dir/mms-sms");
				startActivity(intent);

			}
		});
		choose = (ImageButton) findViewById(R.id.callIcon);
		choose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent = new Intent(Intent.ACTION_DIAL);
				intent.setData(Uri.parse("tel:" + contact));
				startActivity(intent);

			}
		});
		choose = (ImageButton) findViewById(R.id.emailIcon);
		choose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent i = new Intent(Intent.ACTION_SEND);
				i.setType("message/rfc822");
				i.putExtra(Intent.EXTRA_EMAIL, new String[] { email });
				i.putExtra(Intent.EXTRA_SUBJECT, "Enquiry From APP");
				i.putExtra(Intent.EXTRA_TEXT,
						"Hi, \n\nI'm searching on your mobile app and found this listing. Please send me more information about property at "
								+ location + ". \nProject Name: " + project + "\nProperty Road: " + road + "\nType: "
								+ property_type + "\nPrice: RM" + price);
				startActivity(Intent.createChooser(i, "Send Email"));

			}
		});
		choose = (ImageButton) findViewById(R.id.shareIcon);
		choose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				final ProgressDialog saveProgressDialog = new ProgressDialog(LandDetailsActivity.this);
				saveProgressDialog.setMessage("Loading ...");
				saveProgressDialog.isIndeterminate();
				saveProgressDialog.setCancelable(false);

				saveProgressDialog.show();

				PermissionCheckingClass perCheck = new PermissionCheckingClass();
				boolean externalStorage = perCheck.haveInternalStoragePermissions(LandDetailsActivity.this,v,getApplicationContext());

				if(!externalStorage){
					saveProgressDialog.dismiss();
					Toast.makeText(getApplicationContext(),"Please allow the in order to use share function.", Toast.LENGTH_LONG).show();
				}
				else{
					new Thread(new Runnable() {
						@Override
						public void run() {

							if (details.size() > 0) {

								try {
									int pos = pager.getCurrentItem();
									URL imageurl = new URL(details.get(0).image);
									Bitmap bitmap = BitmapFactory.decodeStream(imageurl.openConnection().getInputStream());

									String path = Environment.getExternalStorageDirectory().getAbsolutePath()
											+ "/penangrealty.png";
									file = new File(path);

									FileOutputStream out = new FileOutputStream(file);
									Log.d("in save()", "after outputstream");

									getResizedBitmap(bitmap).compress(Bitmap.CompressFormat.PNG, 50, out);
									out.flush();
									out.close();

								} catch (IOException e) {
								}
							} else {
								Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.no_image);
								String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
								file = new File(extStorageDirectory, "penangrealty.png");
								FileOutputStream outStream;
								try {
									outStream = new FileOutputStream(file);
									bm.compress(Bitmap.CompressFormat.PNG, 80, outStream);
									outStream.flush();
									outStream.close();
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							saveProgressDialog.show();
							handler.sendEmptyMessage(0);
						}

						@SuppressLint("HandlerLeak")
						private Handler handler = new Handler() {
							@Override
							public void handleMessage(Message msg) {
								saveProgressDialog.dismiss();
								String propertyInfo = "Location: " + location + "\nProject: " + project + "\nPrice: RM" + price + "\nType: "
										+ property_type + "\n"
										+ "https://play.google.com/store/apps/details?id=com.magusapp.property";

								AllClassRounder acr = new AllClassRounder();
								acr.displayAlertDialogForShare(LandDetailsActivity.this,file,propertyInfo);
								Log.i("Hello", "jSUT SEEING");
							}
						};
					}).start();
				}
			}
		});

		bookmark = (TextView) findViewById(R.id.interested);
		bookmark.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (value == false) {
					db.insertBookmark(id, mode, json);
					bk.setImageResource(R.drawable.star);
					value = true;
				} else {
					db.deleteBookmark(id);
					bk.setImageResource(R.drawable.star_empty);
					value = false;
				}
			}
		});

		bk = (ImageView) findViewById(R.id.star);
		bk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (value == false) {
					db.insertBookmark(id, mode, json);
					bk.setImageResource(R.drawable.star);
					value = true;
				} else {
					db.deleteBookmark(id);
					bk.setImageResource(R.drawable.star_empty);
					value = false;
				}
			}
		});
	}

	public static Bitmap getResizedBitmap(Bitmap bm) {
		int width = bm.getWidth();
		int height = bm.getHeight();
		int newWidth = width * 50 / 100;
		int newHeight = height * 50 / 100;
		float scaleWidth = ((float) newWidth) / width;
		float scaleHeight = ((float) newHeight) / height;
		Matrix matrix = new Matrix();
		// RESIZE THE BIT MAP
		matrix.postScale(scaleWidth, scaleHeight);
		// RECREATE THE NEW BITMAP
		Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
		return resizedBitmap;
	}

	private class ImagePagerAdapter extends PagerAdapter {

		ArrayList<MainDetails> details;
		private LayoutInflater inflater;

		ImagePagerAdapter(ArrayList<MainDetails> det) {
			this.details = det;
			inflater = getLayoutInflater();
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			((ViewPager) container).removeView((View) object);
		}

		@Override
		public void finishUpdate(View container) {
		}

		@Override
		public int getCount() {
			return details.size();
		}

		@Override
		public Object instantiateItem(ViewGroup view, final int position) {
			final MainDetails content = details.get(position);
			View imageLayout = inflater.inflate(R.layout.pager, view, false);
			ImageView imageView = (ImageView) imageLayout.findViewById(R.id.image);
			imageLoader.displayImage(content.image, imageView, options);

			((ViewPager) view).addView(imageLayout, 0);

			imageLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent i = new Intent(LandDetailsActivity.this, DisplayPhotoActivity.class);
					i.putExtra("position", position);
					i.putExtra("image", content.cover);
					startActivity(i);
				}
			});

			return imageLayout;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view.equals(object);
		}

		@Override
		public void restoreState(Parcelable state, ClassLoader loader) {
		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void startUpdate(View container) {
		}
	}

}
