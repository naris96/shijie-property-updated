package com.magusapp.property;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.magusapp.property.utility.Alert;
import com.magusapp.property.utility.AppPreferences;
import com.magusapp.property.utility.ConnectionDetector;
import com.magusapp.property.utility.ServiceHandler;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class UpdateActivity extends Activity {

	private static final String TAG_type = "type";
	private static final String TAG_email = "email";
	private static final String TAG_phone = "phone";
	private static final String TAG_name = "name";
	private static final String TAG_password = "password";

	String name, phone, email, password, result_type, result_message, new_password;
	String jsonStr;

	JSONObject object;

	ProgressDialog pDialog;

	EditText txtEmail, txtPassword, txtConfirm, txtName, txtPhone;
	Button submit;

	AppPreferences app_pref;

	Boolean isInternetPresent;
	ConnectionDetector connection_detector;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_update);

		connection_detector = new ConnectionDetector(UpdateActivity.this);
		isInternetPresent = connection_detector.isConnectingToInternet();

		app_pref = new AppPreferences(this);

		txtEmail = (EditText) findViewById(R.id.editEmail);
		txtPassword = (EditText) findViewById(R.id.editPassword);
		txtName = (EditText) findViewById(R.id.editName);
		txtPhone = (EditText) findViewById(R.id.editPhone);
		submit = (Button) findViewById(R.id.submit);

		email = app_pref.getUserEmail();
		password = app_pref.getUserPassword();
		name = app_pref.getUserName();
		phone = app_pref.getUserPhone();

		txtEmail.setText(email);
		txtPassword.setText(password);
		txtName.setText(name);
		txtPhone.setText(phone);

		submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (isInternetPresent) {
					new Update().execute();
				} else {
					Alert.ShowAlertMessage("No internet connection.", "Information", null, null, UpdateActivity.this,
							UpdateActivity.this);
				}
			}
		});
	}

	private class Update extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(UpdateActivity.this);
			pDialog.setMessage("Loading...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {

			new_password = txtPassword.getText().toString();

			ServiceHandler sh = new ServiceHandler();
			HashMap<String, String> params = new HashMap<>();
			params.put(TAG_type, "updateuser");
			params.put(TAG_email, email);
			params.put(TAG_phone, phone);
			params.put(TAG_name, name);
			params.put(TAG_password, new_password);
			jsonStr = sh.makeServiceCall("GET", params);

			try {

				JSONArray listing = new JSONArray(jsonStr);

				for (int i = 0; i < listing.length(); i++) {

					JSONObject c2 = listing.getJSONObject(i);
					result_type = c2.getString("@ResultType");
					result_message = c2.getString("@ResultMessage");

				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (pDialog.isShowing())
				pDialog.dismiss();

			if (!result_type.equals("Success")) {
				Alert.ShowAlertMessage(result_message, result_type, null, null, UpdateActivity.this,
						UpdateActivity.this);
				Intent intent = new Intent(UpdateActivity.this, LoginActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
			} else {

				Alert.ShowAlertMessage(result_message, result_type, null, null, UpdateActivity.this,
						UpdateActivity.this);

			}
		}

	}
}
