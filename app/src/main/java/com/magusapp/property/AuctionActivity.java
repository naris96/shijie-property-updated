package com.magusapp.property;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.costum.android.widget.LoadMoreListView;
import com.magusapp.property.utility.Alert;
import com.magusapp.property.utility.AppPreferences;
import com.magusapp.property.utility.ConnectionDetector;
import com.magusapp.property.utility.DecodeEntity;
import com.magusapp.property.utility.ServiceHandler;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;

public class AuctionActivity extends Activity {

	private static final String TAG_type = "type";

	JSONObject object;
	ProgressDialog pDialog;

	LoadMoreListView lv;
	TextView tv;

	ArrayList<MainDetails> details = new ArrayList<MainDetails>();

	JSONArray jarray;

	String jsonStr;
	String type;
	String image;

	Boolean isInternetPresent;
	ConnectionDetector connection_detector;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_list);

		connection_detector = new ConnectionDetector(AuctionActivity.this);
		isInternetPresent = connection_detector.isConnectingToInternet();

		tv = (TextView) findViewById(R.id.textView1);
		lv = (LoadMoreListView) findViewById(R.id.myListView);

		tv.setText("Auction List");

		if (isInternetPresent) {
			if (new AppPreferences(AuctionActivity.this).getUserName() != null) {
				new GetContent().execute();
			} else {
				Intent intent = new Intent(AuctionActivity.this, LoginActivity.class);
				Alert.ShowAlertMessage("Please Login to view Auction Lists", "Information", "Intent", intent,
						AuctionActivity.this, AuctionActivity.this);
			}
		} else {
			Alert.ShowAlertMessage("No internet connection.", "Information", "Finish", null, AuctionActivity.this,
					AuctionActivity.this);
		}
	}

	private class GetContent extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(AuctionActivity.this);
			pDialog.setMessage("Loading...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@SuppressWarnings("deprecation")
		@Override
		protected Void doInBackground(Void... arg0) {

			ServiceHandler sh = new ServiceHandler();
			HashMap<String, String> params = new HashMap<>();
			params.put(TAG_type, "auction");
			jsonStr = sh.makeServiceCall("GET", params);

			if (jsonStr != null) {
				try {

					jarray = new JSONArray(jsonStr);

					for (int k = 0; k < jarray.length(); k++) {

						JSONObject c2 = jarray.getJSONObject(k);

						String id = DecodeEntity.decodeEntities(c2.getString("LelongId"));
						String add = DecodeEntity.decodeEntities(c2.getString("Address"));
						String price = DecodeEntity.decodeEntities(c2.getString("Price"));
						String type = DecodeEntity.decodeEntities(c2.getString("Type"));
						String area = DecodeEntity.decodeEntities(c2.getString("LandArea"));
						String date = DecodeEntity.decodeEntities(c2.getString("AuctionDate"));
						String desc = DecodeEntity.decodeEntities(c2.getString("Description"));
						String agent = DecodeEntity.decodeEntities(c2.getString("AgentName"));
						String mobile = DecodeEntity.decodeEntities(c2.getString("AgentMobile"));
						String email = c2.getString("Email");
						String staff_image = c2.getString("StaffPicURL");
						image = DecodeEntity.decodeEntities(c2.getString("PropertyURL"));

						MainDetails cd = new MainDetails();
						cd.setID(id);
						cd.setLocation(add);
						cd.setPrice(price);
						cd.setType(type);
						cd.setArea(area);
						cd.setCreatedDate(date);
						cd.setDetails(desc);
						cd.setName(agent);
						cd.setPhone(mobile);
						cd.setImage(image);
						cd.setCover(staff_image);
						cd.setEmail(email);
						details.add(cd);

					}

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (pDialog.isShowing())
				pDialog.dismiss();

			if (jsonStr != null) {

				if (jarray.length() > 0) {
					lv.setAdapter(new AuctionAdapter(AuctionActivity.this, details));
				} else {
					Alert.ShowAlertMessage("Auction list is empty, stay tuned for new updates!", "Information",
							"Finish", null, AuctionActivity.this, AuctionActivity.this);
				}
			} else {
				Alert.ShowAlertMessage("Auction list is empty, stay tuned for new updates!", "Information", "Finish",
						null, AuctionActivity.this, AuctionActivity.this);
			}

		}

	}

}
