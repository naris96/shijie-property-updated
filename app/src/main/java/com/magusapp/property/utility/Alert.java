package com.magusapp.property.utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

public class Alert {

	public static void alertDialog(String title, String message, final Context c) {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(c);

		alertDialog.setTitle(title);
		alertDialog.setCancelable(false);
		alertDialog.setMessage(message);

		alertDialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent intent = ((Activity) c).getIntent();
				((Activity) c).finish();
				c.startActivity(intent);
			}
		});

		alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {

				dialog.cancel();
				((Activity) c).finish();

			}
		});

		alertDialog.show();
	}

	public static void ShowAlertMessage(String message, String title, final String modes, final Intent target,
			final Context c, final Activity activity) {
		AlertDialog.Builder alertBuilder = new AlertDialog.Builder(c);
		alertBuilder.setTitle(title);
		alertBuilder.setMessage(message);
		alertBuilder.setCancelable(false);
		alertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int whichButton) {
				if (modes == "Finish")
					activity.finish();
				else if (modes == "Intent")
					activity.startActivity(target);
				else if (modes == "Both") {
					activity.startActivity(target);
					activity.finish();
				}
			}
		});
		alertBuilder.create();
		alertBuilder.show();
	}

}
