package com.magusapp.property.utility;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;

public class ServiceHandler {

	String charset = "UTF-8";
	HttpURLConnection conn;
	DataOutputStream wr;
	StringBuilder result;
	URL urlObj;
	StringBuilder sbParams;
	String paramsString;
	String finalresult;
	String url = "http://www.ringotail.com/api/mobile_api3.php";
	String url2 = "http://www.ringotail.com/liveview/response.php";
	String url3 = "http://www.ringotail.com/liveview/json.php";

	public ServiceHandler() {

	}

	public String makeServiceCall(String method, HashMap<String, String> params) {

		sbParams = new StringBuilder();
		int i = 0;
		for (String key : params.keySet()) {
			try {
				if (i != 0) {
					sbParams.append("&");
				}
				sbParams.append(key).append("=").append(URLEncoder.encode(params.get(key), charset));

			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			i++;
		}

		if (method.equals("POST")) {
			// request method is POST
			try {
				urlObj = new URL(url);

				conn = (HttpURLConnection) urlObj.openConnection();

				conn.setDoOutput(true);

				conn.setRequestMethod("POST");

				conn.setRequestProperty("Accept-Charset", charset);

				conn.setReadTimeout(10000);
				conn.setConnectTimeout(15000);

				conn.connect();

				paramsString = sbParams.toString();

				wr = new DataOutputStream(conn.getOutputStream());
				wr.writeBytes(paramsString);
				wr.flush();
				wr.close();

			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (method.equals("GET")) {
			// request method is GET

			if (sbParams.length() != 0) {
				url += "?" + sbParams.toString();
			}

			 //Log.e("url", url.toString());
			try {
				urlObj = new URL(url);

				conn = (HttpURLConnection) urlObj.openConnection();

				conn.setDoOutput(false);

				conn.setRequestMethod("GET");

				conn.setRequestProperty("Accept-Charset", charset);

				conn.setConnectTimeout(15000);

				conn.connect();

			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		try {
			// Receive the response from the server
			InputStream in = new BufferedInputStream(conn.getInputStream());
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			result = new StringBuilder();
			String line;
			while ((line = reader.readLine()) != null) {
				result.append(line);
			}

			finalresult = result.toString();

			// Log.e("JSON Parser", "result: " + result.toString());

		} catch (IOException e) {
			e.printStackTrace();
		}

		conn.disconnect();

		// return JSON Object
		return finalresult;
	}

	public String makeAppCall(String method, HashMap<String, String> params) {

		sbParams = new StringBuilder();
		int i = 0;
		for (String key : params.keySet()) {
			try {
				if (i != 0) {
					sbParams.append("&");
				}
				sbParams.append(key).append("=").append(URLEncoder.encode(params.get(key), charset));

			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			i++;
		}

		if (method.equals("POST")) {
			// request method is POST
			try {
				urlObj = new URL(url2);

				conn = (HttpURLConnection) urlObj.openConnection();

				conn.setDoOutput(true);

				conn.setRequestMethod("POST");

				conn.setRequestProperty("Accept-Charset", charset);

				conn.setReadTimeout(10000);
				conn.setConnectTimeout(15000);

				conn.connect();

				paramsString = sbParams.toString();

				wr = new DataOutputStream(conn.getOutputStream());
				wr.writeBytes(paramsString);
				wr.flush();
				wr.close();

			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (method.equals("GET")) {
			// request method is GET

			if (sbParams.length() != 0) {
				url3 += "?" + sbParams.toString();
			}

			 Log.e("url", url3.toString());
			try {
				urlObj = new URL(url3);

				conn = (HttpURLConnection) urlObj.openConnection();

				conn.setDoOutput(false);

				conn.setRequestMethod("GET");

				conn.setRequestProperty("Accept-Charset", charset);

				conn.setConnectTimeout(15000);

				conn.connect();

			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		try {
			// Receive the response from the server
			InputStream in = new BufferedInputStream(conn.getInputStream());
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			result = new StringBuilder();
			String line;
			while ((line = reader.readLine()) != null) {
				result.append(line);
			}

			finalresult = result.toString();

			 Log.e("JSON Parser", "result: " + result.toString());

		} catch (IOException e) {
			e.printStackTrace();
		}

		conn.disconnect();

		// return JSON Object
		return finalresult;
	}
}
