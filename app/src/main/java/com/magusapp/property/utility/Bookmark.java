package com.magusapp.property.utility;

public class Bookmark {
	String json;
	String propertyID;
	String mode;

	public void setPropertyID(String propertyID) {
		this.propertyID = propertyID;
	}

	public void setJSON(String json) {
		this.json = json;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getPropertyID() {
		return propertyID;
	}

	public String getJSON() {
		return json;
	}

	public String getMode() {
		return mode;
	}

}
