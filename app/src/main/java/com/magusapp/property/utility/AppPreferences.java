package com.magusapp.property.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class AppPreferences {

	public static final String SERVER_BASE_URL = "http://ringotail.com/api/mobile_api3.php";

	private static final String APP_SHARED_PREFS = "user_details_pref";
	private SharedPreferences appSharedPrefs;
	private Editor prefsEditor;

	public AppPreferences(Context context) {
		this.appSharedPrefs = context.getSharedPreferences(APP_SHARED_PREFS, Context.MODE_PRIVATE);
		this.prefsEditor = appSharedPrefs.edit();
	}

	public boolean setUserDetails(String userId, String userName, String userEmail, String userPhone,
			String userPassword) {
		prefsEditor.putString("userId", userId);
		prefsEditor.putString("userName", userName);
		prefsEditor.putString("userEmail", userEmail);
		prefsEditor.putString("userPhone", userPhone);
		prefsEditor.putString("userPassword", userPassword);
		prefsEditor.commit();
		return true;
	}

	public boolean setWasLaunched(Boolean launched) {
		prefsEditor.putBoolean("wasLaunched", launched);
		prefsEditor.commit();
		return true;
	}

	public String getUserId() {
		return appSharedPrefs.getString("userId", null);
	}

	public String getUserName() {
		return appSharedPrefs.getString("userName", null);
	}

	public String getUserEmail() {
		return appSharedPrefs.getString("userEmail", null);
	}

	public String getUserPhone() {
		return appSharedPrefs.getString("userPhone", null);
	}

	public String getUserPassword() {
		return appSharedPrefs.getString("userPassword", null);
	}

	public boolean getWasLaunched() {
		return appSharedPrefs.getBoolean("wasLaunched", false);
	}

	public boolean isAuthorized() {
		if (getUserName() == null || getUserEmail() == null || getUserPhone() == null) {
			return false;
		}
		return true;
	}

}
