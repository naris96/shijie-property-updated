package com.magusapp.property.utility;

public class DecodeEntity {

	public DecodeEntity() {

	}

	public static String decodeEntities(String sb) {

		if (!sb.equals("null") && !sb.equals("")) {
			return sb.replace("&amp;", "&").replace("&lt;", "<").replace("&gt;", ">").replace("&quot;", "\"")
					.replace("&apos;", "'");
		} else {
			return "";
		}

	}

	public static String replaceType(String sb) {

		return sb.replace("INDUSTRY", "INDUSTRI").replace("DEVELOPMENT", "PEMBANGUNAN")
				.replace("AGRICULTURE", "PERTANIAN").replace("COMMERCIAL", "PERDAGANGAN")
				.replace("RESIDENTIAL", "KEDIAMAN").replace("OTHER", "LAIN-LAIN").replace("AREA", "KAWASAN");

	}

}
