package com.magusapp.property.utility;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseAdapter extends SQLiteOpenHelper {
	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "CMS";

	// Labels table name

	private static final String TABLE_LABELS2 = "Bookmark";

	// Labels Table Columns names
	private static final String KEY_ID = "id";

	private static final String KEY_PropertyID = "Property_id";
	private static final String KEY_JSON = "Json";
	private static final String KEY_Mode = "Mode";

	public DatabaseAdapter(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		// Category table create query

		String sql2 = "CREATE TABLE IF NOT EXISTS " + TABLE_LABELS2 + " ( " + KEY_ID
				+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_PropertyID + " TEXT NOT NULL, " + KEY_Mode
				+ " TEXT NOT NULL, " + KEY_JSON + " TEXT NOT NULL)";

		db.execSQL(sql2);

	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_LABELS2);

		// Create tables again
		onCreate(db);
	}

	// insert bookmark
	public void insertBookmark(String property_id, String mode, String json) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_PropertyID, property_id);
		values.put(KEY_Mode, mode);
		values.put(KEY_JSON, json);

		// Inserting Row
		db.insert(TABLE_LABELS2, null, values);
		db.close();
	}

	public ArrayList<Bookmark> getAllBookmark() {
		ArrayList<Bookmark> bookmarkList = new ArrayList<Bookmark>();
		String selectQuery = "SELECT  * FROM " + TABLE_LABELS2;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst()) {
			do {
				Bookmark bookmark = new Bookmark();
				bookmark.setJSON(cursor.getString(cursor.getColumnIndexOrThrow(KEY_JSON)));
				bookmark.setMode(cursor.getString(cursor.getColumnIndexOrThrow(KEY_Mode)));
				bookmark.setPropertyID(cursor.getString(cursor.getColumnIndexOrThrow(KEY_PropertyID)));

				bookmarkList.add(bookmark);
			} while (cursor.moveToNext());
		}
		cursor.close();
		return bookmarkList;
	}

	public boolean checkBookmark(String ID) {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_LABELS2, new String[] { KEY_PropertyID, KEY_Mode, KEY_JSON },
				KEY_PropertyID + "=?", new String[] { ID }, null, null, null, null);

		if (cursor.moveToFirst()) {
			return true;

		} else {
			return false;

		}

	}

	public void deleteBookmark(String ID) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_LABELS2, KEY_PropertyID + "=?", new String[] { ID });
		db.close();
	}

	// check table empty
	public boolean checkForTables() {

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM " + TABLE_LABELS2, null);

		if (cursor != null) {

			cursor.moveToFirst();

			int count = cursor.getInt(0);

			if (count > 0) {
				return true;
			}

			cursor.close();
		}

		return false;
	}

	// delete all
	public void clearTable() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_LABELS2, null, null);
	}

}
