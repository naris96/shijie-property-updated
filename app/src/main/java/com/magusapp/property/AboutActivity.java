package com.magusapp.property;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.costum.android.widget.LoadMoreListView;
import com.magusapp.property.utility.Alert;
import com.magusapp.property.utility.ConnectionDetector;
import com.magusapp.property.utility.DecodeEntity;
import com.magusapp.property.utility.ServiceHandler;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

public class AboutActivity extends Activity {

	private static final String TAG_type = "type";

	JSONObject object;
	ProgressDialog pDialog;

	LoadMoreListView lv;

	ArrayList<MainDetails> details = new ArrayList<MainDetails>();

	JSONArray jarray;

	String jsonStr;

	WebView wv;
	ImageView header;
	TextView tv;

	String desc, new_desc, font_desc, type;

	Boolean isInternetPresent;
	ConnectionDetector connection_detector;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);

		connection_detector = new ConnectionDetector(AboutActivity.this);
		isInternetPresent = connection_detector.isConnectingToInternet();

		Intent i = getIntent();
		type = i.getStringExtra("type");

		tv = (TextView) findViewById(R.id.textView1);
		wv = (WebView) findViewById(R.id.webView1);
		header = (ImageView) findViewById(R.id.background);

		if (type.equals("AboutUs")) {
			tv.setText("About Us");
		} else {
			tv.setText("Disclaimer");
		}

		if (isInternetPresent) {
			new GetContent().execute();
		} else {
			Alert.ShowAlertMessage("No internet connection.", "Information", "Finish", null, AboutActivity.this,
					AboutActivity.this);
		}
	}

	private class GetContent extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(AboutActivity.this);
			pDialog.setMessage("Loading...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@SuppressWarnings("deprecation")
		@Override
		protected Void doInBackground(Void... arg0) {

			ServiceHandler sh = new ServiceHandler();
			HashMap<String, String> params = new HashMap<>();
			params.put(TAG_type, "setting");
			jsonStr = sh.makeServiceCall("GET", params);

			if (jsonStr != null || !jsonStr.equals("")) {
				try {

					jarray = new JSONArray(jsonStr);

					for (int k = 0; k < jarray.length(); k++) {

						JSONObject c2 = jarray.getJSONObject(k);

						desc = DecodeEntity.decodeEntities(c2.getString(type));

						if (desc.contains("img src=\"")) {
							new_desc = desc.replace("img src=\"", "img src=\"file:///android_res/drawable/");
						} else {
							new_desc = desc;
						}

						if (new_desc.contains("font-size:")) {
							if (header.getTag().equals("320dp")) {
								font_desc = new_desc.replace("font-size:12px", "font-size:14px");
							} else if (header.getTag().equals("480dp")) {
								font_desc = new_desc.replace("font-size:12px", "font-size:16px");
							} else if (header.getTag().equals("600dp")) {
								font_desc = new_desc.replace("font-size:12px", "font-size:19px");
							} else if (header.getTag().equals("720dp")) {
								font_desc = new_desc.replace("font-size:12px", "font-size:23px");
							}
						} else {
							font_desc = new_desc;
						}

					}

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (pDialog.isShowing())
				pDialog.dismiss();

			if (jsonStr == null || jsonStr.equals("")) {

				Alert.alertDialog("Information", "No content for this page. We are working on it.", AboutActivity.this);

			} else {

				wv.setBackgroundColor(0x00000000);
				wv.loadDataWithBaseURL(null, font_desc, "text/html", "UTF-8", null);
			}

		}

	}
}
