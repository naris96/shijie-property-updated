package com.magusapp.property;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.magusapp.property.utility.Alert;
import com.magusapp.property.utility.ConnectionDetector;
import com.magusapp.property.utility.DecodeEntity;
import com.magusapp.property.utility.ServiceHandler;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ListView;

public class InfoActivity extends Activity {

	private static final String TAG_type = "type";

	JSONObject object;
	ProgressDialog pDialog;

	ListView lv;

	ArrayList<MainDetails> details = new ArrayList<MainDetails>();

	JSONArray jarray;

	String jsonStr;
	String type;
	String image;
	String newdate;

	Boolean isInternetPresent;
	ConnectionDetector connection_detector;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_selection);

		connection_detector = new ConnectionDetector(InfoActivity.this);
		isInternetPresent = connection_detector.isConnectingToInternet();

		lv = (ListView) findViewById(R.id.listView1);

		if (isInternetPresent) {
			new GetContent().execute();
		} else {
			Alert.ShowAlertMessage("No internet connection.", "Information", "Finish", null, InfoActivity.this,
					InfoActivity.this);
		}
	}

	private class GetContent extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(InfoActivity.this);
			pDialog.setMessage("Loading...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@SuppressWarnings("deprecation")
		@Override
		protected Void doInBackground(Void... arg0) {

			ServiceHandler sh = new ServiceHandler();
			HashMap<String, String> params = new HashMap<>();
			params.put(TAG_type, "usefulinfo");
			jsonStr = sh.makeServiceCall("GET", params);

			if (jsonStr != null || !jsonStr.equals("")) {
				try {

					jarray = new JSONArray(jsonStr);

					for (int k = 0; k < jarray.length(); k++) {

						JSONObject c2 = jarray.getJSONObject(k);

						String id = DecodeEntity.decodeEntities(c2.getString("InfoId"));
						String name = DecodeEntity.decodeEntities(c2.getString("Title"));
						String date = DecodeEntity.decodeEntities(c2.getString("CreateDate"));
						String desc = DecodeEntity.decodeEntities(c2.getString("Details"));
						String source = DecodeEntity.decodeEntities(c2.getString("Source"));
						String cover = c2.getString("PropertyURL");

						if (!date.equals("")) {
							newdate = date.replace(date.substring(date.length() - 14), "");
						} else {
							newdate = "";
						}

						MainDetails cd = new MainDetails();
						cd.setID(id);
						cd.setName(name);
						cd.setDetails(desc);
						cd.setCreatedDate(date);
						cd.setType(source);
						cd.setCover(cover);
						details.add(cd);

					}

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (pDialog.isShowing())
				pDialog.dismiss();

			if (jsonStr != null || !jsonStr.equals("")) {

				if (jarray.length() > 0) {
					lv.setAdapter(new InfoAdapter(InfoActivity.this, details));
				} else {
					Alert.ShowAlertMessage("No content for this page. We are working on it.", "Information", "Finish",
							null, InfoActivity.this, InfoActivity.this);
				}
			} else {
				Alert.ShowAlertMessage("No content for this page. We are working on it.", "Information", "Finish", null,
						InfoActivity.this, InfoActivity.this);
			}

		}

	}

}
