package com.magusapp.property;

import java.util.HashMap;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.magusapp.property.utility.Alert;
import com.magusapp.property.utility.AppPreferences;
import com.magusapp.property.utility.ConnectionDetector;
import com.magusapp.property.utility.NumberTextWatcher;
import com.magusapp.property.utility.ServiceHandler;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

public class BuyRentActivity extends Activity {

	private static final String TAG_type = "type";
	private static final String TAG_id = "name";
	private static final String TAG_phone = "phone";
	private static final String TAG_keyword = "keyword";
	private static final String TAG_property = "property";
	private static final String TAG_mode = "mode";
	private static final String TAG_price = "price";
	private static final String TAG_invest = "invest";
	private static final String TAG_email = "email";

	TextView submit, txtTitle;
	EditText txtphone, txtemail, txtprice, txtkeyword, txtname, txtRemark;
	Spinner spnr, spnr2;
	RadioGroup radioGroup;
	RadioButton radiobutton;

	String jsonStr;

	JSONObject object;

	ProgressDialog pDialog;

	String[] submissive_type = { "Buy", "Rent", "Both" };
	String[] property_type = { "Residential", "Commercial", "Land", "Industry" };

	String submissiveType, propertyType, purpose, email, phone, price, keyword, remark;
	String name, result_type, result_message;
	String title;

	AppPreferences app_pref;

	Boolean isInternetPresent;
	ConnectionDetector connection_detector;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_buy_rent);

		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		Intent i = getIntent();
		title = i.getStringExtra("title");

		connection_detector = new ConnectionDetector(BuyRentActivity.this);
		isInternetPresent = connection_detector.isConnectingToInternet();

		txtkeyword = (EditText) findViewById(R.id.editKeyword);
		txtname = (EditText) findViewById(R.id.editName);
		txtemail = (EditText) findViewById(R.id.editEmail);
		txtprice = (EditText) findViewById(R.id.editPrice);
		txtphone = (EditText) findViewById(R.id.editPhone);
		txtRemark = (EditText) findViewById(R.id.editRemark);
		radioGroup = (RadioGroup) findViewById(R.id.radioPurpose);
		txtTitle = (TextView) findViewById(R.id.title);

		txtTitle.setText(title);

		txtprice.addTextChangedListener(new NumberTextWatcher(txtprice));

		spnr = (Spinner) findViewById(R.id.spinnerSubType);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
				submissive_type);

		spnr.setAdapter(adapter);
		spnr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

				int position = spnr.getSelectedItemPosition();
				submissiveType = submissive_type[position];
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}

		});

		spnr2 = (Spinner) findViewById(R.id.spinnerProperty);
		ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
				property_type);

		spnr2.setAdapter(adapter2);
		spnr2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

				int position = spnr2.getSelectedItemPosition();
				propertyType = property_type[position];
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}

		});

		onClick();
	}

	public void onClick() {
		submit = (TextView) findViewById(R.id.submit);
		submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				int selectedId = radioGroup.getCheckedRadioButtonId();

				radiobutton = (RadioButton) findViewById(selectedId);
				if (radiobutton.getText().toString().equals("Invest")) {
					purpose = "y";
				} else {
					purpose = "n";
				}

				if (validation()) {
					if (isInternetPresent) {
						new GetContent().execute();
					} else {
						Alert.ShowAlertMessage("No internet connection.", "Information", null, null,
								BuyRentActivity.this, BuyRentActivity.this);
					}
				}

			}
		});
	}

	private class GetContent extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(BuyRentActivity.this);
			pDialog.setMessage("Loading...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			name = txtname.getText().toString();
			keyword = txtkeyword.getText().toString();
			email = txtemail.getText().toString();
			phone = txtphone.getText().toString();
			price = txtprice.getText().toString().replace(",", "");
			remark = txtRemark.getText().toString();

			ServiceHandler sh = new ServiceHandler();
			HashMap<String, String> params = new HashMap<>();
			params.put(TAG_type, "buyrent");
			params.put(TAG_id, name);
			params.put(TAG_phone, phone);
			params.put(TAG_keyword, keyword);
			params.put(TAG_property, propertyType);
			params.put(TAG_mode, submissiveType);
			params.put(TAG_price, price);
			params.put(TAG_invest, purpose);
			params.put(TAG_email, email);
			params.put("remark", remark);

			jsonStr = sh.makeServiceCall("POST", params);

			try {

				JSONArray listing = new JSONArray(jsonStr);

				for (int i = 0; i < listing.length(); i++) {

					JSONObject c2 = listing.getJSONObject(i);

					if (jsonStr.contains("@ResultMessage")) {
						result_message = c2.getString("@ResultMessage");
					} else {
						result_message = c2.getString("@RetID");
					}

				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (pDialog.isShowing())
				pDialog.dismiss();

			if (!jsonStr.contains("@ResultMessage")) {
				Alert.ShowAlertMessage(
						"Thanks for your submission  of you interest to buy ,invest or rent a property. Our dedicated REN will contact you soonest 世界地产谢谢您的支持。我们专业的中介员会尽快联络您",
						"Success", null, null, BuyRentActivity.this, BuyRentActivity.this);

			} else {

				Alert.ShowAlertMessage(result_message, "Information", null, null, BuyRentActivity.this,
						BuyRentActivity.this);

			}
		}

	}

	private boolean validation() {
		// LEVEL 1 - CHECK EMPTY
		String checkEmpty = "";
		txtemail.setText(txtemail.getText().toString().replaceAll(" ", "").trim());
		txtphone.setText(txtphone.getText().toString().replaceAll(" ", "").trim());
		txtprice.setText(txtprice.getText().toString().replaceAll(" ", "").trim());

		if (txtkeyword.getText().toString().matches(""))
			checkEmpty = "-Keyword Required\n";
		if (txtname.getText().toString().matches(""))
			checkEmpty += "-Name Required\n";
		if (txtphone.getText().toString().matches(""))
			checkEmpty += "-Phone Number Required\n";
		if (txtemail.getText().toString().matches(""))
			checkEmpty += "-Email Address Required\n";
		if (txtprice.getText().toString().matches(""))
			checkEmpty += "-Price Required\n";
		if (txtRemark.getText().toString().matches(""))
			checkEmpty += "-Remark Required";

		if (!checkEmpty.matches("")) {
			showMessageForValidation(checkEmpty, "Complete these fields");
			return false;
		} else {
			// LEVEL 2 - CHECK FIELDS
			String fieldsError = "";

			if (!validPhone(txtphone.getText().toString()))
				fieldsError = "-Phone Number not valid";
			if (!validEmail(txtemail.getText().toString()))
				fieldsError += "-Email address not valid\n";

			if (!fieldsError.matches("")) {
				showMessageForValidation(fieldsError, "Fields Error");
				return false;
			} else {
				// Log.i("SUCESS", "SUCCESS");
				return true;
			}
		}

	}

	private boolean validEmail(String email) {
		Pattern pattern = Patterns.EMAIL_ADDRESS;
		return pattern.matcher(email).matches();
	}

	private boolean validPhone(String phone) {
		Pattern pattern = Patterns.PHONE;
		return pattern.matcher(phone).matches();
	}

	private void showMessageForValidation(String theMessage, String theTitle) {
		Alert.ShowAlertMessage(theMessage, theTitle, null, null, BuyRentActivity.this, BuyRentActivity.this);
	}
}
