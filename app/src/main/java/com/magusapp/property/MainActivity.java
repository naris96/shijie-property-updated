package com.magusapp.property;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.magusapp.property.utility.AdjustableImageView;
import com.magusapp.property.utility.AppPreferences;
import com.magusapp.property.utility.ConnectionDetector;
import com.magusapp.property.utility.DecodeEntity;
import com.magusapp.property.utility.ServiceHandler;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends Activity {

	private static final String TAG_type = "type";

	ImageView selection;
	TextView text1, text2, text3, text4, text5, text6, text7, text8, text9, text10, text11, text12, text13;

	int resID;
	String resName;

	ImageLoader imageLoader = ImageLoader.getInstance();
	DisplayImageOptions options;

	ArrayList<MainDetails> details = new ArrayList<MainDetails>();

	JSONObject object;
	ProgressDialog pDialog;

	JSONArray jarray, jarrayBanner;

	String jsonStr = null, jsonBanner = null;

	String name0, name1, name2, name3, name4, name5, name6, name7, name8, name9, name10, name11, name12, name13, name14,
			name15, name16, name17, name18, name19, name20, name21, name22, name23;

	String cover;

	Boolean isInternetPresent;

	ConnectionDetector connection_detector;

	boolean gosetting = false;

	AppPreferences pref;

	ViewFlipper viewFlipper;

	String goclass;
	Bundle extras;

	final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		extras = getIntent().getExtras();

		pref = new AppPreferences(this);

		viewFlipper = (ViewFlipper) findViewById(R.id.banner);
		text1 = (TextView) findViewById(R.id.text1);
		text2 = (TextView) findViewById(R.id.text2);
		text3 = (TextView) findViewById(R.id.text3);
		text4 = (TextView) findViewById(R.id.text4);
		text5 = (TextView) findViewById(R.id.text5);
		text6 = (TextView) findViewById(R.id.text6);
		text7 = (TextView) findViewById(R.id.text7);
		text8 = (TextView) findViewById(R.id.text8);
		text9 = (TextView) findViewById(R.id.text9);
		text10 = (TextView) findViewById(R.id.text10);
		text11 = (TextView) findViewById(R.id.text11);
		text12 = (TextView) findViewById(R.id.text12);
		text13 = (TextView) findViewById(R.id.text13);

		onClick();

		connection_detector = new ConnectionDetector(MainActivity.this);
		isInternetPresent = connection_detector.isConnectingToInternet();

		if (!isInternetPresent) {
			showSettingsAlert();
		} else {
			new GetContent().execute();
		}

		options = new DisplayImageOptions.Builder().cacheInMemory(false).cacheOnDisk(true).considerExifParams(true)
				.showImageForEmptyUri(R.drawable.no_image).bitmapConfig(Bitmap.Config.RGB_565).build();

		if (!imageLoader.isInited()) {
			imageLoader.init(ImageLoaderConfiguration.createDefault(this));
		}


		if (Build.VERSION.SDK_INT >= 23) {
			int hasLocationPermission = checkSelfPermission( Manifest.permission.ACCESS_FINE_LOCATION );
			int hasCLocationPermission = checkSelfPermission( Manifest.permission.ACCESS_COARSE_LOCATION );
			int hasSMSPermission = checkSelfPermission( Manifest.permission.SEND_SMS );

			List<String> permissions = new ArrayList<String>();
			if( hasLocationPermission != PackageManager.PERMISSION_GRANTED ) {
				permissions.add( Manifest.permission.ACCESS_FINE_LOCATION );
			}

			if( hasCLocationPermission != PackageManager.PERMISSION_GRANTED ) {
				permissions.add( Manifest.permission.ACCESS_COARSE_LOCATION );
			}

			if( hasSMSPermission != PackageManager.PERMISSION_GRANTED ) {
				permissions.add( Manifest.permission.SEND_SMS );
			}


			if( !permissions.isEmpty() ) {
				requestPermissions( permissions.toArray( new String[permissions.size()] ), 123 );
			}
		}

	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		switch ( requestCode ) {
			case 123: {
				for( int i = 0; i < permissions.length; i++ ) {
					if( grantResults[i] == PackageManager.PERMISSION_GRANTED ) {
						Log.d( "Permissions", "Permission Granted: " + permissions[i] );
					} else if( grantResults[i] == PackageManager.PERMISSION_DENIED ) {
						Log.d( "Permissions", "Permission Denied: " + permissions[i] );
					}
				}
			}
			break;
			default: {
				super.onRequestPermissionsResult(requestCode, permissions, grantResults);
			}
		}
	}

	public void showSettingsAlert() {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);

		alertDialog.setCancelable(false);

		alertDialog.setTitle("Information");

		alertDialog.setMessage("Internet is not available. Do you want to turn on internet connection?");

		alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent intent = new Intent(Settings.ACTION_SETTINGS);
				startActivity(intent);
				gosetting = true;
			}
		});

		alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();

			}
		});

		alertDialog.show();
	}


	private void onClick() {
		selection = (ImageView) findViewById(R.id.image1);
		selection.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), Search1Activity.class);
				startActivity(i);

			}
		});

		selection = (ImageView) findViewById(R.id.image2);
		selection.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), AuctionActivity.class);
				startActivity(i);

			}
		});

		selection = (ImageView) findViewById(R.id.image3);
		selection.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), NewsListingActivity.class);
				startActivity(i);

			}
		});

		selection = (ImageView) findViewById(R.id.image4);
		selection.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), BestBuyActivity.class);
				i.putExtra("title", name1);
				startActivity(i);
			}
		});

		selection = (ImageView) findViewById(R.id.image5);
		selection.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent i = new Intent(getApplicationContext(), RecentOuterActivity.class);
				i.putExtra("new1", name15);
				i.putExtra("new2", name16);
				startActivity(i);

			}
		});

		selection = (ImageView) findViewById(R.id.image6);
		selection.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent i = new Intent(MainActivity.this, DirectoryActivity.class);
				i.putExtra("type", "directory");
				startActivity(i);
			}
		});

		selection = (ImageView) findViewById(R.id.image7);
		selection.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), SellLetActivity.class);
				i.putExtra("title", name19);
				startActivity(i);

			}
		});

		selection = (ImageView) findViewById(R.id.image8);
		selection.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), GalleryActivity.class);
				startActivity(i);

			}
		});

		selection = (ImageView) findViewById(R.id.image9);
		selection.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), BuyRentActivity.class);
				i.putExtra("title", name20);
				startActivity(i);

			}
		});

		selection = (ImageView) findViewById(R.id.image10);
		selection.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), NewsStartActivity.class);
				i.putExtra("news", name7);
				i.putExtra("info", name8);
				i.putExtra("goclass", "");
				startActivity(i);
			}
		});

		selection = (ImageView) findViewById(R.id.image11);
		selection.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent i = new Intent(getApplicationContext(), CalculatorActivity.class);
				startActivity(i);

			}
		});

		selection = (ImageView) findViewById(R.id.image12);
		selection.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), JoinActivity.class);
				i.putExtra("title", name21);
				startActivity(i);

			}
		});

		selection = (ImageView) findViewById(R.id.image13);
		selection.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), OtherActivity.class);
				i.putExtra("video", name14);
				startActivity(i);
			}
		});

	}

	private class GetContent extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(MainActivity.this);
			pDialog.setMessage("Loading...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@SuppressWarnings("deprecation")
		@Override
		protected Void doInBackground(Void... arg0) {

			ServiceHandler sh = new ServiceHandler();
			HashMap<String, String> params = new HashMap<>();
			params.put(TAG_type, "setting");
			jsonStr = sh.makeServiceCall("GET", params);

			ServiceHandler sh2 = new ServiceHandler();
			HashMap<String, String> params2 = new HashMap<>();
			params2.put("type", "app_catalog_all_products");
			params2.put("id", "48");
			jsonBanner = sh2.makeAppCall("GET", params2);

			if (jsonStr != null) {
				try {

					jarray = new JSONArray(jsonStr);

					for (int k = 0; k < jarray.length(); k++) {

						JSONObject c2 = jarray.getJSONObject(k);

						name0 = DecodeEntity.decodeEntities(c2.getString("Module0"));
						name1 = DecodeEntity.decodeEntities(c2.getString("Module1"));
						name2 = DecodeEntity.decodeEntities(c2.getString("Module2"));
						name3 = DecodeEntity.decodeEntities(c2.getString("Module3"));
						name4 = DecodeEntity.decodeEntities(c2.getString("Module4"));
						name5 = DecodeEntity.decodeEntities(c2.getString("Module5"));
						name6 = DecodeEntity.decodeEntities(c2.getString("Module6"));
						name7 = DecodeEntity.decodeEntities(c2.getString("Module7"));
						name8 = DecodeEntity.decodeEntities(c2.getString("Module8"));
						name9 = DecodeEntity.decodeEntities(c2.getString("Module9"));
						name10 = DecodeEntity.decodeEntities(c2.getString("Module10"));
						name11 = DecodeEntity.decodeEntities(c2.getString("Module11"));
						name12 = DecodeEntity.decodeEntities(c2.getString("Module12"));
						name13 = DecodeEntity.decodeEntities(c2.getString("Module13"));
						name14 = DecodeEntity.decodeEntities(c2.getString("Module14"));
						name15 = DecodeEntity.decodeEntities(c2.getString("Module15"));
						name16 = DecodeEntity.decodeEntities(c2.getString("Module16"));
						name17 = DecodeEntity.decodeEntities(c2.getString("Module17"));
						name18 = DecodeEntity.decodeEntities(c2.getString("Module18"));
						name19 = DecodeEntity.decodeEntities(c2.getString("Module19"));
						name20 = DecodeEntity.decodeEntities(c2.getString("Module20"));
						name21 = DecodeEntity.decodeEntities(c2.getString("Module21"));
						name22 = DecodeEntity.decodeEntities(c2.getString("Module22"));
						name23 = DecodeEntity.decodeEntities(c2.getString("Module23"));

					}

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			if (jsonBanner != null) {
				try {
					// Getting JSON Array node
					jarrayBanner = new JSONArray(jsonBanner);

					for (int i = 0; i < jarrayBanner.length(); i++) {
						JSONObject obj = jarrayBanner.getJSONObject(i);

						String title = DecodeEntity.decodeEntities(obj.getString("Name"));

						JSONArray prods = obj.getJSONArray("Product_Records");

						// looping through All Contacts
						for (int k = 0; k < prods.length(); k++) {
							JSONObject prodObj = prods.getJSONObject(k);
							String shortdesc = DecodeEntity.decodeEntities(prodObj.getString("ShortDesc"));
							String descrip = DecodeEntity.decodeEntities(prodObj.getString("Description"));

							JSONArray arr = prodObj.getJSONArray("Photo_Records");

							if (arr.length() != 0) {
								JSONObject photo = arr.getJSONObject(0);
								cover = "https://www.ringotail.com/liveview/" + photo.getString("Filename");
							}

							MainDetails cd = new MainDetails();
							cd.setCover(cover);
							if (shortdesc.contains("itunes")) {
								cd.setDetails(descrip);
							} else {
								cd.setDetails(shortdesc);
							}
							cd.setName(title);
							details.add(cd);

						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			} else {
				Log.e("ServiceHandler", "Couldn't get any data from the url");
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (pDialog.isShowing())
				pDialog.dismiss();

			for (int j = 0; j < details.size(); j++) {
				final MainDetails content = details.get(j);
				AdjustableImageView imageView = new AdjustableImageView(MainActivity.this);
				imageLoader.displayImage(content.cover, imageView, options);
				viewFlipper.addView(imageView);
				imageView.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

						Intent intent = new Intent(Intent.ACTION_VIEW);
						intent.setData(Uri.parse(content.details));
						startActivity(intent);
					}
				});
			}

			text1.setText(name0);
			text2.setText(name10);
			// text3.setText(name7);
			text4.setText(name1);
			text5.setText(name4);
			text6.setText(name6);
			text7.setText(name3);
			text8.setText(name23);
			text9.setText(name2);
			text10.setText(name8);
			text11.setText(name9);
			text12.setText(name11);
			text13.setText(name13);

			if (!pref.getWasLaunched()) {
				Intent i = new Intent(getApplicationContext(), VideoActivity.class);
				i.putExtra("video", name14);
				startActivity(i);
				pref.setWasLaunched(true);
			}

			if (extras != null) {

				goclass = extras.getString("goclass");

				if(goclass!=null){
					if (goclass.equals("141")) {
						Intent i = new Intent(getApplicationContext(), AuctionActivity.class);
						startActivity(i);
					} else if (goclass.equals("142")) {
						Intent i = new Intent(getApplicationContext(), BestBuyActivity.class);
						i.putExtra("title", name1);
						startActivity(i);
					} else if (goclass.equals("143")) {
						Intent i = new Intent(getApplicationContext(), NewsListingActivity.class);
						startActivity(i);
					} else if (goclass.equals("144")) {
						Intent i = new Intent(MainActivity.this, DirectoryActivity.class);
						i.putExtra("type", "directory");
						startActivity(i);
					} else if (goclass.equals("145")) {
						Intent i = new Intent(getApplicationContext(), NewsStartActivity.class);
						i.putExtra("news", name7);
						i.putExtra("info", name8);
						i.putExtra("goclass", goclass);

						startActivity(i);
					}
					goclass = "";
				}
				//Log.e("go", goclass);
			}

		}

	}

	@Override
	public void onResume() {
		super.onResume(); // Always call the superclass method first

		if (gosetting) {
			recreate();
		}

	}

}
