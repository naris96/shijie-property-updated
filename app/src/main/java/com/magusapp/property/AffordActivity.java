package com.magusapp.property;

import java.text.DecimalFormat;

import com.magusapp.property.utility.NumberTextWatcher;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class AffordActivity extends Fragment {

	EditText gross_income, installment, credit, load, other;
	Button calculate;
	TextView txt_gross_income, total_loan, income;
	TextView txt65, txt75, txt85;

	DecimalFormat df;

	double getincome = 0, getinstallment = 0, getcredit = 0, getload = 0, getother = 0, gettotal = 0, getnettincome = 0,
			get65 = 0, get75 = 0, get85 = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.activity_afford, container, false);

		getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		calculate = (Button) rootView.findViewById(R.id.calculate);
		gross_income = (EditText) rootView.findViewById(R.id.txt_gross_income);
		credit = (EditText) rootView.findViewById(R.id.txt_credit);
		installment = (EditText) rootView.findViewById(R.id.txt_installment);
		load = (EditText) rootView.findViewById(R.id.txt_load);
		other = (EditText) rootView.findViewById(R.id.txt_other);

		txt_gross_income = (TextView) rootView.findViewById(R.id.txt_gross);
		total_loan = (TextView) rootView.findViewById(R.id.txt_total);
		income = (TextView) rootView.findViewById(R.id.txt_nett);

		txt65 = (TextView) rootView.findViewById(R.id.txt_65);
		txt75 = (TextView) rootView.findViewById(R.id.txt_75);
		txt85 = (TextView) rootView.findViewById(R.id.txt_85);

		gross_income.addTextChangedListener(new NumberTextWatcher(gross_income));
		credit.addTextChangedListener(new NumberTextWatcher(credit));
		installment.addTextChangedListener(new NumberTextWatcher(installment));
		load.addTextChangedListener(new NumberTextWatcher(load));
		other.addTextChangedListener(new NumberTextWatcher(other));

		df = new DecimalFormat("#,###,##0.00");

		gross_income.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable s) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

				if (!gross_income.getText().toString().replace(",", "").equals("")) {
					getincome = Double.parseDouble(gross_income.getText().toString().replace(",", ""));
					txt_gross_income.setText("RM " + df.format(getincome));
				} else {
					txt_gross_income.setText("RM " + "0.00");
				}

			}
		});

		calculate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (!installment.getText().toString().replace(" ", "").equals("")) {
					getinstallment = Double
							.parseDouble(installment.getText().toString().replace(" ", "").replace(",", ""));

				} else {
					getinstallment = 0;

				}

				if (!credit.getText().toString().replace(" ", "").equals("")) {
					getcredit = Double.parseDouble(credit.getText().toString().replace(" ", "").replace(",", ""));
				} else {
					getcredit = 0;
				}

				if (!load.getText().toString().replace(" ", "").equals("")) {
					getload = Double.parseDouble(load.getText().toString().replace(" ", "").replace(",", ""));
				} else {
					getload = 0;

				}

				if (!other.getText().toString().replace(" ", "").equals("")) {
					getother = Double.parseDouble(other.getText().toString().replace(" ", "").replace(",", ""));
				} else {
					getother = 0;
				}

				gettotal = getinstallment + getcredit + getload + getother;
				getnettincome = getincome - gettotal;

				total_loan.setText("RM " + String.valueOf(df.format(gettotal)));
				income.setText("RM " + String.valueOf(df.format(getnettincome)));

				get65 = getnettincome * 65 / 100;
				get75 = getnettincome * 75 / 100;
				get85 = getnettincome * 85 / 100;

				txt65.setText("RM " + String.valueOf(df.format(get65)));
				txt75.setText("RM " + String.valueOf(df.format(get75)));
				txt85.setText("RM " + String.valueOf(df.format(get85)));

			}

		});

		return rootView;
	}

}
