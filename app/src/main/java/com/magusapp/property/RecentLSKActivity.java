package com.magusapp.property;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.magusapp.property.utility.Alert;
import com.magusapp.property.utility.AppPreferences;
import com.magusapp.property.utility.ConnectionDetector;
import com.magusapp.property.utility.DecodeEntity;
import com.magusapp.property.utility.ServiceHandler;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class RecentLSKActivity extends Activity {

	private static final String TAG_type = "type";
	private static final String TAG_limit = "limit";
	private static final String TAG_mode = "mode";
	private static final String TAG_name = "userid";
	private static final String TAG_keyword = "keyword";

	EditText txtKeyword;
	Button txtType;
	Button txtMode;
	TextView search;
	ListView lv;
	ImageView hide;
	TextView txt_keyword, txt_type, txt_mode;
	Button searchButton;

	String keyword;
	String property_type = "";
	String jsonStr, json;
	String newdate;
	String mode = "";
	String getmode;

	JSONObject object;
	JSONArray listing, jarray;

	ProgressDialog pDialog;

	AppPreferences app_pref;

	int selected = 0;
	int selected_mode = 0;
	String[] propertyType = { "INDUSTRY", "DEVELOPMENT", "AGRICULTURE", "COMMERCIAL", "RESIDENTIAL", "AREA", "OTHER" };
	String[] modeType = { "Sales", "Rent" };

	ArrayList<MainDetails> details = new ArrayList<MainDetails>();

	Boolean isInternetPresent;
	ConnectionDetector connection_detector;

	boolean hide_boolean = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_recent_lsk);

		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		connection_detector = new ConnectionDetector(RecentLSKActivity.this);
		isInternetPresent = connection_detector.isConnectingToInternet();

		lv = (ListView) findViewById(R.id.listView1);
		txtKeyword = (EditText) findViewById(R.id.editKeyword);
		txtType = (Button) findViewById(R.id.type);
		txtMode = (Button) findViewById(R.id.mode);
		search = (TextView) findViewById(R.id.search);
		hide = (ImageView) findViewById(R.id.hide);
		txt_keyword = (TextView) findViewById(R.id.keyword);
		txt_type = (TextView) findViewById(R.id.texttype);
		txt_mode = (TextView) findViewById(R.id.textmode);
		searchButton = (Button) findViewById(R.id.searchButton);

		app_pref = new AppPreferences(this);

		onClick();
	}

	public void onClick() {

		hide.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!hide_boolean) {
					txt_keyword.setVisibility(View.GONE);
					txt_type.setVisibility(View.GONE);
					txt_mode.setVisibility(View.GONE);
					txtKeyword.setVisibility(View.GONE);
					txtType.setVisibility(View.GONE);
					txtMode.setVisibility(View.GONE);
					hide_boolean = true;
					hide.setImageResource(R.drawable.down);
				} else {
					txt_keyword.setVisibility(View.VISIBLE);
					txt_type.setVisibility(View.VISIBLE);
					txt_mode.setVisibility(View.VISIBLE);
					txtKeyword.setVisibility(View.VISIBLE);
					txtType.setVisibility(View.VISIBLE);
					txtMode.setVisibility(View.VISIBLE);
					hide_boolean = false;
					hide.setImageResource(R.drawable.up);
				}
			}
		});

		txtMode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(RecentLSKActivity.this);
				builder.setTitle("Select Mode");

				builder.setSingleChoiceItems(modeType, selected_mode, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						selected_mode = which;
					}
				});
				builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						mode = modeType[selected_mode];
						txtMode.setText(mode);

					}
				});
				AlertDialog alert = builder.create();
				alert.show();

			}
		});

		search.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(txtKeyword.getWindowToken(), 0);

				keyword = txtKeyword.getText().toString();

				if (mode.equals("Sales")) {
					getmode = "recentsalelsk";

				} else {
					getmode = "recentrentlsk";

				}

				if (!keyword.matches("") && !property_type.matches("") && !mode.matches("")) {
					if (isInternetPresent) {
						new GetContent().execute();
					} else {
						Alert.ShowAlertMessage("No internet connection.", "Information", "Finish", null,
								RecentLSKActivity.this, RecentLSKActivity.this);
					}
				} else {
					Alert.ShowAlertMessage("Please Complete All Fields", "Informations", null, null,
							RecentLSKActivity.this, RecentLSKActivity.this);

				}

			}
		});

		searchButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(txtKeyword.getWindowToken(), 0);

				keyword = txtKeyword.getText().toString();

				if (mode.equals("Sales")) {
					getmode = "recentsalelsk";

				} else {
					getmode = "recentrentlsk";

				}

				if (!keyword.matches("") && !property_type.matches("") && !mode.matches("")) {
					if (isInternetPresent) {
						new GetContent().execute();
					} else {
						Alert.ShowAlertMessage("No internet connection.", "Information", "Finish", null,
								RecentLSKActivity.this, RecentLSKActivity.this);
					}
				} else {
					Alert.ShowAlertMessage("Please Complete All Fields", "Informations", null, null,
							RecentLSKActivity.this, RecentLSKActivity.this);

				}

			}
		});

		txtType.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(RecentLSKActivity.this);
				builder.setTitle("Select Property Type");

				builder.setSingleChoiceItems(propertyType, selected, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						selected = which;
					}
				});
				builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						property_type = propertyType[selected];
						txtType.setText(property_type);
					}
				});
				AlertDialog alert = builder.create();
				alert.show();

			}
		});
	}

	private class GetContent extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(RecentLSKActivity.this);
			pDialog.setMessage("Loading...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {

			keyword = txtKeyword.getText().toString().replace("'", "''");

			ServiceHandler sh = new ServiceHandler();
			HashMap<String, String> params = new HashMap<>();
			params.put(TAG_type, getmode);
			params.put(TAG_name, app_pref.getUserId());
			params.put(TAG_keyword, keyword);
			params.put(TAG_limit, "20");
			params.put(TAG_mode, DecodeEntity.replaceType(property_type));

			jsonStr = sh.makeServiceCall("GET", params);

			details.clear();
			if (jsonStr != null || !jsonStr.equals("")) {
				try {

					listing = new JSONArray(jsonStr);

					for (int i = 0; i < listing.length(); i++) {

						JSONObject c2 = listing.getJSONObject(i);

						String address = DecodeEntity.decodeEntities(c2.getString("Alamat"));
						String property = DecodeEntity.decodeEntities(c2.getString("PropertyType"));
						String price = DecodeEntity.decodeEntities(c2.getString("Price"));
						String land_type = DecodeEntity.decodeEntities(c2.getString("LandType"));
						String date = DecodeEntity.decodeEntities(c2.getString("TransactionDate"));
						String area = DecodeEntity.decodeEntities(c2.getString("Land"));
						String buildup = DecodeEntity.decodeEntities(c2.getString("BuildUp"));
						String propertyName = DecodeEntity.decodeEntities(c2.getString("Kawasan"));

						if (!date.equals("")) {
							newdate = date.replace(date.substring(date.length() - 14), "");
						} else {
							newdate = "";
						}

						MainDetails cd = new MainDetails();
						cd.setLocation(address);
						cd.setPrice(price);
						cd.setType(property);
						cd.setDetails(land_type);
						cd.setCreatedDate(date);
						cd.setArea(area);
						cd.setBuildUp(buildup);
						cd.setName(propertyName);
						details.add(cd);

					}

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (pDialog.isShowing())
				pDialog.dismiss();

			if (jsonStr != null || !jsonStr.equals("")) {

				if (listing.length() > 0) {
					txt_keyword.setVisibility(View.GONE);
					txt_type.setVisibility(View.GONE);
					txt_mode.setVisibility(View.GONE);
					txtKeyword.setVisibility(View.GONE);
					txtType.setVisibility(View.GONE);
					txtMode.setVisibility(View.GONE);
					hide.setVisibility(View.VISIBLE);
					hide.setImageResource(R.drawable.down);
					hide_boolean = true;
					lv.setAdapter(new RecentAdapter(RecentLSKActivity.this, details));

				} else {
					lv.setAdapter(null);
					Alert.ShowAlertMessage("No transaction found.", "Information", null, null, RecentLSKActivity.this,
							RecentLSKActivity.this);
				}
			} else {
				lv.setAdapter(null);
				Alert.ShowAlertMessage("No transaction found.", "Information", null, null, RecentLSKActivity.this,
						RecentLSKActivity.this);
			}

		}

	}

}
