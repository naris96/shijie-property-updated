package com.magusapp.property;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.magusapp.property.AndroidMultiPartEntity.ProgressListener;
import com.magusapp.property.utility.Alert;
import com.magusapp.property.utility.ConnectionDetector;
import com.magusapp.property.utility.NumberTextWatcher;
import com.magusapp.property.utility.SquareImageView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@SuppressLint("NewApi")
@TargetApi(Build.VERSION_CODES.KITKAT)
public class SellLetActivity extends Activity {

	private static final String TAG_type = "type";
	private static final String TAG_name = "name";
	private static final String TAG_phone = "phone";
	private static final String TAG_email = "email";
	private static final String TAG_company = "company";
	private static final String TAG_price = "price";
	private static final String TAG_address = "address";
	private static final String TAG_remark = "remark";
	private static final String TAG_subtype = "mode";
	private static final String TAG_photo1 = "photo1";
	private static final String TAG_photo2 = "photo2";
	private static final String TAG_photo3 = "photo3";

	String[] submissive_type = { "Sell", "Let", "Check Sell", "Check Let" };

	ProgressDialog barProgressDialog;

	Bitmap bmp = null, bmp2 = null, bmp3 = null;

	String path, path2, path3;
	String resName;
	String jsonStr;
	String submissiveType = "";
	String name, phone, email, address, price, remark, company;
	String res;
	String getresult = "";
	String title;

	SquareImageView photo1, photo2, photo3;
	Spinner spnr;
	TextView submit, txtTitle;
	EditText txtname, txtphone, txtemail, txtaddress, txtprice, txtremark, txtcompany;

	JSONObject object;

	boolean bmp_ok = false, bmp_ok2 = false, bmp_ok3 = false;

	long totalSize;

	Boolean isInternetPresent;
	ConnectionDetector connection_detector;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sell_let);

		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		Intent i = getIntent();
		title = i.getStringExtra("title");

		connection_detector = new ConnectionDetector(SellLetActivity.this);
		isInternetPresent = connection_detector.isConnectingToInternet();

		txtname = (EditText) findViewById(R.id.editName);
		txtphone = (EditText) findViewById(R.id.editPhone);
		txtemail = (EditText) findViewById(R.id.editEmail);
		txtaddress = (EditText) findViewById(R.id.editAddress);
		txtprice = (EditText) findViewById(R.id.editPrice);
		txtremark = (EditText) findViewById(R.id.editRemark);
		txtcompany = (EditText) findViewById(R.id.editCompanyName);
		txtTitle = (TextView) findViewById(R.id.title);

		txtTitle.setText(title);

		txtprice.addTextChangedListener(new NumberTextWatcher(txtprice));

		spnr = (Spinner) findViewById(R.id.spinner1);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
				submissive_type);

		spnr.setAdapter(adapter);
		spnr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

				int position = spnr.getSelectedItemPosition();
				submissiveType = submissive_type[position];
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}

		});

		onClick();

		if (Build.VERSION.SDK_INT >= 23) {
				int hasCamPermission = checkSelfPermission( android.Manifest.permission.CAMERA );
				int hasRStoragePermission = checkSelfPermission( android.Manifest.permission.READ_EXTERNAL_STORAGE );
				int hasWStoragePermission = checkSelfPermission( android.Manifest.permission.WRITE_EXTERNAL_STORAGE );

				List<String> permissions = new ArrayList<String>();

				if( hasCamPermission != PackageManager.PERMISSION_GRANTED ) {
					permissions.add( android.Manifest.permission.CAMERA );
				}


				if( hasRStoragePermission != PackageManager.PERMISSION_GRANTED ) {
					permissions.add( android.Manifest.permission.READ_EXTERNAL_STORAGE );
				}

				if( hasWStoragePermission != PackageManager.PERMISSION_GRANTED ) {
					permissions.add( android.Manifest.permission.WRITE_EXTERNAL_STORAGE );
				}

				if( !permissions.isEmpty() ) {
					requestPermissions( permissions.toArray( new String[permissions.size()] ), 123 );
				}
			}

		}

		@Override
		public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
			switch ( requestCode ) {
				case 123: {
					for( int i = 0; i < permissions.length; i++ ) {
						if( grantResults[i] == PackageManager.PERMISSION_GRANTED ) {
							Log.d( "Permissions", "Permission Granted: " + permissions[i] );
						} else if( grantResults[i] == PackageManager.PERMISSION_DENIED ) {
							Log.d( "Permissions", "Permission Denied: " + permissions[i] );
						}
					}
				}
				break;
				default: {
					super.onRequestPermissionsResult(requestCode, permissions, grantResults);
				}
			}
		}
	public void onClick() {
		submit = (TextView) findViewById(R.id.submit);
		submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (validation()) {
					if (isInternetPresent) {
						name = txtname.getText().toString();
						phone = txtphone.getText().toString();
						email = txtemail.getText().toString();
						price = txtprice.getText().toString().replace(",", "");
						company = txtcompany.getText().toString();
						address = txtaddress.getText().toString();
						remark = txtremark.getText().toString();
						new GetContent().execute();
					} else {
						Alert.ShowAlertMessage("No internet connection.", "Information", null, null,
								SellLetActivity.this, SellLetActivity.this);
					}
				}

			}
		});
		photo1 = (SquareImageView) findViewById(R.id.photo1);
		photo1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (bmp_ok) {
					Dialog custom = new Dialog(SellLetActivity.this);
					custom.requestWindowFeature(Window.FEATURE_NO_TITLE);
					custom.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
					custom.setContentView(R.layout.view_image);
					ImageView iv = (ImageView) custom.findViewById(R.id.imageView1);
					iv.setImageBitmap(bmp);
					custom.show();

				} else {
					Intent i = new Intent(SellLetActivity.this, SelectPhotoActivity.class);
					startActivityForResult(i, 1);
				}

			}
		});

		photo2 = (SquareImageView) findViewById(R.id.photo2);
		photo2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (bmp_ok2) {
					Dialog custom = new Dialog(SellLetActivity.this);
					custom.requestWindowFeature(Window.FEATURE_NO_TITLE);
					custom.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
					custom.setContentView(R.layout.view_image);
					ImageView iv = (ImageView) custom.findViewById(R.id.imageView1);
					iv.setImageBitmap(bmp2);
					custom.show();

				} else {
					Intent i = new Intent(SellLetActivity.this, SelectPhotoActivity.class);
					startActivityForResult(i, 2);
				}

			}
		});
		photo3 = (SquareImageView) findViewById(R.id.photo3);
		photo3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (bmp_ok3) {
					Dialog custom = new Dialog(SellLetActivity.this);
					custom.requestWindowFeature(Window.FEATURE_NO_TITLE);
					custom.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
					custom.setContentView(R.layout.view_image);
					ImageView iv = (ImageView) custom.findViewById(R.id.imageView1);
					ImageView x = (ImageView) custom.findViewById(R.id.imageView2);
					iv.setImageBitmap(bmp3);

					custom.show();

				} else {
					Intent i = new Intent(SellLetActivity.this, SelectPhotoActivity.class);
					startActivityForResult(i, 3);
				}
			}
		});
	}

	private class GetContent extends AsyncTask<Void, Integer, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			barProgressDialog = new ProgressDialog(SellLetActivity.this);
			barProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			barProgressDialog.setMessage("Uploading... Please Wait");
			barProgressDialog.setCancelable(false);
			barProgressDialog.setMax(100);
			barProgressDialog.show();

		}

		@SuppressWarnings("deprecation")
		@Override
		protected Void doInBackground(Void... arg0) {

			try {
				HttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost("http://magusapp.com/api/mobile_api3.php");

				AndroidMultiPartEntity entity = new AndroidMultiPartEntity(new ProgressListener() {

					@Override
					public void transferred(long num) {
						publishProgress((int) (num / (float) totalSize) * 100);
						Log.d("DEBUG", num + " - " + totalSize);
					}
				});

				entity.addPart(TAG_type, new StringBody("submitlisting"));
				entity.addPart(TAG_name, new StringBody(name));
				entity.addPart(TAG_phone, new StringBody(phone));
				entity.addPart(TAG_email, new StringBody(email));
				entity.addPart(TAG_price, new StringBody(price));
				entity.addPart(TAG_company, new StringBody(company));
				entity.addPart(TAG_address, new StringBody(address));
				entity.addPart(TAG_remark, new StringBody(remark));
				entity.addPart(TAG_subtype, new StringBody(submissiveType));

				if (bmp != null) {
					ByteArrayOutputStream stream = new ByteArrayOutputStream();
					bmp.compress(Bitmap.CompressFormat.JPEG, 50, stream);
					byte[] byteArray = stream.toByteArray();
					entity.addPart(TAG_photo1, new ByteArrayBody(byteArray, ContentType.MULTIPART_FORM_DATA,
							System.currentTimeMillis() + ".jpg"));
				}

				if (bmp2 != null) {
					ByteArrayOutputStream stream = new ByteArrayOutputStream();
					bmp2.compress(Bitmap.CompressFormat.JPEG, 50, stream);
					byte[] byteArray = stream.toByteArray();
					entity.addPart(TAG_photo2, new ByteArrayBody(byteArray, ContentType.MULTIPART_FORM_DATA,
							System.currentTimeMillis() + ".jpg"));
				}

				if (bmp3 != null) {
					ByteArrayOutputStream stream = new ByteArrayOutputStream();
					bmp3.compress(Bitmap.CompressFormat.JPEG, 50, stream);
					byte[] byteArray = stream.toByteArray();
					entity.addPart(TAG_photo3, new ByteArrayBody(byteArray, ContentType.MULTIPART_FORM_DATA,
							System.currentTimeMillis() + ".jpg"));
				}

				totalSize = entity.getContentLength();
				post.setEntity(entity);

				// Making server call
				HttpResponse response = client.execute(post);
				HttpEntity httpEntity = response.getEntity();
				String result = EntityUtils.toString(httpEntity);

				try {

					JSONArray listing = new JSONArray(result);

					for (int i = 0; i < listing.length(); i++) {

						JSONObject c2 = listing.getJSONObject(i);
						getresult = c2.getString("@RetID");

					}

				} catch (JSONException e) {
					e.printStackTrace();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
			barProgressDialog.setProgress((values[0]));

		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			// Dismiss the progress dialog
			barProgressDialog.dismiss();

			if (getresult.equals("")) {
				Alert.ShowAlertMessage("Unsuccessful", "Information", null, null, SellLetActivity.this,
						SellLetActivity.this);
			} else {
				Alert.ShowAlertMessage(
						"Thanks for your submission  of you interest to buy ,invest or rent a property. Our dedicated REN will contact you soonest 世界地产谢谢您的支持。我们专业的中介员会尽快联络您",
						"Success", null, null, SellLetActivity.this, SellLetActivity.this);
			}
		}

	}

	/* Render Main Image */

	class BitmapWorkerTask extends AsyncTask<Integer, Void, Bitmap> {
		// Decode image in background.
		@Override
		protected void onPreExecute() {
			barProgressDialog = new ProgressDialog(SellLetActivity.this);

			barProgressDialog.setTitle("Rendering Image ...");
			barProgressDialog.setMessage("Please wait a moment ...");
			barProgressDialog.isIndeterminate();
			barProgressDialog.show();
		}

		@Override
		protected Bitmap doInBackground(Integer... params) {

			bmp = getScaledBitmap(path);

			Log.e("width", String.valueOf(bmp.getWidth()));
			Log.e("height", String.valueOf(bmp.getHeight()));

			return bmp;
		}

		// Once complete, see if ImageView is still around and set bitmap.
		@Override
		protected void onPostExecute(Bitmap bitmap) {
			barProgressDialog.dismiss();
			setPic(photo1, path);
			photo2.setVisibility(View.VISIBLE);
			bmp_ok = true;
		}
	}

	class BitmapWorkerTask2 extends AsyncTask<Integer, Void, Bitmap> {
		// Decode image in background.
		@Override
		protected void onPreExecute() {
			barProgressDialog = new ProgressDialog(SellLetActivity.this);

			barProgressDialog.setTitle("Rendering Image ...");
			barProgressDialog.setMessage("Please wait a moment ...");
			barProgressDialog.isIndeterminate();
			barProgressDialog.show();
		}

		@Override
		protected Bitmap doInBackground(Integer... params) {

			bmp2 = getScaledBitmap(path2);

			Log.e("size", String.valueOf(bmp2.getAllocationByteCount()));

			return bmp2;
		}

		// Once complete, see if ImageView is still around and set bitmap.
		@Override
		protected void onPostExecute(Bitmap bitmap) {
			barProgressDialog.dismiss();
			setPic(photo2, path2);
			photo3.setVisibility(View.VISIBLE);
			bmp_ok2 = true;

		}
	}

	class BitmapWorkerTask3 extends AsyncTask<Integer, Void, Bitmap> {
		// Decode image in background.
		@Override
		protected void onPreExecute() {
			barProgressDialog = new ProgressDialog(SellLetActivity.this);

			barProgressDialog.setTitle("Rendering Image ...");
			barProgressDialog.setMessage("Please wait a moment ...");
			barProgressDialog.isIndeterminate();
			barProgressDialog.show();
		}

		@Override
		protected Bitmap doInBackground(Integer... params) {

			bmp3 = getScaledBitmap(path3);

			Log.e("size", String.valueOf(bmp3.getAllocationByteCount()));

			return bmp3;
		}

		// Once complete, see if ImageView is still around and set bitmap.
		@Override
		protected void onPostExecute(Bitmap bitmap) {
			barProgressDialog.dismiss();
			setPic(photo3, path3);
			bmp_ok3 = true;

		}
	}

	private void setPic(ImageView mImageView, String path) {
		// Get the dimensions of the View
		int targetW = mImageView.getWidth();
		int targetH = mImageView.getHeight();

		// Get the dimensions of the bitmap
		BitmapFactory.Options bmOptions = new BitmapFactory.Options();
		bmOptions.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, bmOptions);
		int photoW = bmOptions.outWidth;
		int photoH = bmOptions.outHeight;

		// Determine how much to scale down the image
		int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

		// Decode the image file into a Bitmap sized to fill the View
		bmOptions.inJustDecodeBounds = false;
		bmOptions.inSampleSize = scaleFactor;
		bmOptions.inPurgeable = true;

		Bitmap bitmap = BitmapFactory.decodeFile(path, bmOptions);
		mImageView.setImageBitmap(bitmap);
	}

	private Bitmap getScaledBitmap(String picturePath) {
		BitmapFactory.Options sizeOptions = new BitmapFactory.Options();
		sizeOptions.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(picturePath, sizeOptions);

		int inSampleSize = calculateInSampleSize(sizeOptions);

		sizeOptions.inJustDecodeBounds = false;
		sizeOptions.inSampleSize = inSampleSize;

		return BitmapFactory.decodeFile(picturePath, sizeOptions);
	}

	private int calculateInSampleSize(BitmapFactory.Options options) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		// Calculate ratios of height and width to requested height and
		// width

		if (height > 900 || width > 900) {
			final int heightRatio = Math.round((float) height / (float) height * 5);
			final int widthRatio = Math.round((float) width / (float) width * 5);

			// Choose the smallest ratio as inSampleSize value, this will
			// guarantee
			// a final image with both dimensions larger than or equal to the
			// requested height and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}

		return inSampleSize;
	}

	Bitmap decodeFile(File f) {
		try {
			// Decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(f), null, o);

			// The new size we want to scale to

			DisplayMetrics metrics = getBaseContext().getResources().getDisplayMetrics();

			int REQUIRED_SIZE = metrics.widthPixels;
			if (metrics.widthPixels > 1280) {
				REQUIRED_SIZE = 800;
			}

			// Find the correct scale value. It should be the power of 2.
			int width_tmp = o.outWidth, height_tmp = o.outHeight;
			int scale = 1;
			while (true) {
				if (width_tmp < REQUIRED_SIZE || height_tmp < REQUIRED_SIZE)
					break;
				width_tmp /= 2;
				height_tmp /= 2;
				scale *= 2;
			}

			// Decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;

			return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
		} catch (FileNotFoundException e) {
		}
		return null;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// if the result is capturing Image
		if (requestCode == 1) {
			if (resultCode == RESULT_OK) {
				path = data.getStringExtra("fileUri");
				new BitmapWorkerTask().execute();
			}
		} else if (requestCode == 2) {
			if (resultCode == RESULT_OK) {
				path2 = data.getStringExtra("fileUri");
				new BitmapWorkerTask2().execute();
			}
		} else if (requestCode == 3) {
			if (resultCode == RESULT_OK) {
				path3 = data.getStringExtra("fileUri");
				new BitmapWorkerTask3().execute();
			}
		}
	}

	private boolean validation() {
		// LEVEL 1 - CHECK EMPTY
		String checkEmpty = "";
		txtemail.setText(txtemail.getText().toString().replaceAll(" ", "").trim());
		txtname.setText(txtname.getText().toString().replaceAll(" ", "").trim());
		txtphone.setText(txtphone.getText().toString().replaceAll(" ", "").trim());
		txtaddress.setText(txtemail.getText().toString().replaceAll(" ", "").trim());
		txtprice.setText(txtname.getText().toString().replaceAll(" ", "").trim());

		if (txtname.getText().toString().matches(""))
			checkEmpty = "-Name Required\n";
		if (txtphone.getText().toString().matches(""))
			checkEmpty += "-Phone Number Required\n";
		if (txtemail.getText().toString().matches(""))
			checkEmpty += "-Email Address Required\n";
		if (submissiveType.matches(""))
			checkEmpty += "-Submissive Type Required\n";
		if (txtaddress.getText().toString().matches(""))
			checkEmpty += "-Address Required\n";
		if (txtprice.getText().toString().matches(""))
			checkEmpty += "-Price Required\n";

		if (!checkEmpty.matches("")) {
			showMessageForValidation(checkEmpty, "Complete these fields");
			return false;
		} else {
			// LEVEL 2 - CHECK FIELDS
			String fieldsError = "";
			if (!validPhone(txtphone.getText().toString()))
				fieldsError = "-Phone Number not valid";
			if (!validEmail(txtemail.getText().toString()))
				fieldsError += "-Email address not valid\n";

			if (!fieldsError.matches("")) {
				showMessageForValidation(fieldsError, "Fields Error");
				return false;
			} else {
				// Log.i("SUCESS", "SUCCESS");
				return true;
			}
		}

	}

	private boolean validEmail(String email) {
		Pattern pattern = Patterns.EMAIL_ADDRESS;
		return pattern.matcher(email).matches();
	}

	private boolean validPhone(String phone) {
		Pattern pattern = Patterns.PHONE;
		return pattern.matcher(phone).matches();
	}

	private void showMessageForValidation(String theMessage, String theTitle) {
		Alert.ShowAlertMessage(theMessage, theTitle, null, null, SellLetActivity.this, SellLetActivity.this);
	}

}
