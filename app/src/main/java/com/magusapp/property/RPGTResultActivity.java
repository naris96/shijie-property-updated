package com.magusapp.property;

import java.text.DecimalFormat;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class RPGTResultActivity extends Activity {

	double property_price, selling_price, year, count_total;

	double total_rpgt, num_examption, num_examption2, num_examption3, num_rpgt_rate, num_rpgt_rate2, num_rpgt_rate3,
			num_rpgt_pay, num_rpgt_pay2, num_rpgt_pay3, num_net_gain, num_net_gain2, num_net_gain3;

	TextView year_ownership, year_ownership2, year_ownership3, gross_gain, examption, rpgt_rate, rpgt_pay, net_gain,
			gross_gain2, examption2, rpgt_rate2, rpgt_pay2, net_gain2, gross_gain3, examption3, rpgt_rate3, rpgt_pay3,
			net_gain3;

	DecimalFormat df;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_rpgtresult);

		Intent i = getIntent();
		property_price = Double.parseDouble(i.getStringExtra("property_price"));
		selling_price = Double.parseDouble(i.getStringExtra("selling_price"));
		year = Double.parseDouble(i.getStringExtra("year"));
		count_total = Double.parseDouble(i.getStringExtra("count_total"));

		year_ownership2 = (TextView) findViewById(R.id.txt_year_ownership2);
		year_ownership = (TextView) findViewById(R.id.txt_year_ownership);
		year_ownership3 = (TextView) findViewById(R.id.txt_year_ownership3);

		gross_gain = (TextView) findViewById(R.id.txt_gross_gain);
		examption = (TextView) findViewById(R.id.txt_examption);
		rpgt_rate = (TextView) findViewById(R.id.txt_rpgt_rate);
		rpgt_pay = (TextView) findViewById(R.id.txt_rpgt_pay);
		net_gain = (TextView) findViewById(R.id.txt_net_gain);
		gross_gain2 = (TextView) findViewById(R.id.txt_gross_gain2);
		examption2 = (TextView) findViewById(R.id.txt_examption2);
		rpgt_rate2 = (TextView) findViewById(R.id.txt_rpgt_rate2);
		rpgt_pay2 = (TextView) findViewById(R.id.txt_rpgt_pay2);
		net_gain2 = (TextView) findViewById(R.id.txt_net_gain2);
		gross_gain3 = (TextView) findViewById(R.id.txt_gross_gain3);
		examption3 = (TextView) findViewById(R.id.txt_examption3);
		rpgt_rate3 = (TextView) findViewById(R.id.txt_rpgt_rate3);
		rpgt_pay3 = (TextView) findViewById(R.id.txt_rpgt_pay3);
		net_gain3 = (TextView) findViewById(R.id.txt_net_gain3);

		df = new DecimalFormat("#,###,##0.00");

		if (year == 1) {
			num_rpgt_rate = 30;
			num_rpgt_rate2 = 30;
			num_rpgt_rate3 = 30;
		} else if (year == 2) {
			num_rpgt_rate = 30;
			num_rpgt_rate2 = 30;
			num_rpgt_rate3 = 30;
		} else if (year == 3) {
			num_rpgt_rate = 30;
			num_rpgt_rate2 = 30;
			num_rpgt_rate3 = 30;
		} else if (year == 4) {
			num_rpgt_rate = 20;
			num_rpgt_rate2 = 20;
			num_rpgt_rate3 = 30;
		} else if (year == 5) {
			num_rpgt_rate = 15;
			num_rpgt_rate2 = 15;
			num_rpgt_rate3 = 30;
		} else if (year > 5) {
			num_rpgt_rate = 0;
			num_rpgt_rate2 = 5;
			num_rpgt_rate3 = 5;
		}

		total_rpgt = selling_price - property_price - count_total;
		num_examption = total_rpgt * 10 / 100;
		if (num_examption > 10000) {
			num_examption = 10000;
		}
		num_examption2 = 0;
		num_examption3 = 0;
		num_rpgt_pay = (total_rpgt - num_examption) * num_rpgt_rate / 100;
		num_rpgt_pay2 = (total_rpgt - num_examption2) * num_rpgt_rate2 / 100;
		num_rpgt_pay3 = (total_rpgt - num_examption3) * num_rpgt_rate3 / 100;
		num_net_gain = total_rpgt - num_rpgt_pay;
		num_net_gain2 = total_rpgt - num_rpgt_pay2;
		num_net_gain3 = total_rpgt - num_rpgt_pay3;

		year_ownership.setText(i.getStringExtra("year"));
		year_ownership2.setText(i.getStringExtra("year"));
		year_ownership3.setText(i.getStringExtra("year"));

		rpgt_rate.setText(String.valueOf(num_rpgt_rate) + " %");
		rpgt_rate2.setText(String.valueOf(num_rpgt_rate2) + " %");
		rpgt_rate3.setText(String.valueOf(num_rpgt_rate3) + " %");

		gross_gain.setText("RM " + df.format(total_rpgt));
		gross_gain2.setText("RM " + df.format(total_rpgt));
		gross_gain3.setText("RM " + df.format(total_rpgt));

		examption.setText("RM " + df.format(num_examption));
		examption2.setText("RM " + df.format(num_examption2));
		examption3.setText("RM " + df.format(num_examption3));

		rpgt_pay.setText("RM " + df.format(num_rpgt_pay));
		rpgt_pay2.setText("RM " + df.format(num_rpgt_pay2));
		rpgt_pay3.setText("RM " + df.format(num_rpgt_pay3));

		net_gain.setText("RM " + df.format(num_net_gain));
		net_gain2.setText("RM " + df.format(num_net_gain2));
		net_gain3.setText("RM " + df.format(num_net_gain3));

	}
}
