package com.magusapp.property;

import java.util.HashMap;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.magusapp.property.utility.Alert;
import com.magusapp.property.utility.AppPreferences;
import com.magusapp.property.utility.ConnectionDetector;
import com.magusapp.property.utility.ServiceHandler;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends Activity {

	private static final String TAG_type = "type";
	private static final String TAG_email = "email";
	private static final String TAG_password = "password";

	String name = "", password = "", forgetPassword = "";
	String result_name = "", result_email = "", result_phone = "", result_type = "", result_message = "",
			result_id = "";
	String jsonStr;

	JSONObject object;

	ProgressDialog pDialog;

	EditText txtEmail, txtPassword;

	Button submit, forget, register;

	Boolean isInternetPresent;
	ConnectionDetector connection_detector;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		txtEmail = (EditText) findViewById(R.id.editEmail);
		txtPassword = (EditText) findViewById(R.id.editPassword);

		onClick();
	}

	public void onClick() {
		submit = (Button) findViewById(R.id.submit);
		submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				name = txtEmail.getText().toString();
				password = txtPassword.getText().toString();

				if (!name.matches("") && !password.matches("")) {

					connection_detector = new ConnectionDetector(LoginActivity.this);
					isInternetPresent = connection_detector.isConnectingToInternet();
					if (isInternetPresent) {
						new Login().execute();
					} else {
						Alert.ShowAlertMessage("No internet connection.", "Information", "Finish", null,
								LoginActivity.this, LoginActivity.this);
					}
				} else {
					Alert.ShowAlertMessage("Please enter your Email and Password", "Informations", null, null,
							LoginActivity.this, LoginActivity.this);

				}

			}
		});

		forget = (Button) findViewById(R.id.forget_password);
		forget.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				LayoutInflater factory = LayoutInflater.from(LoginActivity.this);
				final View textEntryView = factory.inflate(R.layout.forget_password, null);
				new AlertDialog.Builder(LoginActivity.this).setTitle("Forget Your Password?").setView(textEntryView)
						.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int whichButton) {
						EditText txtForgetEmail = (EditText) textEntryView.findViewById(R.id.txtForgetEmail);
						forgetPassword = txtForgetEmail.getText().toString();
						if (!forgetPassword.matches("")) {
							if (validEmail(forgetPassword)) {
								new ForgetPassword().execute();
							} else {
								Alert.ShowAlertMessage("Email address not valid", "Informations", null, null,
										LoginActivity.this, LoginActivity.this);

							}
						} else {
							Alert.ShowAlertMessage("Please enter your email", "Informations", null, null,
									LoginActivity.this, LoginActivity.this);

						}
						/* User clicked OK so do some stuff */
					}
				}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int whichButton) {

						/* User clicked cancel so do some stuff */
					}
				}).create().show();

			}
		});

		register = (Button) findViewById(R.id.register);
		register.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
			}
		});
	}

	private class Login extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(LoginActivity.this);
			pDialog.setMessage("Loading...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {

			ServiceHandler sh = new ServiceHandler();
			HashMap<String, String> params = new HashMap<>();
			params.put(TAG_type, "verifyuser");
			params.put(TAG_email, name);
			params.put(TAG_password, password);
			jsonStr = sh.makeServiceCall("GET", params);

			try {

				JSONArray listing = new JSONArray(jsonStr);

				for (int i = 0; i < listing.length(); i++) {

					JSONObject c2 = listing.getJSONObject(i);

					if (jsonStr.contains("Name")) {
						result_id = c2.getString("ID");
						result_name = c2.getString("Name");
						result_email = c2.getString("Email");
						result_phone = c2.getString("Handphone");
					} else {
						result_type = c2.getString("@ResultType");
						result_message = c2.getString("@ResultMessage");

					}
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (pDialog.isShowing())
				pDialog.dismiss();

			if (jsonStr.contains("Name")) {
				new AppPreferences(LoginActivity.this).setUserDetails(result_id, result_name, result_email,
						result_phone, password);

				Intent intent = new Intent(LoginActivity.this, MainActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				Alert.ShowAlertMessage("Successfully Logged In", "Congratulations!", "Intent", intent,
						LoginActivity.this, LoginActivity.this);

			} else {
				Alert.ShowAlertMessage(result_message, "Information", null, null, LoginActivity.this,
						LoginActivity.this);
			}

		}

	}

	private boolean validEmail(String email) {
		Pattern pattern = Patterns.EMAIL_ADDRESS;
		return pattern.matcher(email).matches();
	}

	private class ForgetPassword extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(LoginActivity.this);
			pDialog.setMessage("Loading...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {

			ServiceHandler sh = new ServiceHandler();
			HashMap<String, String> params = new HashMap<>();
			params.put(TAG_type, "forgetpassword");
			params.put(TAG_email, forgetPassword);
			jsonStr = sh.makeServiceCall("GET", params);

			try {

				JSONArray listing = new JSONArray(jsonStr);

				for (int i = 0; i < listing.length(); i++) {

					JSONObject c2 = listing.getJSONObject(i);

					result_message = c2.getString("@ResultMessage");

				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (pDialog.isShowing())
				pDialog.dismiss();

			Alert.ShowAlertMessage(result_message, "Information", null, null, LoginActivity.this, LoginActivity.this);

		}

	}

}
