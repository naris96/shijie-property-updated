package com.magusapp.property;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.costum.android.widget.LoadMoreListView;
import com.magusapp.property.utility.Alert;
import com.magusapp.property.utility.ConnectionDetector;
import com.magusapp.property.utility.DecodeEntity;
import com.magusapp.property.utility.ServiceHandler;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;

public class BestBuyActivity extends Activity {

	int total;

	String price;
	String jsonStr;
	String newdate;
	String project;

	JSONObject object;

	ProgressDialog pDialog;

	LoadMoreListView lv;

	SearchListAdapter adapter;

	ArrayList<MainDetails> details = new ArrayList<MainDetails>();;

	Boolean isInternetPresent;
	ConnectionDetector connection_detector;

	TextView title, search;

	String name;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_best_buy);

		Intent i = getIntent();
		name = i.getStringExtra("title");

		connection_detector = new ConnectionDetector(BestBuyActivity.this);
		isInternetPresent = connection_detector.isConnectingToInternet();

		title = (TextView) findViewById(R.id.textView1);
		title.setText(name);

		lv = (LoadMoreListView) findViewById(R.id.myListView);
		adapter = new SearchListAdapter(this, details);

		if (isInternetPresent) {
			new GetContent().execute();
		} else {
			Alert.ShowAlertMessage("No internet connection.", "Information", "Finish", null, BestBuyActivity.this,
					BestBuyActivity.this);
		}
	}

	/**
	 * Async task class to get json by making HTTP call
	 */
	private class GetContent extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(BestBuyActivity.this);
			pDialog.setMessage("Loading...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {

			ServiceHandler sh = new ServiceHandler();
			HashMap<String, String> params = new HashMap<>();
			params.put("type", "best_buy");

			jsonStr = sh.makeServiceCall("GET", params);

			details.clear();
			if (jsonStr != null || !jsonStr.equals("")) {
				try {

					// Getting JSON Array node
					object = new JSONObject(jsonStr);

					JSONArray listing = object.getJSONArray("listing");

					total = listing.length();

					for (int k = 0; k < listing.length(); k++) {

						JSONObject c2 = listing.getJSONObject(k);

						String location = DecodeEntity.decodeEntities(c2.getString("location"));

						String type = DecodeEntity.decodeEntities(c2.getString("type"));

						if (jsonStr.contains("\"Project\"")) {
							project = DecodeEntity.decodeEntities(c2.getString("Project"));
						} else {
							project = DecodeEntity.decodeEntities(c2.getString("road"));
						}

						price = DecodeEntity.decodeEntities(c2.getString("SalePrice"));

						String update = DecodeEntity.decodeEntities(c2.getString("UpdatedDate"));

						String img = DecodeEntity.decodeEntities(c2.getString("PropertyURL"));

						String saleorrent = DecodeEntity.decodeEntities(c2.getString("SaleType"));

						if (!update.equals("")) {
							newdate = update.replace(update.substring(update.length() - 14), "");
						} else {
							newdate = "";
						}

						MainDetails cd;
						cd = new MainDetails();
						cd.setLocation(location);
						cd.setType(type);
						cd.setPrice(price);
						cd.setUpdate(newdate);
						cd.setObject(c2);
						cd.setImage(img);
						cd.setSaleorrent(saleorrent);
						if (jsonStr.contains("\"Project\"")) {
							cd.setProject(project);

						} else {
							cd.setDetails(project);

						}
						details.add(cd);

					}

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (pDialog.isShowing())
				pDialog.dismiss();

			if (jsonStr != null || !jsonStr.equals("")) {

				if (total > 0) {

					lv.setAdapter(new SearchListAdapter(BestBuyActivity.this, details));

				} else {
					lv.setAdapter(null);
					Alert.ShowAlertMessage("No result found.", "Information", null, null, BestBuyActivity.this,
							BestBuyActivity.this);
				}
			} else {
				lv.setAdapter(null);
				Alert.ShowAlertMessage("No result found.", "Information", null, null, BestBuyActivity.this,
						BestBuyActivity.this);
			}

		}

	}

}
