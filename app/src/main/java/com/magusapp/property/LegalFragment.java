package com.magusapp.property;

import java.text.DecimalFormat;

import com.magusapp.property.utility.NumberTextWatcher;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

public class LegalFragment extends Fragment {

	EditText property_price;
	TextView s_legal_fee, s_stamp_duty, l_legal_fee, l_stamp_duty, total, total2, total_all;

	double x = 0, y = 0, z = 0;
	double x1 = 0, x2 = 0, x3 = 0, x4 = 0, x5 = 0;
	double loan_stamp_duty = 0;
	double sp_stamp_duty = 0;
	double legal;
	double ssd1 = 0, ssd2 = 0, ssd3 = 0;
	double l1 = 0, l2 = 0, l3 = 0, l4 = 0, l5 = 0, l6 = 0;
	double total_1, total_2, total_3;

	DecimalFormat df;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.activity_legal_fragment, container, false);

		getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		property_price = (EditText) rootView.findViewById(R.id.txt_property_price);
		s_legal_fee = (TextView) rootView.findViewById(R.id.txt_s_legal);
		s_stamp_duty = (TextView) rootView.findViewById(R.id.txt_s_tamp);
		l_legal_fee = (TextView) rootView.findViewById(R.id.txt_l_legal);
		l_stamp_duty = (TextView) rootView.findViewById(R.id.txt_l_tamp);
		total = (TextView) rootView.findViewById(R.id.txt_total);
		total2 = (TextView) rootView.findViewById(R.id.txt_total2);
		total_all = (TextView) rootView.findViewById(R.id.txt_all);

		property_price.addTextChangedListener(new NumberTextWatcher(property_price));

		df = new DecimalFormat("#,###,##0.00");

		property_price.setOnEditorActionListener(new EditText.OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE || event.getAction() == KeyEvent.ACTION_DOWN) {

					if (!property_price.getText().toString().replace(" ", "").trim().matches("")) {
						x = 0;
						y = 0;
						z = 0;
						x1 = 0;
						x2 = 0;
						x3 = 0;
						x4 = 0;
						x5 = 0;
						loan_stamp_duty = 0;
						sp_stamp_duty = 0;
						legal = 0;
						ssd1 = 0;
						ssd2 = 0;
						ssd3 = 0;
						l1 = 0;
						l2 = 0;
						l3 = 0;
						l4 = 0;
						l5 = 0;
						l6 = 0;
						total_1 = 0;
						total_2 = 0;
						total_3 = 0;

						x = Double.parseDouble(property_price.getText().toString().replace(",", ""));
						loan_stamp_duty = x * 0.5 / 100;

						// calculate legal fee
						if (x > 150000) {
							x1 = x - 150000;
							l1 = 150000 * 1 / 100;

							Log.e("l1", String.valueOf(l1));

							if (x1 > 850000) {
								x2 = x1 - 850000;
								l2 = 850000 * 0.7 / 100;
								Log.e("l2", String.valueOf(l2));

								if (x2 > 2000000) {
									x3 = x2 - 2000000;
									l3 = 2000000 * 0.6 / 100;
									Log.e("l3", String.valueOf(l3));

									if (x3 > 2000000) {
										x4 = x3 - 2000000;
										l4 = 2000000 * 0.5 / 100;
										Log.e("l4", String.valueOf(l4));

										if (x4 > 2500000) {
											x5 = x4 - 2500000;
											l5 = 2500000 * 0.4 / 100;
											Log.e("l5", String.valueOf(l5));
											Log.e("x4", String.valueOf(x4));
											Log.e("x5", String.valueOf(x5));

											l6 = x5 * 0.3 / 100;
											Log.e("l6", String.valueOf(l6));

										} else {
											l5 = x4 * 0.4 / 100;
											Log.e("l5", String.valueOf(l5));
										}

									} else {
										l4 = x3 * 0.5 / 100;
										Log.e("l4", String.valueOf(l4));
									}

								} else {
									l3 = x2 * 0.6 / 100;
									Log.e("l3", String.valueOf(l3));
								}

							} else {
								l2 = x1 * 0.7 / 100;
								Log.e("l2", String.valueOf(l2));
							}

							legal = l1 + l2 + l3 + l4 + l5 + l6;

						} else {
							legal = x * 1 / 100;
							Log.e("l1", String.valueOf(legal));
						}

						// calculate stamp duty
						if (x > 100000) {
							y = x - 100000;
							ssd1 = 100000 * 1 / 100;
							Log.e("ssd1", String.valueOf(ssd1));

							if (y > 400000) {
								z = y - 400000;
								ssd2 = 400000 * 2 / 100;
								Log.e("ssd2", String.valueOf(ssd2));

								if (z > 500000) {
									ssd3 = z * 3 / 100;
									Log.e("ssd3", String.valueOf(ssd3));
								}

							} else {
								ssd2 = y * 2 / 100;
								Log.e("ssd2", String.valueOf(ssd2));
							}

							sp_stamp_duty = ssd1 + ssd2 + ssd3;

						} else {
							sp_stamp_duty = x * 1 / 100;
							Log.e("ssd1", String.valueOf(sp_stamp_duty));
						}

						total_1 = legal + sp_stamp_duty;
						total_2 = legal + loan_stamp_duty;
						total_3 = total_1 + total_2;

						s_legal_fee.setText("RM " + df.format(legal));
						s_stamp_duty.setText("RM " + df.format(sp_stamp_duty));
						total.setText("RM " + df.format(total_1));
						l_legal_fee.setText("RM " + df.format(legal));
						l_stamp_duty.setText("RM " + df.format(loan_stamp_duty));
						total2.setText("RM " + df.format(total_2));
						total_all.setText("RM " + df.format(total_3));

						InputMethodManager imm = (InputMethodManager) getActivity()
								.getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(property_price.getWindowToken(), 0);

					}

					return true; // consume.

				}
				return false;
			}

		});

		return rootView;
	}

}
