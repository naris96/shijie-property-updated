package com.magusapp.property;

import java.text.DecimalFormat;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class CoverterActivity extends Fragment {

	EditText unit1, unit2;
	TextView txt1, txt2;
	Spinner convertion;

	int getposition = 0;
	double getvalue = 0, value = 0;

	String[] unit = { "Select Conversion Type", "Hectare-Acre", "Acre-Square Feet", "Acre-Meter Square Feet",
			"Relong-Square Feet", "Meter Square Feet-Square Feet", "Rantai-Feet", "Meter-Feet", "Millimeter-Feet" };

	DecimalFormat twoDForm;

	boolean above = false, below = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.activity_coverter, container, false);

		getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		unit1 = (EditText) rootView.findViewById(R.id.txt_unit1);
		unit2 = (EditText) rootView.findViewById(R.id.txt_unit2);

		txt1 = (TextView) rootView.findViewById(R.id.unit1);
		txt2 = (TextView) rootView.findViewById(R.id.unit2);

		twoDForm = new DecimalFormat("#.##");

		convertion = (Spinner) rootView.findViewById(R.id.spinner1);

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_spinner_dropdown_item, unit);
		convertion.setAdapter(adapter);

		convertion.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				getposition = position;

				if (getposition == 1) {
					txt1.setText("Hectare");
					txt2.setText("Acre");
				} else if (getposition == 2) {
					txt1.setText("Acre");
					txt2.setText("Square Feet");
				} else if (getposition == 3) {
					txt1.setText("Acre");
					txt2.setText("Meter Square Feet");
				} else if (getposition == 4) {
					txt1.setText("Relong");
					txt2.setText("Square Feet");
				} else if (getposition == 5) {
					txt1.setText("Meter Square Feet");
					txt2.setText("Square Feet");
				} else if (getposition == 6) {
					txt1.setText("Rantai");
					txt2.setText("Feet");
				} else if (getposition == 7) {
					txt1.setText("Meter");
					txt2.setText("Feet");
				} else if (getposition == 8) {
					txt1.setText("Millimeter");
					txt2.setText("Feet");
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
			}
		});

		unit1.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				above = true;
				below = false;
				return false;
			}
		});

		unit2.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				above = false;
				below = true;
				return false;
			}

		});

		unit1.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable s) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (s.length() > 0) {
					if (!s.toString().startsWith(".")) {
						if (above == true) {
							if (getposition == 1) {
								getvalue = Double.parseDouble(String.valueOf(s)) * 2.471;
								unit2.setText(String.valueOf(getvalue));

							} else if (getposition == 2) {
								getvalue = Double.parseDouble(String.valueOf(s)) * 43560;
								unit2.setText(String.valueOf(getvalue));

							} else if (getposition == 3) {
								getvalue = Double.parseDouble(String.valueOf(s)) * 4046.86;
								unit2.setText(String.valueOf(getvalue));

							} else if (getposition == 4) {
								getvalue = Double.parseDouble(String.valueOf(s)) * 30976;
								unit2.setText(String.valueOf(getvalue));

							} else if (getposition == 5) {
								getvalue = Double.parseDouble(String.valueOf(s)) * 10.7639;
								unit2.setText(String.valueOf(getvalue));

							} else if (getposition == 6) {
								getvalue = Double.parseDouble(String.valueOf(s)) * 66;
								unit2.setText(String.valueOf(getvalue));

							} else if (getposition == 7) {
								getvalue = Double.parseDouble(String.valueOf(s)) * 3.281;
								unit2.setText(String.valueOf(getvalue));

							} else if (getposition == 8) {
								getvalue = Double.parseDouble(String.valueOf(s)) * 0.003281;
								unit2.setText(String.valueOf(getvalue));

							}
						}
					}
				}
			}
		});

		unit2.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable s) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (s.length() > 0) {
					if (!s.toString().startsWith(".")) {

						if (below == true) {
							if (getposition == 1) {
								getvalue = Double.parseDouble(String.valueOf(s)) / 2.471;
								unit1.setText(String.valueOf(getvalue));

							} else if (getposition == 2) {
								getvalue = Double.parseDouble(String.valueOf(s)) / 43560;
								unit1.setText(String.valueOf(getvalue));

							} else if (getposition == 3) {
								getvalue = Double.parseDouble(String.valueOf(s)) / 4046.86;
								unit1.setText(String.valueOf(getvalue));

							} else if (getposition == 4) {
								getvalue = Double.parseDouble(String.valueOf(s)) / 30976;
								unit1.setText(String.valueOf(getvalue));

							} else if (getposition == 5) {
								getvalue = Double.parseDouble(String.valueOf(s)) / 10.7639;
								unit1.setText(String.valueOf(getvalue));

							} else if (getposition == 6) {
								getvalue = Double.parseDouble(String.valueOf(s)) / 66;
								unit1.setText(String.valueOf(getvalue));

							} else if (getposition == 7) {
								getvalue = Double.parseDouble(String.valueOf(s)) / 3.281;
								unit1.setText(String.valueOf(getvalue));

							} else if (getposition == 8) {
								getvalue = Double.parseDouble(String.valueOf(s)) / 0.003281;
								unit1.setText(String.valueOf(getvalue));

							}
						}
					}
				}
			}
		});

		return rootView;
	}

}
