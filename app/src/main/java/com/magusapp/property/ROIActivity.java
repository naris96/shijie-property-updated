package com.magusapp.property;

import java.text.DecimalFormat;

import com.magusapp.property.utility.NumberTextWatcher;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class ROIActivity extends Fragment {

	ImageButton calculate;
	EditText income, cost, average;
	TextView total, total_average;

	DecimalFormat df;

	double getincome = 0, getcost = 0, gettotal = 0, getaverage = 0, gettotalaverage = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.activity_roi, container, false);

		getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		calculate = (ImageButton) rootView.findViewById(R.id.calculate);
		income = (EditText) rootView.findViewById(R.id.txt_income);
		cost = (EditText) rootView.findViewById(R.id.txt_cost);
		average = (EditText) rootView.findViewById(R.id.txt_average);
		total = (TextView) rootView.findViewById(R.id.gettotal);
		total_average = (TextView) rootView.findViewById(R.id.get_total_average);

		income.addTextChangedListener(new NumberTextWatcher(income));
		cost.addTextChangedListener(new NumberTextWatcher(cost));
		df = new DecimalFormat("#,###,##0.00");

		calculate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (!cost.getText().toString().replace(",", "").equals("")
						&& !income.getText().toString().replace(",", "").equals("")) {
					getincome = Double.parseDouble(income.getText().toString().replace(",", ""));
					getcost = Double.parseDouble(cost.getText().toString().replace(",", ""));

					gettotal = (getincome / getcost) * 100;
					total.setText(String.valueOf(df.format(gettotal)) + " %");
				}

				if (!average.getText().toString().replace(",", "").equals("")
						&& !income.getText().toString().replace(",", "").equals("")) {
					getincome = Double.parseDouble(income.getText().toString().replace(",", ""));
					getaverage = Double.parseDouble(average.getText().toString().replace(",", ""));

					gettotalaverage = getincome / (getaverage / 100);
					total_average.setText(String.valueOf("RM" + df.format(gettotalaverage)));

				}

			}
		});

		return rootView;
	}

}
