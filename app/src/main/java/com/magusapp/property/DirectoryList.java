package com.magusapp.property;

import java.util.ArrayList;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class DirectoryList extends BaseAdapter {

	ArrayList<MainDetails> details;
	Context context;

	ImageLoader imageLoader = ImageLoader.getInstance();
	DisplayImageOptions options;

	public DirectoryList(Context activity, ArrayList<MainDetails> det) {

		details = det;
		context = activity;

		options = new DisplayImageOptions.Builder().cacheInMemory(false).showImageForEmptyUri(R.drawable.no_image)
				.showImageOnFail(R.drawable.no_image).cacheOnDisk(true).considerExifParams(true)
				.bitmapConfig(Bitmap.Config.RGB_565).build();

		if (!imageLoader.isInited()) {
			imageLoader.init(ImageLoaderConfiguration.createDefault(context));
		}
	}

	@Override
	public int getCount() {
		return details.size();
	}

	@Override
	public Object getItem(int position) {
		return details.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;

		LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		if (view == null) {

			view = vi.inflate(R.layout.directory_list, null);
		}

		ImageView img = (ImageView) view.findViewById(R.id.icon);
		TextView project = (TextView) view.findViewById(R.id.project);
		TextView location = (TextView) view.findViewById(R.id.location);
		TextView date = (TextView) view.findViewById(R.id.create);

		final MainDetails content = details.get(position);

		project.setText(content.project);
		location.setText(content.location);
		date.setText(content.createddate);
		imageLoader.displayImage(content.image, img, options);

		view.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent i = new Intent(context, DirectoryDetailsActivity.class);
				i.putExtra("title", content.project);
				i.putExtra("detail", content.details);
				i.putExtra("id", content.id);
				((Activity) context).startActivity(i);

			}
		});

		return view;
	}

}
