package com.magusapp.property;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.magusapp.property.utility.HackyViewPager;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import uk.co.senab.photoview.PhotoView;

public class DisplayPhotoActivity extends Activity {

	String json2;
	String image;
	ArrayList<MainDetails> details;
	JSONArray jarray;

	private static final String STATE_POSITION = "STATE_POSITION";

	ImageLoader imageLoader = ImageLoader.getInstance();

	DisplayImageOptions options;

	private static final String ISLOCKED_ARG = "isLocked";

	HackyViewPager pager;

	int pagerPosition;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gallery_display);

		Intent i = getIntent();
		json2 = i.getStringExtra("image");
		pagerPosition = i.getIntExtra("position", 0);

		if (savedInstanceState != null) {
			pagerPosition = savedInstanceState.getInt(STATE_POSITION);
			// boolean isLocked = savedInstanceState.getBoolean(ISLOCKED_ARG,
			// false);
			// pager.setLocked(isLocked);
		}

		options = new DisplayImageOptions.Builder().showImageForEmptyUri(R.drawable.no_image).cacheInMemory(false)
				.cacheOnDisk(true).considerExifParams(true).bitmapConfig(Bitmap.Config.RGB_565).build();

		if (!imageLoader.isInited()) {
			imageLoader.init(ImageLoaderConfiguration.createDefault(this));
		}

		pager = (HackyViewPager) findViewById(R.id.pager);

		new GetContent().execute();
	}

	/**
	 * Async task class to get json by making HTTP call
	 */
	private class GetContent extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

		}

		@Override
		protected Void doInBackground(Void... arg0) {
			details = new ArrayList<MainDetails>();
			if (json2 != null) {
				try {

					jarray = new JSONArray(json2);

					for (int j = 0; j < jarray.length(); j++) {
						JSONObject c = jarray.getJSONObject(j);

						if (json2.contains("SmallURL")) {

							image = c.getString("SmallURL");
						} else {
							image = c.getString("pictureURL");
						}

						MainDetails cd;
						cd = new MainDetails();
						cd.setImage(image);

						details.add(cd);

					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			} else {
				Log.e("ServiceHandler", "Couldn't get any data from the url");
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

			pager.setAdapter(new ImagePagerAdapter(details));
			pager.setCurrentItem(pagerPosition);
		}

	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		if (isViewPagerActive()) {
			// outState.putBoolean(ISLOCKED_ARG, ((HackyViewPager)
			// pager).isLocked());
			outState.putInt(STATE_POSITION, pager.getCurrentItem());
		}
		super.onSaveInstanceState(outState);
	}

	private boolean isViewPagerActive() {
		return (pager != null && pager instanceof HackyViewPager);
	}

	private class ImagePagerAdapter extends PagerAdapter {

		private ArrayList<MainDetails> details;
		private LayoutInflater inflater;

		ImagePagerAdapter(ArrayList<MainDetails> det) {
			this.details = det;
			inflater = getLayoutInflater();
		}

		@Override
		public int getCount() {
			return details.size();
		}

		@Override
		public View instantiateItem(ViewGroup container, int position) {
			MainDetails content = details.get(position);
			View imageLayout = inflater.inflate(R.layout.preview_image, container, false);
			PhotoView imageView = (PhotoView) imageLayout.findViewById(R.id.imgDisplay);

			imageLoader.displayImage(content.image, imageView, options);

			((HackyViewPager) container).addView(imageLayout, 0);
			return imageLayout;

		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view.equals(object);
		}

	}

}
