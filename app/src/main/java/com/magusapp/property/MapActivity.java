package com.magusapp.property;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.magusapp.property.utility.ConnectionDetector;
import com.magusapp.property.utility.GPSTracker;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.widget.Toast;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {

	// Google Map
	private GoogleMap googleMap;
	String longi;
	String lat;
	double latitude;
	double longitude;
	String address, name;

	Boolean isInternetPresent = false;
	ConnectionDetector connection_detector;

	GPSTracker gps;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_map);

		Intent i = getIntent();
		name = i.getStringExtra("name");
		address = i.getStringExtra("address");
		lat = i.getStringExtra("latitude");
		longi = i.getStringExtra("longitude");

		latitude = Double.valueOf(lat);
		longitude = Double.valueOf(longi);

		SupportMapFragment mapFragment =
				(SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
		mapFragment.getMapAsync(this);

	}

	@Override
	public void onMapReady(GoogleMap map) {

		MarkerOptions marker = new MarkerOptions()
				.position(new LatLng(latitude, longitude)).title(name)
				.snippet(address);
	//	marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.map_mark));
		map.addMarker(marker);

		map.moveCamera(CameraUpdateFactory.newLatLngZoom(
				new LatLng(latitude, longitude), 15));

		map.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
			@Override
			public void onInfoWindowClick(Marker marker) {

				gps = new GPSTracker(MapActivity.this);

				// check if GPS enabled
				if (gps.canGetLocation()) {

					double latitude = gps.getLatitude();
					double longitude = gps.getLongitude();

					String url = "http://maps.google.com/maps?saddr="
							+ latitude
							+ ","
							+ longitude
							+ "&daddr=" + lat + "," + longi;

					Intent intent = new Intent(
							Intent.ACTION_VIEW,
							Uri.parse(url));

					startActivity(intent);

				} else {
					// can't get location
					// GPS or Network is not enabled
					// Ask user to enable GPS/network in
					// settings
					gps.showSettingsAlert();

				}
			}
		});
	}

}
