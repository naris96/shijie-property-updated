package com.magusapp.property;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.magusapp.property.utility.Alert;
import com.magusapp.property.utility.ConnectionDetector;
import com.magusapp.property.utility.NumberTextWatcher;
import com.magusapp.property.utility.ServiceHandler;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class SearchActivity extends Activity {

	Button choose, search, property, location;
	ImageView reset, bookmark, nearby;
	EditText edittext, txt_min, txt_max, txt_buildup_min, txt_buildup_max, txt_land_min, txt_land_max;

	int selected_state = 0;
	int selected_city = 0;
	int selected_property = 0;

	String saleorrent = "", city = "", state = "", mode = "", property_type = "", minvalue = "", maxvalue = "";
	String[] propertyType = { "Residential", "Commercial", "Land", "Industry" };
	String[] stateList = { "KEDAH", "PENANG ISLAND", "PERAK", "PENANG MAINLAND (SEBERANG PERAI)" };
	String[] city_list;
	String land_min, land_max, size_min, size_max;
	String Slocation;

	Boolean isInternetPresent;
	ConnectionDetector connection_detector;

	String jsonStr;
	JSONArray jarray;
	ProgressDialog pDialog;

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search);

		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		connection_detector = new ConnectionDetector(SearchActivity.this);
		isInternetPresent = connection_detector.isConnectingToInternet();

		saleorrent = "sales_property";

		search = (Button) findViewById(R.id.search);
		property = (Button) findViewById(R.id.type);
		location = (Button) findViewById(R.id.location);
		reset = (ImageView) findViewById(R.id.reset);
		bookmark = (ImageView) findViewById(R.id.bookmark);
		nearby = (ImageView) findViewById(R.id.nearby);
		edittext = (EditText) findViewById(R.id.editText1);
		txt_max = (EditText) findViewById(R.id.to);
		txt_min = (EditText) findViewById(R.id.from);
		txt_buildup_min = (EditText) findViewById(R.id.sizeFrom);
		txt_land_min = (EditText) findViewById(R.id.landedFrom);
		txt_buildup_max = (EditText) findViewById(R.id.sizeTo);
		txt_land_max = (EditText) findViewById(R.id.landedTo);

		txt_max.addTextChangedListener(new NumberTextWatcher(txt_max));
		txt_min.addTextChangedListener(new NumberTextWatcher(txt_min));
		txt_buildup_min.addTextChangedListener(new NumberTextWatcher(txt_buildup_min));
		txt_land_min.addTextChangedListener(new NumberTextWatcher(txt_land_min));
		txt_buildup_max.addTextChangedListener(new NumberTextWatcher(txt_buildup_max));
		txt_land_max.addTextChangedListener(new NumberTextWatcher(txt_land_max));

		onClick();

	}

	public void onClick() {
		choose = (Button) findViewById(R.id.sales);
		choose.setBackgroundResource(R.drawable.background_full);
		choose.setTextColor(Color.WHITE);

		choose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				resetButton();
				choose = (Button) findViewById(R.id.sales);
				choose.setBackgroundResource(R.drawable.background_full);
				choose.setTextColor(Color.WHITE);
				saleorrent = "sales_property";

			}
		});

		choose = (Button) findViewById(R.id.rent);
		choose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				resetButton();
				choose = (Button) findViewById(R.id.rent);
				choose.setBackgroundResource(R.drawable.background_full);
				choose.setTextColor(Color.WHITE);
				saleorrent = "rent_property";

			}
		});

		property.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(SearchActivity.this);
				builder.setTitle("Select Property Type");

				builder.setSingleChoiceItems(propertyType, selected_property, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						selected_property = which;
					}
				});
				builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						property_type = propertyType[selected_property];
						property.setText(property_type);
					}
				});
				AlertDialog alert = builder.create();
				alert.show();

			}
		});

		location.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(SearchActivity.this);
				builder.setTitle("Select State");

				builder.setSingleChoiceItems(stateList, selected_state, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						selected_state = which;

					}
				});

				builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Slocation = stateList[selected_state];

						if (selected_state == 3) {
							Slocation = "PENANG MAINLAND";
						}

						if (isInternetPresent) {
							new GetContent().execute();
						} else {
							Alert.ShowAlertMessage("No internet connection.", "Information", "Finish", null,
									SearchActivity.this, SearchActivity.this);
						}

					}

				});
				AlertDialog alert = builder.create();
				alert.show();
			}
		});

		search.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (property_type.equals("")) {

					Toast.makeText(SearchActivity.this, "Property Type must be selected", Toast.LENGTH_SHORT).show();

				} else {

					if (saleorrent.equals("sales_property") && property_type.equals("Residential")) {
						mode = "SR";
					} else if (saleorrent.equals("sales_property") && property_type.equals("Commercial")) {
						mode = "SC";
					} else if (saleorrent.equals("sales_property") && property_type.equals("Land")) {
						mode = "SL";
					} else if (saleorrent.equals("sales_property") && property_type.equals("Industry")) {
						mode = "SI";
					} else if (saleorrent.equals("rent_property") && property_type.equals("Residential")) {
						mode = "RR";
					} else if (saleorrent.equals("rent_property") && property_type.equals("Commercial")) {
						mode = "RC";
					} else if (saleorrent.equals("rent_property") && property_type.equals("Land")) {
						mode = "RL";
					} else if (saleorrent.equals("rent_property") && property_type.equals("Industry")) {
						mode = "RI";
					}

					maxvalue = txt_max.getText().toString().replace(",", "");
					minvalue = txt_min.getText().toString().replace(",", "");
					size_min = txt_buildup_min.getText().toString().replace(",", "");
					size_max = txt_buildup_max.getText().toString().replace(",", "");
					land_min = txt_land_min.getText().toString().replace(",", "");
					land_max = txt_land_max.getText().toString().replace(",", "");

					if (city.equals("Any")) {
						city = "";
					}

					Intent i = new Intent(SearchActivity.this, SearchListActivity.class);
					i.putExtra("type", saleorrent);
					i.putExtra("mode", mode);
					i.putExtra("property_type", property_type);
					i.putExtra("location", city);
					i.putExtra("min", minvalue);
					i.putExtra("max", maxvalue);
					i.putExtra("project", edittext.getText().toString().replace("'", "''"));
					i.putExtra("minsize", size_min);
					i.putExtra("maxsize", size_max);
					i.putExtra("minlandsize", land_min);
					i.putExtra("maxlandsize", land_max);

					startActivity(i);
				}

			}
		});

		reset.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				city = "";
				state = "";
				mode = "";
				maxvalue = "";
				minvalue = "";
				size_min = "";
				size_max = "";
				land_min = "";
				land_max = "";
				property_type = "";
				txt_max.setText("");
				txt_min.setText("");
				txt_buildup_min.setText("");
				txt_buildup_max.setText("");
				txt_land_min.setText("");
				txt_land_max.setText("");
				edittext.setText("");
				location.setText("Select Location");
				property.setText("Select Property Type");

			}
		});

		bookmark.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(SearchActivity.this, BookmarkActivity.class);
				startActivity(i);
			}
		});

		nearby.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(SearchActivity.this, NearbyActivity.class);
				startActivity(i);
			}
		});

	}

	private void resetButton() {
		choose = (Button) findViewById(R.id.sales);
		choose.setBackgroundResource(R.drawable.button_outer);
		choose.setTextColor(Color.BLACK);
		choose = (Button) findViewById(R.id.rent);
		choose.setBackgroundResource(R.drawable.button_outer);
		choose.setTextColor(Color.BLACK);
	}

	/**
	 * Async task class to get json by making HTTP call
	 */
	private class GetContent extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(SearchActivity.this);
			pDialog.setMessage("Loading...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {

			ServiceHandler sh = new ServiceHandler();
			HashMap<String, String> params = new HashMap<>();
			params.put("type", "getLocation");
			params.put("State", Slocation);

			jsonStr = sh.makeServiceCall("GET", params);

			if (jsonStr != null || !jsonStr.equals("")) {
				try {

					jarray = new JSONArray(jsonStr);

					city_list = new String[jarray.length()];

					for (int i = 0; i < jarray.length(); i++) {

						JSONObject c2 = jarray.getJSONObject(i);

						city_list[i] = c2.getString("Location");

					}

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (pDialog.isShowing())
				pDialog.dismiss();

			if (jarray.length() > 0) {
				AlertDialog.Builder builder2 = new AlertDialog.Builder(SearchActivity.this);
				builder2.setTitle("Select City");

				builder2.setSingleChoiceItems(city_list, selected_city, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						selected_city = which;
					}
				}).setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						city = city_list[selected_city];
						state = stateList[selected_state];
						location.setText(city);
					}
				});
				AlertDialog alert = builder2.create();
				alert.show();
			} else {
				Alert.ShowAlertMessage("Empty", "Select City", null, null, SearchActivity.this, SearchActivity.this);
			}

		}

	}
}
