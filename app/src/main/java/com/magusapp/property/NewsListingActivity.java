package com.magusapp.property;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.magusapp.property.utility.Alert;
import com.magusapp.property.utility.ConnectionDetector;
import com.magusapp.property.utility.DecodeEntity;
import com.magusapp.property.utility.ServiceHandler;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;

public class NewsListingActivity extends Activity {

	private static final String TAG_type = "type";

	JSONObject object;
	ProgressDialog pDialog;

	ArrayList<MainDetails> details = new ArrayList<MainDetails>();

	JSONArray jarray;

	String jsonStr, type;

	ListView lv;
	Button choose;
	String price, saleorrent, newdate, mode, project;

	Boolean isInternetPresent;
	ConnectionDetector connection_detector;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_news_listing);

		lv = (ListView) findViewById(R.id.listView);

		saleorrent = "Sale";
		type = "getAllNewSaleProperty";

		connection_detector = new ConnectionDetector(NewsListingActivity.this);
		isInternetPresent = connection_detector.isConnectingToInternet();

		if (isInternetPresent) {

			new GetContent().execute();
		} else {
			Alert.ShowAlertMessage("No internet connection.", "Information", "Finish", null, NewsListingActivity.this,
					NewsListingActivity.this);
		}
		onClick();

	}

	public void onClick() {
		choose = (Button) findViewById(R.id.sales);
		choose.setBackgroundResource(R.drawable.background_full);
		choose.setTextColor(Color.WHITE);

		choose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!saleorrent.equals("Sale")) {

					resetButton();
					choose = (Button) findViewById(R.id.sales);
					choose.setBackgroundResource(R.drawable.background_full);
					choose.setTextColor(Color.WHITE);
					saleorrent = "Sale";
					type = "getAllNewSaleProperty";
					new GetContent().execute();
				}
			}
		});

		choose = (Button) findViewById(R.id.rent);
		choose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!saleorrent.equals("Rent")) {
					resetButton();
					choose = (Button) findViewById(R.id.rent);
					choose.setBackgroundResource(R.drawable.background_full);
					choose.setTextColor(Color.WHITE);
					saleorrent = "Rent";
					type = "getAllNewRentProperty";
					new GetContent().execute();
				}
			}
		});
	}

	private void resetButton() {
		choose = (Button) findViewById(R.id.sales);
		choose.setBackgroundResource(R.drawable.button_outer);
		choose.setTextColor(Color.BLACK);
		choose = (Button) findViewById(R.id.rent);
		choose.setBackgroundResource(R.drawable.button_outer);
		choose.setTextColor(Color.BLACK);
	}

	private class GetContent extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(NewsListingActivity.this);
			pDialog.setMessage("Loading...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {

			ServiceHandler sh = new ServiceHandler();
			HashMap<String, String> params = new HashMap<>();
			params.put(TAG_type, type);
			jsonStr = sh.makeServiceCall("GET", params);
			details.clear();

			if (jsonStr != null || !jsonStr.equals("")) {
				try {

					JSONArray listing = new JSONArray(jsonStr);

					for (int k = 0; k < listing.length(); k++) {

						JSONObject c2 = listing.getJSONObject(k);

						String location = DecodeEntity.decodeEntities(c2.getString("location"));
						String type = DecodeEntity.decodeEntities(c2.getString("type"));

						if (saleorrent.equals("Sale")) {
							price = DecodeEntity.decodeEntities(c2.getString("SalePrice"));
						} else {
							price = DecodeEntity.decodeEntities(c2.getString("RentPrice"));
						}

						if (jsonStr.contains("\"Project\"")) {
							project = DecodeEntity.decodeEntities(c2.getString("Project"));
						} else {
							project = DecodeEntity.decodeEntities(c2.getString("road"));
						}

						String update = DecodeEntity.decodeEntities(c2.getString("UpdatedDate"));
						String img = DecodeEntity.decodeEntities(c2.getString("PropertyURL"));

						if (saleorrent.equals("Sale")) {
							mode = DecodeEntity.decodeEntities(c2.getString("SaleType"));
						} else {
							mode = DecodeEntity.decodeEntities(c2.getString("RentType"));
						}

						if (!update.equals("")) {
							newdate = update.replace(update.substring(update.length() - 14), "");
						} else {
							newdate = "";
						}

						MainDetails cd;
						cd = new MainDetails();
						cd.setLocation(location);
						cd.setType(type);
						cd.setPrice(price);
						if (jsonStr.contains("\"Project\"")) {
							cd.setProject(project);

						} else {
							cd.setDetails(project);

						}
						cd.setUpdate(newdate);
						cd.setObject(c2);
						cd.setSaleorrent(mode);
						cd.setImage(img);
						details.add(cd);

					}

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (pDialog.isShowing())
				pDialog.dismiss();

			if (jsonStr != null || !jsonStr.equals("")) {

				lv.setAdapter(new SearchListAdapter(NewsListingActivity.this, details));

			} else {
				Alert.ShowAlertMessage(
						"Please submit your requirement to us at buyer/tenant submission. Our dedicated REN will contact you soonest. \n世界地产谢谢您，请在 buyer/tenant 登记您的要求，我们专业的仲介员会尽快联络您。",
						"NO LISTING MATCH YOUR SEARCH", "Finish", null, NewsListingActivity.this,
						NewsListingActivity.this);
			}

		}

	}
}
