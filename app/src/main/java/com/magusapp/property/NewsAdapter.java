package com.magusapp.property;

import java.util.ArrayList;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class NewsAdapter extends BaseAdapter {

	ArrayList<MainDetails> details;
	Context context;

	ImageLoader imageLoader = ImageLoader.getInstance();
	DisplayImageOptions options;

	public NewsAdapter(Context activity, ArrayList<MainDetails> det) {

		details = det;
		context = activity;

		options = new DisplayImageOptions.Builder().cacheInMemory(false).showImageForEmptyUri(R.drawable.no_image)
				.showImageOnFail(R.drawable.no_image).cacheOnDisk(true).considerExifParams(true)
				.bitmapConfig(Bitmap.Config.RGB_565).build();

		if (!imageLoader.isInited()) {
			imageLoader.init(ImageLoaderConfiguration.createDefault(context));
		}

	}

	@Override
	public int getCount() {
		return details.size();
	}

	@Override
	public Object getItem(int position) {
		return details.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;

		LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		if (view == null) {

			view = vi.inflate(R.layout.news_list, null);
		}

		TextView name = (TextView) view.findViewById(R.id.name);
		TextView desc = (TextView) view.findViewById(R.id.description);
		TextView date = (TextView) view.findViewById(R.id.date);
		TextView source = (TextView) view.findViewById(R.id.source);
		TextView dateT = (TextView) view.findViewById(R.id.dateTitle);
		TextView sourceT = (TextView) view.findViewById(R.id.sourceTitle);
		ImageView img = (ImageView) view.findViewById(R.id.icon);

		final MainDetails content = details.get(position);
		name.setText(content.name);
		desc.setText("Description: " + content.details);
		sourceT.setText("Source: ");
		source.setText(content.type);
		dateT.setText("News Date Time: ");
		date.setText(content.createddate);
		imageLoader.displayImage(content.cover, img, options);

		view.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent i = new Intent(context, NewsDetailsActivity.class);
				i.putExtra("id", content.id);
				i.putExtra("detail", content.details);
				i.putExtra("date", content.createddate);
				i.putExtra("name", content.name);
				i.putExtra("source", content.type);
				i.putExtra("type", "news");
				((Activity) context).startActivity(i);

			}
		});

		return view;
	}

}
