package com.magusapp.property;

import java.text.DecimalFormat;
import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class RecentAdapter extends BaseAdapter {

	ArrayList<MainDetails> details;
	Context context;

	public RecentAdapter(Context activity, ArrayList<MainDetails> det) {

		details = det;
		context = activity;

	}

	@Override
	public int getCount() {
		return details.size();
	}

	@Override
	public Object getItem(int position) {
		return details.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;

		LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		if (view == null) {

			view = vi.inflate(R.layout.recent_list, null);
		}

		DecimalFormat df = new DecimalFormat("#,###,##0.00");

		TextView address = (TextView) view.findViewById(R.id.address);
		TextView property = (TextView) view.findViewById(R.id.property);
		TextView price = (TextView) view.findViewById(R.id.price);
		TextView land = (TextView) view.findViewById(R.id.land);
		TextView date = (TextView) view.findViewById(R.id.date);
		TextView name = (TextView) view.findViewById(R.id.propertyName);
		TextView area = (TextView) view.findViewById(R.id.landArea);
		TextView build = (TextView) view.findViewById(R.id.buildUp);

		final MainDetails content = details.get(position);

		address.setText(content.location);
		property.setText(content.type);

		if (!content.price.matches("")) {
			price.setText("RM" + df.format(Double.parseDouble(content.price)));
		} else {
			price.setText("-");
		}

		land.setText(content.details);
		date.setText(content.createddate);
		name.setText(content.name);
		area.setText(content.area);
		build.setText(content.buildUp);

		view.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

			}
		});

		return view;
	}

}
