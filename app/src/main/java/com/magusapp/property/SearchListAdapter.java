package com.magusapp.property;

import java.text.DecimalFormat;
import java.util.ArrayList;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SearchListAdapter extends BaseAdapter {

	ArrayList<MainDetails> details;
	Context context;

	ImageLoader imageLoader = ImageLoader.getInstance();
	DisplayImageOptions options;

	public SearchListAdapter(Context activity, ArrayList<MainDetails> det) {

		details = det;
		context = activity;

		options = new DisplayImageOptions.Builder().cacheInMemory(false).showImageForEmptyUri(R.drawable.no_image)
				.showImageOnFail(R.drawable.no_image).cacheOnDisk(true).considerExifParams(true)
				.bitmapConfig(Bitmap.Config.RGB_565).build();

		if (!imageLoader.isInited()) {
			imageLoader.init(ImageLoaderConfiguration.createDefault(context));
		}
	}

	@Override
	public int getCount() {
		return details.size();
	}

	@Override
	public Object getItem(int position) {
		return details.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;

		LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		if (view == null) {

			view = vi.inflate(R.layout.search_list, null);
		}

		ImageView img = (ImageView) view.findViewById(R.id.icon);
		TextView title = (TextView) view.findViewById(R.id.location);
		TextView project = (TextView) view.findViewById(R.id.project);
		TextView road = (TextView) view.findViewById(R.id.projectTitle);
		TextView property = (TextView) view.findViewById(R.id.property);
		TextView price = (TextView) view.findViewById(R.id.price);
		TextView update = (TextView) view.findViewById(R.id.update);

		final MainDetails content = details.get(position);

		DecimalFormat df = new DecimalFormat("#,###,##0.00");

		title.setText(content.location);

		if (content.project == null) {
			project.setText(null);

		} else {
			project.setText(content.project);

		}

		property.setText(content.type);
		if (!content.price.matches("")) {
			price.setText("RM" + df.format(Double.parseDouble(content.price)));
		} else {
			price.setText("-");
		}
		update.setText(content.update);
		imageLoader.displayImage(content.image, img, options);

		view.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (content.saleorrent.endsWith("R")) {
					Intent i = new Intent(context, ResidentialDetailsActivity.class);
					i.putExtra("mode", content.saleorrent);
					i.putExtra("object", content.jsonObject.toString());
					((Activity) context).startActivity(i);

				} else if (content.saleorrent.endsWith("C")) {
					Intent i = new Intent(context, CommercialDetailsActivity.class);
					i.putExtra("mode", content.saleorrent);
					i.putExtra("object", content.jsonObject.toString());
					((Activity) context).startActivity(i);

				} else if (content.saleorrent.endsWith("L")) {
					Intent i = new Intent(context, LandDetailsActivity.class);
					i.putExtra("mode", content.saleorrent);
					i.putExtra("object", content.jsonObject.toString());
					((Activity) context).startActivity(i);

				} else if (content.saleorrent.endsWith("I")) {
					Intent i = new Intent(context, IndustryDetailsActivity.class);
					i.putExtra("mode", content.saleorrent);
					i.putExtra("object", content.jsonObject.toString());
					((Activity) context).startActivity(i);

				}

			}
		});

		return view;
	}

}
