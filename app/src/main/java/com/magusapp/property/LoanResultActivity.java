package com.magusapp.property;

import java.text.DecimalFormat;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class LoanResultActivity extends Activity {

	double property_price = 0, down_payment = 0, mrta = 0, length = 0, annual = 0;

	TextView total_loan, annual_interest, length_mortgage, monthly_repay, total_payment, total_interest;

	double p = 0, r = 0, n = 0;

	double power;

	double monthly_repayment, c_total_payment, c_total_interest;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_loan_result);

		Intent i = getIntent();
		property_price = Double.parseDouble(i.getStringExtra("property_price"));
		down_payment = Double.parseDouble(i.getStringExtra("down_payment"));
		mrta = Double.parseDouble(i.getStringExtra("mrta"));
		length = Double.parseDouble(i.getStringExtra("length"));
		annual = Double.parseDouble(i.getStringExtra("annual"));

		total_loan = (TextView) findViewById(R.id.txt_total_loan);
		annual_interest = (TextView) findViewById(R.id.txt_annual);
		length_mortgage = (TextView) findViewById(R.id.txt_length);
		monthly_repay = (TextView) findViewById(R.id.txt_monthly);
		total_payment = (TextView) findViewById(R.id.txt_total_payment);
		total_interest = (TextView) findViewById(R.id.txt_total_interest);

		p = property_price - down_payment + mrta; // total loan amount
		n = length * 12; // total months
		r = annual / 1200; // monthly interest

		power = Math.pow(1 + r, n);
		monthly_repayment = (p * power * r) / (power - 1);
		c_total_payment = monthly_repayment * n;
		c_total_interest = c_total_payment - p;

		DecimalFormat df = new DecimalFormat("#,###,##0.00");

		total_loan.setText("RM " + df.format(p));
		annual_interest.setText(String.format("%.2f", annual) + " %");
		length_mortgage.setText(String.format("%d", (int) length) + " year(s)");
		monthly_repay.setText("RM " + df.format(monthly_repayment));
		total_payment.setText("RM " + df.format(c_total_payment));
		total_interest.setText("RM " + df.format(c_total_interest));
	}
}
