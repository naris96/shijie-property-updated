package com.magusapp.property;

import com.magusapp.property.utility.Alert;
import com.magusapp.property.utility.AppPreferences;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class OtherActivity extends Activity {

	String[] item = { "Login", "Register", "About Us", "Disclaimer", "Event", "Our Video", "Contact Us" };
	String[] item2 = { "Profile", "About Us", "Disclaimer", "Event", "Our Video", "Contact Us", "Logout" };

	String video;

	ListView lv;

	CustomListAdapter adapter;

	AppPreferences app_pref;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_other);

		Intent intent = getIntent();
		video = intent.getStringExtra("video");

		app_pref = new AppPreferences(this);

		lv = (ListView) findViewById(R.id.listView1);

		if (app_pref.isAuthorized()) {
			adapter = new CustomListAdapter(this, item2);
		} else {
			adapter = new CustomListAdapter(this, item);
		}

		lv.setAdapter(adapter);
		lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				switch (position) {
				case 0:
					if (app_pref.isAuthorized()) {
						Intent i1 = new Intent(OtherActivity.this, UpdateActivity.class);
						startActivity(i1);
					} else {
						Intent i = new Intent(OtherActivity.this, LoginActivity.class);
						startActivity(i);
					}
					break;
				case 1:
					if (app_pref.isAuthorized()) {
						Intent i2 = new Intent(OtherActivity.this, AboutActivity.class);
						i2.putExtra("type", "AboutUs");
						startActivity(i2);
					} else {
						Intent i1 = new Intent(OtherActivity.this, RegisterActivity.class);
						startActivity(i1);
					}

					break;
				case 2:

					if (app_pref.isAuthorized()) {
						Intent i4 = new Intent(OtherActivity.this, AboutActivity.class);
						i4.putExtra("type", "Disclaimer");
						startActivity(i4);
					} else {
						Intent i2 = new Intent(OtherActivity.this, AboutActivity.class);
						i2.putExtra("type", "AboutUs");
						startActivity(i2);
					}

					break;

				case 3:

					if (app_pref.isAuthorized()) {
						Intent i4 = new Intent(OtherActivity.this, EventActivity.class);
						startActivity(i4);
					} else {
						Intent i4 = new Intent(OtherActivity.this, AboutActivity.class);
						i4.putExtra("type", "Disclaimer");
						startActivity(i4);
					}

					break;

				case 4:

					if (app_pref.isAuthorized()) {
						Intent i6 = new Intent(OtherActivity.this, VideoActivity.class);
						i6.putExtra("video", video);
						startActivity(i6);
					} else {
						Intent i = new Intent(getApplicationContext(), EventActivity.class);
						startActivity(i);
					}

					break;

				case 5:

					if (app_pref.isAuthorized()) {
						Intent i3 = new Intent(OtherActivity.this, ContactActivity.class);
						startActivity(i3);
					} else {

						Intent i6 = new Intent(OtherActivity.this, VideoActivity.class);
						i6.putExtra("video", video);
						startActivity(i6);
					}

					break;

				case 6:

					if (app_pref.isAuthorized()) {
						app_pref.setUserDetails(null, null, null, null, null);

						Intent intent = new Intent(OtherActivity.this, MainActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						Alert.ShowAlertMessage("Successfully Logged Out", "See you again!", "Intent", intent,
								OtherActivity.this, OtherActivity.this);
					} else {
						Intent i3 = new Intent(OtherActivity.this, ContactActivity.class);
						startActivity(i3);
					}

					break;

				}
			}
		});

	}
}
