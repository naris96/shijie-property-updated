package com.magusapp.property;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CustomListAdapter extends BaseAdapter {

	// Declare Variables
	Context context;
	String[] name;
	LayoutInflater inflater;

	public CustomListAdapter(Context context, String[] name) {
		this.context = context;
		this.name = name;
	}

	@Override
	public int getCount() {
		return name.length;
	}

	@Override
	public Object getItem(int position) {
		return name[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		View view = convertView;

		LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		Log.e("Position", String.valueOf(position));

		if (view == null) {

			view = vi.inflate(R.layout.list_item, null);
		}
		TextView tName = (TextView) view.findViewById(R.id.textView1);

		tName.setText(name[position]);

		return view;

	}

}
