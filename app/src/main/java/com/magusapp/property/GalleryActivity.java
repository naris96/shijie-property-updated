package com.magusapp.property;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.magusapp.property.utility.Alert;
import com.magusapp.property.utility.ConnectionDetector;
import com.magusapp.property.utility.DecodeEntity;
import com.magusapp.property.utility.ServiceHandler;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ListView;

public class GalleryActivity extends Activity {

	private static final String TAG_type = "type";

	JSONObject object;

	ImageLoader imageLoader = ImageLoader.getInstance();
	DisplayImageOptions options;

	ProgressDialog pDialog;

	String jsonStr;

	String image;

	JSONArray jarray;

	ListView gv;

	ArrayList<MainDetails> details = new ArrayList<MainDetails>();

	Boolean isInternetPresent;
	ConnectionDetector connection_detector;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gallery);

		connection_detector = new ConnectionDetector(GalleryActivity.this);
		isInternetPresent = connection_detector.isConnectingToInternet();

		gv = (ListView) findViewById(R.id.listView);

		// image loader
		options = new DisplayImageOptions.Builder().cacheInMemory(false).cacheOnDisk(true).considerExifParams(true)
				.bitmapConfig(Bitmap.Config.RGB_565).build();

		if (!imageLoader.isInited()) {
			imageLoader.init(ImageLoaderConfiguration.createDefault(this));
		}

		if (isInternetPresent) {
			new GetContent().execute();
		} else {
			Alert.ShowAlertMessage("No internet connection.", "Information", "Finish", null, GalleryActivity.this,
					GalleryActivity.this);
		}

	}

	private class GetContent extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(GalleryActivity.this);
			pDialog.setMessage("Loading...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@SuppressWarnings("deprecation")
		@Override
		protected Void doInBackground(Void... arg0) {

			ServiceHandler sh = new ServiceHandler();
			HashMap<String, String> params = new HashMap<>();
			params.put(TAG_type, "gallery");
			jsonStr = sh.makeServiceCall("GET", params);

			if (jsonStr != null || !jsonStr.equals("")) {
				try {

					jarray = new JSONArray(jsonStr);

					for (int k = 0; k < jarray.length(); k++) {

						JSONObject c2 = jarray.getJSONObject(k);

						String title = DecodeEntity.decodeEntities(c2.getString("GelleryTitle"));

						String description = DecodeEntity.decodeEntities(c2.getString("Description"));

						image = DecodeEntity.decodeEntities(c2.getString("pictureURL"));

						String cover = DecodeEntity.decodeEntities(c2.getString("thumnail"));

						MainDetails cd = new MainDetails();
						cd.setName(title);
						cd.setDetails(description);
						cd.setCover(cover);
						cd.setImage(image);
						details.add(cd);

					}

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (pDialog.isShowing())
				pDialog.dismiss();

			if (jsonStr == null || jsonStr.equals("")) {

				Alert.alertDialog("Information", "No content for this page. We are working on it.",
						GalleryActivity.this);

			} else {

				gv.setAdapter(new GalleryAdapter(GalleryActivity.this, details, jsonStr));
			}

		}
	}
}
