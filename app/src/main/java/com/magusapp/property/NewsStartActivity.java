package com.magusapp.property;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class NewsStartActivity extends Activity {

	ImageView choose;
	TextView name1, name2;

	String news, info;
	String goclass;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_news_start);

		Intent i = getIntent();
		news = i.getStringExtra("news");
		info = i.getStringExtra("info");
		goclass = i.getStringExtra("goclass");

		if (goclass.equals("145")) {
			Intent intent = new Intent(NewsStartActivity.this, NewsActivity.class);
			startActivity(intent);
		}

		name1 = (TextView) findViewById(R.id.text1);
		name2 = (TextView) findViewById(R.id.text2);

		name1.setText(news);
		name2.setText(info);

		onClick();
	}

	public void onClick() {

		choose = (ImageView) findViewById(R.id.info);
		choose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent i = new Intent(NewsStartActivity.this, InfoActivity.class);
				startActivity(i);

			}
		});

		choose = (ImageView) findViewById(R.id.news);
		choose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent i = new Intent(NewsStartActivity.this, NewsActivity.class);
				startActivity(i);

			}
		});
	}
}
