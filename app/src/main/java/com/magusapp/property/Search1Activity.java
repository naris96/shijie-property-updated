package com.magusapp.property;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.magusapp.property.utility.Alert;
import com.magusapp.property.utility.ConnectionDetector;
import com.magusapp.property.utility.DecodeEntity;
import com.magusapp.property.utility.GPSTracker;
import com.magusapp.property.utility.ServiceHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class Search1Activity extends Activity {

	private static final String TAG_type = "type";
	private static final String TAG_mode = "mode";
	private static final String TAG_long = "long";
	private static final String TAG_lat = "lat";
	private static final String TAG_dist = "dist";

	private static final String TAG_title = "location";
	private static final String TAG_address = "road";
	private static final String TAG_longitude = "Longitude";
	private static final String TAG_latitude = "Latitude";
	private static final String TAG_project = "Project";
	private static final String TAG_zoning = "modetype";

	Spinner spnrProperty, spnrMode;
	ListView lv;
	TextView advance;

	String[] modeType = { "Sale", "Rent" };
	String[] propertyType = { "Residential", "Commercial", "Land", "Industry" };

	ArrayList<MainDetails> details1, details2, details3, details4;

	String getMode, getProperty;
	String newdate, price, saleorrent;

	boolean launch = false;

	Boolean isInternetPresent;
	ConnectionDetector connection_detector;

	GPSTracker gps;

	String jsonStr;
	JSONArray listing;
	JSONObject object;
	ProgressDialog pDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search1);

		gps = new GPSTracker(this);
		connection_detector = new ConnectionDetector(this);
		isInternetPresent = connection_detector.isConnectingToInternet();

		lv = (ListView) findViewById(R.id.listView1);
		advance = (TextView) findViewById(R.id.advance);

		getMode = "Sale";
		getProperty = "Residential";

		spnrMode = (Spinner) findViewById(R.id.spinner1);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, modeType);

		spnrMode.setAdapter(adapter);
		spnrMode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

				int position = spnrMode.getSelectedItemPosition();
				getMode = modeType[position];

				if (launch) {
					new GetContent().execute();
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});

		spnrProperty = (Spinner) findViewById(R.id.spinner2);
		ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, R.layout.spinner_item, propertyType);

		spnrProperty.setAdapter(adapter2);
		spnrProperty.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

				int position = spnrProperty.getSelectedItemPosition();
				getProperty = propertyType[position];

				if (launch) {
					if (getProperty.equals("Residential")) {
						lv.setAdapter(new SearchListAdapter(Search1Activity.this, details1));
						Log.e("size", String.valueOf(details1.size()));
					}
					if (getProperty.equals("Commercial")) {
						lv.setAdapter(new SearchListAdapter(Search1Activity.this, details2));
						Log.e("size", String.valueOf(details2.size()));
					}
					if (getProperty.equals("Land")) {
						lv.setAdapter(new SearchListAdapter(Search1Activity.this, details3));
						Log.e("size", String.valueOf(details3.size()));
					}
					if (getProperty.equals("Industry")) {
						lv.setAdapter(new SearchListAdapter(Search1Activity.this, details4));
						Log.e("size", String.valueOf(details4.size()));
					}
				}
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});

		advance.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), SearchActivity.class);
				startActivity(i);
			}

		});


		if (!gps.canGetLocation()) {
			showSettingsAlert();
		} else {
			if (isInternetPresent) {

				new GetContent().execute();
			} else {
				Alert.ShowAlertMessage("No internet connection.", "Information", "Finish", null, Search1Activity.this,
						Search1Activity.this);
			}
		}

	}

	/**
	 * Async task class to get json by making HTTP call
	 */
	private class GetContent extends AsyncTask<Void, Void, Void> {

		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
			pDialog = new ProgressDialog(Search1Activity.this);
			pDialog.setMessage("Loading...");
			pDialog.setCancelable(false);
			pDialog.show();

		}

		@Override
		protected Void doInBackground(Void... arg0) {

			ServiceHandler sh2 = new ServiceHandler();
			HashMap<String, String> params2 = new HashMap<>();
			params2.put(TAG_type, "nearby");
			params2.put(TAG_lat, String.valueOf(gps.getLatitude()));
			params2.put(TAG_long, String.valueOf(gps.getLongitude()));
			params2.put(TAG_dist, "3");

			params2.put(TAG_mode, getMode);

			jsonStr = sh2.makeServiceCall("GET", params2);

			details1 = new ArrayList<MainDetails>();
			details2 = new ArrayList<MainDetails>();
			details3 = new ArrayList<MainDetails>();
			details4 = new ArrayList<MainDetails>();

			if (jsonStr != null && !jsonStr.equals("")) {
				try {
					// Getting JSON Array node
					object = new JSONObject(jsonStr);
					listing = object.getJSONArray("listing");

					for (int i = 0; i < listing.length(); i++) {
						JSONObject c = listing.getJSONObject(i);

						String mode = DecodeEntity.decodeEntities(c.getString("modetype"));

						String location = DecodeEntity.decodeEntities(c.getString("location"));
						String type = DecodeEntity.decodeEntities(c.getString("type"));

						if (getMode.equals("Sale")) {
							price = DecodeEntity.decodeEntities(c.getString("SalePrice"));
						} else {
							price = DecodeEntity.decodeEntities(c.getString("RentPrice"));
						}

						String project = DecodeEntity.decodeEntities(c.getString("Project"));
						String update = DecodeEntity.decodeEntities(c.getString("UpdatedDate"));
						String img = DecodeEntity.decodeEntities(c.getString("PropertyURL"));

						if (!update.equals("")) {
							newdate = update.replace(update.substring(update.length() - 14), "");
						} else {
							newdate = "";
						}

						MainDetails cd;
						cd = new MainDetails();
						cd.setLocation(location);
						cd.setType(type);
						cd.setPrice(price);
						cd.setProject(project);
						cd.setUpdate(newdate);
						cd.setObject(c);
						cd.setSaleorrent(mode);
						cd.setImage(img);

						if (mode.endsWith("R")) {
							details1.add(cd);
						}

						if (mode.endsWith("C")) {
							details2.add(cd);
						}

						if (mode.endsWith("L")) {
							details3.add(cd);
						}

						if (mode.endsWith("I")) {
							details4.add(cd);
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			} else {
				Log.e("ServiceHandler", "Couldn't get any data from the url");
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			// Dismiss the progress dialog
			if (pDialog.isShowing())
				pDialog.dismiss();

			if (jsonStr != null || !jsonStr.equals("")) {

				launch = true;

				Log.e("size1", String.valueOf(details1.size()));
				Log.e("size2", String.valueOf(details2.size()));
				Log.e("size3", String.valueOf(details3.size()));
				Log.e("size4", String.valueOf(details4.size()));

				if (getProperty.equals("Residential")) {
					lv.setAdapter(new SearchListAdapter(Search1Activity.this, details1));
				}
				if (getProperty.equals("Commercial")) {
					lv.setAdapter(new SearchListAdapter(Search1Activity.this, details2));
				}
				if (getProperty.equals("Land")) {
					lv.setAdapter(new SearchListAdapter(Search1Activity.this, details3));
				}
				if (getProperty.equals("Industry")) {
					lv.setAdapter(new SearchListAdapter(Search1Activity.this, details4));
				}
			} else {
				Alert.ShowAlertMessage(
						"Please submit your requirement to us at buyer/tenant submission. Our dedicated REN will contact you soonest. \n世界地产谢谢您，请在 buyer/tenant 登记您的要求，我们专业的仲介员会尽快联络您。",
						"NO LISTING MATCH YOUR SEARCH", "Finish", null, Search1Activity.this, Search1Activity.this);
			}
		}
	}

	public void showSettingsAlert() {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(Search1Activity.this);

		alertDialog.setCancelable(false);

		alertDialog.setTitle("Location not found");

		alertDialog.setMessage("Unable to get your location. Do you want to turn on GPS?");

		alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
				startActivity(intent);
				finish();
			}
		});

		alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();

			}
		});

		alertDialog.show();
	}

}
