package com.magusapp.property;

import java.text.DecimalFormat;
import java.util.ArrayList;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AuctionAdapter extends BaseAdapter {

	ArrayList<MainDetails> details;
	Context context;

	ImageLoader imageLoader = ImageLoader.getInstance();
	DisplayImageOptions options;

	public AuctionAdapter(Context activity, ArrayList<MainDetails> det) {

		details = det;
		context = activity;

		options = new DisplayImageOptions.Builder().cacheInMemory(false).showImageForEmptyUri(R.drawable.no_image)
				.showImageOnFail(R.drawable.no_image).cacheOnDisk(true).considerExifParams(true)
				.bitmapConfig(Bitmap.Config.RGB_565).build();

		if (!imageLoader.isInited()) {
			imageLoader.init(ImageLoaderConfiguration.createDefault(context));
		}
	}

	@Override
	public int getCount() {
		return details.size();
	}

	@Override
	public Object getItem(int position) {
		return details.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;

		LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		if (view == null) {

			view = vi.inflate(R.layout.auction_list, null);
		}

		DecimalFormat df = new DecimalFormat("#,###,##0.00");

		ImageView img = (ImageView) view.findViewById(R.id.icon);
		TextView add = (TextView) view.findViewById(R.id.address);
		TextView type = (TextView) view.findViewById(R.id.type);
		TextView price = (TextView) view.findViewById(R.id.price);
		TextView date = (TextView) view.findViewById(R.id.date);

		final MainDetails content = details.get(position);

		add.setText(content.location);
		type.setText(content.type);
		date.setText(content.createddate);

		if (!content.price.matches("")) {
			price.setText("RM" + df.format(Double.parseDouble(content.price)));
		} else {
			price.setText("-");
		}

		imageLoader.displayImage(content.image, img, options);

		view.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent i = new Intent(context, AuctionDetailsActivity.class);
				i.putExtra("id", content.id);
				i.putExtra("title", content.location);
				i.putExtra("detail", content.details);
				i.putExtra("type", content.type);
				i.putExtra("price", content.price);
				i.putExtra("area", content.area);
				i.putExtra("date", content.createddate);
				i.putExtra("name", content.name);
				i.putExtra("phone", content.phone);
				i.putExtra("email", content.mail);
				i.putExtra("staff", content.cover);

				((Activity) context).startActivity(i);

			}
		});

		return view;
	}

}
