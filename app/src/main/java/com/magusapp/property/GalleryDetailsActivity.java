package com.magusapp.property;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;

import org.json.JSONArray;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class GalleryDetailsActivity extends Activity {

	private static final String TAG_id = "id";
	private static final String TAG_type = "type";

	String image;

	String title, detail, id = "";

	TextView txtTitle, txtDetail;
	ImageView img;
	ImageButton choose;

	JSONArray jarray;

	ProgressDialog pDialog;

	ImageLoader imageLoader = ImageLoader.getInstance();
	DisplayImageOptions options;

	File file;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gallery_details);

		options = new DisplayImageOptions.Builder().cacheInMemory(false).showImageForEmptyUri(R.drawable.no_image)
				.cacheOnDisk(true).considerExifParams(true).bitmapConfig(Bitmap.Config.RGB_565).build();

		if (!imageLoader.isInited()) {
			imageLoader.init(ImageLoaderConfiguration.createDefault(this));
		}

		Intent i = getIntent();
		title = i.getStringExtra("title");
		detail = i.getStringExtra("detail");
		image = i.getStringExtra("image");

		img = (ImageView) findViewById(R.id.imageView1);
		txtTitle = (TextView) findViewById(R.id.title);
		txtDetail = (TextView) findViewById(R.id.details);

		txtTitle.setText(title);
		txtDetail.setText(detail);

		imageLoader.displayImage(image, img, options);

		onClick();
	}

	public void onClick() {
		choose = (ImageButton) findViewById(R.id.SMSIcon);
		choose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.putExtra("address", "0144817777");
				intent.putExtra("sms_body", "Title: " + title + "\n" + detail);
				intent.setType("vnd.android-dir/mms-sms");
				startActivity(intent);

			}
		});
		choose = (ImageButton) findViewById(R.id.callIcon);
		choose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent = new Intent(Intent.ACTION_DIAL);
				intent.setData(Uri.parse("tel:" + "0125001048"));
				startActivity(intent);

			}
		});

		choose = (ImageButton) findViewById(R.id.emailIcon);
		choose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent i = new Intent(Intent.ACTION_SEND);
				i.setType("message/rfc822");
				i.putExtra(Intent.EXTRA_EMAIL, new String[] { "enquiries@lskworld.com" });
				i.putExtra(Intent.EXTRA_SUBJECT, "Enquiry From APP");
				i.putExtra(Intent.EXTRA_TEXT, "Title: " + title + "\n" + detail);
				startActivity(Intent.createChooser(i, "Send Email"));

			}
		});
		choose = (ImageButton) findViewById(R.id.shareIcon);
		choose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				final ProgressDialog saveProgressDialog = new ProgressDialog(GalleryDetailsActivity.this);
				saveProgressDialog.setMessage("Loading ...");
				saveProgressDialog.isIndeterminate();
				saveProgressDialog.setCancelable(false);

				saveProgressDialog.show();
				new Thread(new Runnable() {
					@Override
					public void run() {

						try {

							URL imageurl = new URL(image);
							Bitmap bitmap = BitmapFactory.decodeStream(imageurl.openConnection().getInputStream());

							String path = Environment.getExternalStorageDirectory().getAbsolutePath()
									+ "/penangrealty.png";
							file = new File(path);

							FileOutputStream out = new FileOutputStream(file);
							Log.d("in save()", "after outputstream");

							getResizedBitmap(bitmap).compress(Bitmap.CompressFormat.PNG, 50, out);
							out.flush();
							out.close();

						} catch (IOException e) {

						}

						Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
						whatsappIntent.setType("text/plain");
						whatsappIntent.putExtra(Intent.EXTRA_TEXT,
								"Title: " + title + "\n" + detail + "\n" + "http://onelink.to/hhx3s7");
						whatsappIntent.setType("image/png");
						whatsappIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
						startActivity(Intent.createChooser(whatsappIntent, "Share"));

						handler.sendEmptyMessage(0);
					}

					private Handler handler = new Handler() {
						@Override
						public void handleMessage(Message msg) {
							saveProgressDialog.dismiss();
						}
					};
				}).start();

			}
		});
	}

	public static Bitmap getResizedBitmap(Bitmap bm) {
		int width = bm.getWidth();
		int height = bm.getHeight();
		int newWidth = width * 50 / 100;
		int newHeight = height * 50 / 100;
		float scaleWidth = ((float) newWidth) / width;
		float scaleHeight = ((float) newHeight) / height;
		Matrix matrix = new Matrix();
		// RESIZE THE BIT MAP
		matrix.postScale(scaleWidth, scaleHeight);
		// RECREATE THE NEW BITMAP
		Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
		return resizedBitmap;
	}

}
