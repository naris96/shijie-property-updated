package com.magusapp.property;

import java.text.DecimalFormat;

import com.magusapp.property.utility.Alert;
import com.magusapp.property.utility.NumberTextWatcher;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class RPGTFragment extends Fragment {

	ImageButton calculate;
	EditText property_price, selling_price, year, legal_fee, agent_commission, admin_fee, repair_renovation, other;
	TextView total;

	double count_total;
	double num_legal_fee = 0, num_agent_commission = 0, num_admin = 0, num_reapir = 0, num_other = 0;

	DecimalFormat df;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.activity_rpgtfragment, container, false);

		getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		calculate = (ImageButton) rootView.findViewById(R.id.calculate);
		property_price = (EditText) rootView.findViewById(R.id.txt_property_price);
		selling_price = (EditText) rootView.findViewById(R.id.txt_selling_price);
		year = (EditText) rootView.findViewById(R.id.txt_year);
		legal_fee = (EditText) rootView.findViewById(R.id.txt_legal);
		agent_commission = (EditText) rootView.findViewById(R.id.txt_agent);
		admin_fee = (EditText) rootView.findViewById(R.id.txt_admin);
		repair_renovation = (EditText) rootView.findViewById(R.id.txt_repair);
		other = (EditText) rootView.findViewById(R.id.txt_other);
		total = (TextView) rootView.findViewById(R.id.txt_all);

		property_price.addTextChangedListener(new NumberTextWatcher(property_price));
		selling_price.addTextChangedListener(new NumberTextWatcher(selling_price));

		legal_fee.addTextChangedListener(new NumberTextWatcher(legal_fee));
		agent_commission.addTextChangedListener(new NumberTextWatcher(agent_commission));
		admin_fee.addTextChangedListener(new NumberTextWatcher(admin_fee));
		repair_renovation.addTextChangedListener(new NumberTextWatcher(repair_renovation));
		other.addTextChangedListener(new NumberTextWatcher(other));

		df = new DecimalFormat("#,###,##0.00");

		onClick();

		other.setOnEditorActionListener(new EditText.OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE || event.getAction() == KeyEvent.ACTION_DOWN) {

					if (legal_fee.getText().toString().replace(" ", "").matches("")) {
						num_legal_fee = 0;
					} else {
						num_legal_fee = Double.parseDouble(legal_fee.getText().toString().replace(",", ""));
					}

					if (agent_commission.getText().toString().replace(" ", "").matches("")) {
						num_agent_commission = 0;
					} else {
						num_agent_commission = Double
								.parseDouble(agent_commission.getText().toString().replace(",", ""));
					}

					if (admin_fee.getText().toString().replace(" ", "").matches("")) {
						num_admin = 0;
					} else {
						num_admin = Double.parseDouble(admin_fee.getText().toString().replace(",", ""));
					}

					if (repair_renovation.getText().toString().replace(" ", "").matches("")) {
						num_reapir = 0;
					} else {
						num_reapir = Double.parseDouble(repair_renovation.getText().toString().replace(",", ""));
					}

					if (other.getText().toString().replace(" ", "").matches("")) {
						num_other = 0;
					} else {
						num_other = Double.parseDouble(other.getText().toString().replace(",", ""));
					}

					count_total = num_legal_fee + num_agent_commission + num_admin + num_reapir + num_other;

					Log.e("count", String.valueOf(count_total));

					total.setText(df.format(count_total));
					InputMethodManager imm = (InputMethodManager) getActivity()
							.getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(other.getWindowToken(), 0);
					return true;
				}
				return false;
			}

		});

		legal_fee.setOnEditorActionListener(new EditText.OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE || event.getAction() == KeyEvent.ACTION_DOWN) {

					if (legal_fee.getText().toString().replace(" ", "").matches("")) {
						num_legal_fee = 0;
					} else {
						num_legal_fee = Double.parseDouble(legal_fee.getText().toString().replace(",", ""));
					}

					if (agent_commission.getText().toString().replace(" ", "").matches("")) {
						num_agent_commission = 0;
					} else {
						num_agent_commission = Double
								.parseDouble(agent_commission.getText().toString().replace(",", ""));
					}

					if (admin_fee.getText().toString().replace(" ", "").matches("")) {
						num_admin = 0;
					} else {
						num_admin = Double.parseDouble(admin_fee.getText().toString().replace(",", ""));
					}

					if (repair_renovation.getText().toString().replace(" ", "").matches("")) {
						num_reapir = 0;
					} else {
						num_reapir = Double.parseDouble(repair_renovation.getText().toString().replace(",", ""));
					}

					if (other.getText().toString().replace(" ", "").matches("")) {
						num_other = 0;
					} else {
						num_other = Double.parseDouble(other.getText().toString().replace(",", ""));
					}

					count_total = num_legal_fee + num_agent_commission + num_admin + num_reapir + num_other;

					Log.e("count", String.valueOf(count_total));

					total.setText(df.format(count_total));
					InputMethodManager imm = (InputMethodManager) getActivity()
							.getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(other.getWindowToken(), 0);
					return true;
				}
				return false;
			}

		});

		agent_commission.setOnEditorActionListener(new EditText.OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE || event.getAction() == KeyEvent.ACTION_DOWN) {

					if (legal_fee.getText().toString().replace(" ", "").matches("")) {
						num_legal_fee = 0;
					} else {
						num_legal_fee = Double.parseDouble(legal_fee.getText().toString().replace(",", ""));
					}

					if (agent_commission.getText().toString().replace(" ", "").matches("")) {
						num_agent_commission = 0;
					} else {
						num_agent_commission = Double
								.parseDouble(agent_commission.getText().toString().replace(",", ""));
					}

					if (admin_fee.getText().toString().replace(" ", "").matches("")) {
						num_admin = 0;
					} else {
						num_admin = Double.parseDouble(admin_fee.getText().toString().replace(",", ""));
					}

					if (repair_renovation.getText().toString().replace(" ", "").matches("")) {
						num_reapir = 0;
					} else {
						num_reapir = Double.parseDouble(repair_renovation.getText().toString().replace(",", ""));
					}

					if (other.getText().toString().replace(" ", "").matches("")) {
						num_other = 0;
					} else {
						num_other = Double.parseDouble(other.getText().toString().replace(",", ""));
					}

					count_total = num_legal_fee + num_agent_commission + num_admin + num_reapir + num_other;

					Log.e("count", String.valueOf(count_total));

					total.setText(df.format(count_total));
					InputMethodManager imm = (InputMethodManager) getActivity()
							.getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(other.getWindowToken(), 0);
					return true;
				}
				return false;
			}

		});

		admin_fee.setOnEditorActionListener(new EditText.OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE || event.getAction() == KeyEvent.ACTION_DOWN) {

					if (legal_fee.getText().toString().replace(" ", "").matches("")) {
						num_legal_fee = 0;
					} else {
						num_legal_fee = Double.parseDouble(legal_fee.getText().toString().replace(",", ""));
					}

					if (agent_commission.getText().toString().replace(" ", "").matches("")) {
						num_agent_commission = 0;
					} else {
						num_agent_commission = Double
								.parseDouble(agent_commission.getText().toString().replace(",", ""));
					}

					if (admin_fee.getText().toString().replace(" ", "").matches("")) {
						num_admin = 0;
					} else {
						num_admin = Double.parseDouble(admin_fee.getText().toString().replace(",", ""));
					}

					if (repair_renovation.getText().toString().replace(" ", "").matches("")) {
						num_reapir = 0;
					} else {
						num_reapir = Double.parseDouble(repair_renovation.getText().toString().replace(",", ""));
					}

					if (other.getText().toString().replace(" ", "").matches("")) {
						num_other = 0;
					} else {
						num_other = Double.parseDouble(other.getText().toString().replace(",", ""));
					}

					count_total = num_legal_fee + num_agent_commission + num_admin + num_reapir + num_other;

					Log.e("count", String.valueOf(count_total));

					total.setText(df.format(count_total));
					InputMethodManager imm = (InputMethodManager) getActivity()
							.getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(other.getWindowToken(), 0);
					return true;
				}
				return false;
			}

		});

		repair_renovation.setOnEditorActionListener(new EditText.OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE || event.getAction() == KeyEvent.ACTION_DOWN) {

					if (legal_fee.getText().toString().replace(" ", "").matches("")) {
						num_legal_fee = 0;
					} else {
						num_legal_fee = Double.parseDouble(legal_fee.getText().toString().replace(",", ""));
					}

					if (agent_commission.getText().toString().replace(" ", "").matches("")) {
						num_agent_commission = 0;
					} else {
						num_agent_commission = Double
								.parseDouble(agent_commission.getText().toString().replace(",", ""));
					}

					if (admin_fee.getText().toString().replace(" ", "").matches("")) {
						num_admin = 0;
					} else {
						num_admin = Double.parseDouble(admin_fee.getText().toString().replace(",", ""));
					}

					if (repair_renovation.getText().toString().replace(" ", "").matches("")) {
						num_reapir = 0;
					} else {
						num_reapir = Double.parseDouble(repair_renovation.getText().toString().replace(",", ""));
					}

					if (other.getText().toString().replace(" ", "").matches("")) {
						num_other = 0;
					} else {
						num_other = Double.parseDouble(other.getText().toString().replace(",", ""));
					}

					count_total = num_legal_fee + num_agent_commission + num_admin + num_reapir + num_other;

					Log.e("count", String.valueOf(count_total));

					total.setText(df.format(count_total));
					InputMethodManager imm = (InputMethodManager) getActivity()
							.getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(other.getWindowToken(), 0);
					return true;
				}
				return false;
			}

		});

		return rootView;
	}

	public void onClick() {
		calculate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (validation()) {

					Intent i = new Intent(getActivity(), RPGTResultActivity.class);
					i.putExtra("property_price", property_price.getText().toString().replace(",", ""));
					i.putExtra("selling_price", selling_price.getText().toString().replace(",", ""));
					i.putExtra("count_total", String.valueOf(count_total));
					i.putExtra("year", year.getText().toString());
					startActivity(i);
				}

			}
		});
	}

	public boolean validation() {
		String checkEmpty = "";
		property_price.setText(property_price.getText().toString().replaceAll(" ", "").trim());
		selling_price.setText(selling_price.getText().toString().replaceAll(" ", "").trim());
		year.setText(year.getText().toString().replaceAll(" ", "").trim());

		if (property_price.getText().toString().matches(""))
			checkEmpty = "-Property Price Required\n";
		if (selling_price.getText().toString().matches(""))
			checkEmpty += "-Selling Price Required\n";
		if (year.getText().toString().matches(""))
			checkEmpty += "-How many year Required";

		if (!checkEmpty.matches("")) {
			Alert.ShowAlertMessage(checkEmpty, "Complete these fields", null, null, getActivity(), getActivity());
			return false;
		} else {
			return true;
		}
	}
}
