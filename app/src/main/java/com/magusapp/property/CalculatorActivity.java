package com.magusapp.property;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class CalculatorActivity extends FragmentActivity {

	TextView select;
	LoanFragment firstFragment = new LoanFragment();
	LegalFragment secFragment = new LegalFragment();
	RPGTFragment thirdFragment = new RPGTFragment();
	TenancyFragment fourthFragment = new TenancyFragment();
	ROIActivity fifthFragment = new ROIActivity();
	AffordActivity sixthFragment = new AffordActivity();
	CoverterActivity seventhFragment = new CoverterActivity();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_calculator);

		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.replace(R.id.fragment_container, firstFragment);
		transaction.commit();

		onClick();

	}

	public void onClick() {
		select = (TextView) findViewById(R.id.loan);
		select.setBackgroundResource(R.drawable.selected_bg);
		select.setTextColor(getResources().getColor(R.color.red));

		select = (TextView) findViewById(R.id.loan);
		select.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				resetButton();
				select = (TextView) findViewById(R.id.loan);
				select.setBackgroundResource(R.drawable.selected_bg);
				select.setTextColor(getResources().getColor(R.color.red));

				FragmentManager mFragmentManager = getSupportFragmentManager();
				mFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
				FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
				transaction.replace(R.id.fragment_container, firstFragment);
				transaction.commit();

			}
		});

		select = (TextView) findViewById(R.id.legal);
		select.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				resetButton();
				select = (TextView) findViewById(R.id.legal);
				select.setBackgroundResource(R.drawable.selected_bg);
				select.setTextColor(getResources().getColor(R.color.red));

				FragmentManager mFragmentManager = getSupportFragmentManager();
				mFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
				FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
				transaction.replace(R.id.fragment_container, secFragment);
				transaction.commit();

			}
		});

		select = (TextView) findViewById(R.id.rpgt);
		select.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				resetButton();
				select = (TextView) findViewById(R.id.rpgt);
				select.setBackgroundResource(R.drawable.selected_bg);
				select.setTextColor(getResources().getColor(R.color.red));

				FragmentManager mFragmentManager = getSupportFragmentManager();
				mFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
				FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
				transaction.replace(R.id.fragment_container, thirdFragment);
				transaction.commit();

			}
		});

		select = (TextView) findViewById(R.id.tenancy);
		select.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				resetButton();
				select = (TextView) findViewById(R.id.tenancy);
				select.setBackgroundResource(R.drawable.selected_bg);
				select.setTextColor(getResources().getColor(R.color.red));

				FragmentManager mFragmentManager = getSupportFragmentManager();
				mFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
				FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
				transaction.replace(R.id.fragment_container, fourthFragment);
				transaction.commit();

			}
		});

		select = (TextView) findViewById(R.id.roi);
		select.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				resetButton();
				select = (TextView) findViewById(R.id.roi);
				select.setBackgroundResource(R.drawable.selected_bg);
				select.setTextColor(getResources().getColor(R.color.red));

				FragmentManager mFragmentManager = getSupportFragmentManager();
				mFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
				FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
				transaction.replace(R.id.fragment_container, fifthFragment);
				transaction.commit();

			}
		});

		select = (TextView) findViewById(R.id.affordability);
		select.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				resetButton();
				select = (TextView) findViewById(R.id.affordability);
				select.setBackgroundResource(R.drawable.selected_bg);
				select.setTextColor(getResources().getColor(R.color.red));

				FragmentManager mFragmentManager = getSupportFragmentManager();
				mFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
				FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
				transaction.replace(R.id.fragment_container, sixthFragment);
				transaction.commit();

			}
		});

		select = (TextView) findViewById(R.id.convert);
		select.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				resetButton();
				select = (TextView) findViewById(R.id.convert);
				select.setBackgroundResource(R.drawable.selected_bg);
				select.setTextColor(getResources().getColor(R.color.red));

				FragmentManager mFragmentManager = getSupportFragmentManager();
				mFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
				FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
				transaction.replace(R.id.fragment_container, seventhFragment);
				transaction.commit();

			}
		});
	}

	private void resetButton() {
		select = (TextView) findViewById(R.id.loan);
		select.setBackgroundResource(0);
		select.setTextColor(Color.WHITE);
		select = (TextView) findViewById(R.id.legal);
		select.setBackgroundResource(0);
		select.setTextColor(Color.WHITE);
		select = (TextView) findViewById(R.id.rpgt);
		select.setBackgroundResource(0);
		select.setTextColor(Color.WHITE);
		select = (TextView) findViewById(R.id.tenancy);
		select.setBackgroundResource(0);
		select.setTextColor(Color.WHITE);
		select = (TextView) findViewById(R.id.roi);
		select.setBackgroundResource(0);
		select.setTextColor(Color.WHITE);
		select = (TextView) findViewById(R.id.affordability);
		select.setBackgroundResource(0);
		select.setTextColor(Color.WHITE);
		select = (TextView) findViewById(R.id.convert);
		select.setBackgroundResource(0);
		select.setTextColor(Color.WHITE);
	}

}