package com.magusapp.property;

import java.util.HashMap;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.magusapp.property.utility.Alert;
import com.magusapp.property.utility.ConnectionDetector;
import com.magusapp.property.utility.ServiceHandler;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

public class RegisterActivity extends Activity {

	private static final String TAG_type = "type";
	private static final String TAG_email = "email";
	private static final String TAG_phone = "phone";
	private static final String TAG_name = "name";
	private static final String TAG_password = "password";

	String name, phone, email, password, result_type, result_message;
	String jsonStr;

	JSONObject object;

	ProgressDialog pDialog;

	EditText txtEmail, txtPassword, txtConfirm, txtName, txtPhone;
	Button submit;

	Boolean isInternetPresent;
	ConnectionDetector connection_detector;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);

		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		txtEmail = (EditText) findViewById(R.id.editEmail);
		txtPassword = (EditText) findViewById(R.id.editPassword);
		txtConfirm = (EditText) findViewById(R.id.editCPassword);
		txtName = (EditText) findViewById(R.id.editName);
		txtPhone = (EditText) findViewById(R.id.editPhone);
		submit = (Button) findViewById(R.id.submit);

		submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (validation()) {
					connection_detector = new ConnectionDetector(RegisterActivity.this);
					isInternetPresent = connection_detector.isConnectingToInternet();

					if (isInternetPresent) {
						new Register().execute();
					} else {
						Alert.ShowAlertMessage("No internet connection.", "Information", "Finish", null,
								RegisterActivity.this, RegisterActivity.this);
					}
				}
			}
		});
	}

	private class Register extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(RegisterActivity.this);
			pDialog.setMessage("Loading...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {

			email = txtEmail.getText().toString();
			password = txtPassword.getText().toString();
			name = txtName.getText().toString();
			phone = txtPhone.getText().toString();

			ServiceHandler sh = new ServiceHandler();
			HashMap<String, String> params = new HashMap<>();
			params.put(TAG_type, "createuser");
			params.put(TAG_email, email);
			params.put(TAG_phone, phone);
			params.put(TAG_name, name);
			params.put(TAG_password, password);
			jsonStr = sh.makeServiceCall("GET", params);

			try {

				JSONArray listing = new JSONArray(jsonStr);

				for (int i = 0; i < listing.length(); i++) {

					JSONObject c2 = listing.getJSONObject(i);
					result_type = c2.getString("@ResultType");
					result_message = c2.getString("@ResultMessage");

				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (pDialog.isShowing())
				pDialog.dismiss();

			if (!result_type.equals("Success")) {
				Alert.ShowAlertMessage(result_message, result_type, null, null, RegisterActivity.this,
						RegisterActivity.this);
				Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
			} else {

				Alert.ShowAlertMessage(result_message, "Informations", null, null, RegisterActivity.this,
						RegisterActivity.this);

			}
		}

	}

	private boolean validation() {
		// LEVEL 1 - CHECK EMPTY
		String checkEmpty = "";
		txtEmail.setText(txtEmail.getText().toString().replaceAll(" ", "").trim());
		txtPassword.setText(txtPassword.getText().toString().replaceAll(" ", "").trim());
		txtConfirm.setText(txtConfirm.getText().toString().replaceAll(" ", "").trim());
		txtName.setText(txtName.getText().toString().replaceAll(" ", "").trim());
		txtPhone.setText(txtPhone.getText().toString().replaceAll(" ", "").trim());

		if (txtName.getText().toString().matches(""))
			checkEmpty = "-Name Required\n";
		if (txtPhone.getText().toString().matches(""))
			checkEmpty += "-Phone Number Required\n";
		if (txtEmail.getText().toString().matches(""))
			checkEmpty += "-Email Address Required\n";
		if (txtPassword.getText().toString().matches(""))
			checkEmpty += "-Password Required\n";
		if (txtConfirm.getText().toString().matches(""))
			checkEmpty += "-Confirm Password Required\n";

		if (!checkEmpty.matches("")) {
			showMessageForValidation(checkEmpty, "Complete these fields");
			return false;
		} else {
			// LEVEL 2 - CHECK FIELDS
			String fieldsError = "";

			if (!validEmail(txtEmail.getText().toString()))
				fieldsError = "-Email address not valid\n";
			if (txtPassword.getText().length() < 8)
				fieldsError += "-Passwords must more than 8 characters\n";
			if (txtPassword.getText().length() > 15)
				fieldsError += "-Passwords must not more than 15 characters\n";
			if (!txtPassword.getText().toString().matches(txtConfirm.getText().toString()))
				fieldsError += "-Password and Confirm Password must be the same\n";

			if (!fieldsError.matches("")) {
				showMessageForValidation(fieldsError, "Fields Error");
				return false;
			} else {
				// Log.i("SUCESS", "SUCCESS");
				return true;
			}
		}

	}

	private boolean validEmail(String email) {
		Pattern pattern = Patterns.EMAIL_ADDRESS;
		return pattern.matcher(email).matches();
	}

	private void showMessageForValidation(String theMessage, String theTitle) {
		Alert.ShowAlertMessage(theMessage, theTitle, null, null, RegisterActivity.this, RegisterActivity.this);
	}

}
