package com.magusapp.property;

import com.magusapp.property.utility.Alert;
import com.magusapp.property.utility.AppPreferences;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class RecentOuterActivity extends Activity {

	ImageView choose;
	AppPreferences app_pref;
	TextView name1, name2;

	String new1, new2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_recent_outer);

		Intent i = getIntent();
		new1 = i.getStringExtra("new1");
		new2 = i.getStringExtra("new2");

		name1 = (TextView) findViewById(R.id.text1);
		name2 = (TextView) findViewById(R.id.text2);

		name1.setText(new1);
		name2.setText(new2);

		app_pref = new AppPreferences(this);
		onClick();
	}

	public void onClick() {
		choose = (ImageView) findViewById(R.id.recent_public);
		choose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (app_pref.getUserName() == null) {
					Intent intent = new Intent(RecentOuterActivity.this, LoginActivity.class);
					Alert.ShowAlertMessage("Please Login to view Recently Transacted", "Information", "Intent", intent,
							RecentOuterActivity.this, RecentOuterActivity.this);
				} else {
					Intent i = new Intent(RecentOuterActivity.this, RecentActivity.class);
					startActivity(i);
				}

			}
		});

		choose = (ImageView) findViewById(R.id.lsk);
		choose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (!app_pref.isAuthorized()) {
					Intent intent = new Intent(RecentOuterActivity.this, LoginActivity.class);
					Alert.ShowAlertMessage("Please Login to view Recently Transacted", "Alert", "Intent", intent,
							RecentOuterActivity.this, RecentOuterActivity.this);
				} else {
					Intent i = new Intent(RecentOuterActivity.this, RecentLSKActivity.class);
					startActivity(i);
				}

			}
		});
	}
}
