package com.magusapp.property;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.magusapp.property.utility.Alert;
import com.magusapp.property.utility.ConnectionDetector;
import com.magusapp.property.utility.DecodeEntity;
import com.magusapp.property.utility.ServiceHandler;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ListView;

public class EventActivity extends Activity {

	private static final String TAG_type = "type";

	JSONObject object;
	ProgressDialog pDialog;

	ListView lv;

	ArrayList<MainDetails> details = new ArrayList<MainDetails>();

	JSONArray jarray;

	String jsonStr;
	String type;
	String image;
	String newdate, newdate2;

	Boolean isInternetPresent;
	ConnectionDetector connection_detector;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_selection);

		connection_detector = new ConnectionDetector(EventActivity.this);
		isInternetPresent = connection_detector.isConnectingToInternet();

		lv = (ListView) findViewById(R.id.listView1);

		if (isInternetPresent) {
			new GetContent().execute();
		} else {
			Alert.ShowAlertMessage("No internet connection.", "Information", "Finish", null, EventActivity.this,
					EventActivity.this);
		}
	}

	private class GetContent extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(EventActivity.this);
			pDialog.setMessage("Loading...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {

			ServiceHandler sh = new ServiceHandler();
			HashMap<String, String> params = new HashMap<>();
			params.put(TAG_type, "event");

			jsonStr = sh.makeServiceCall("GET", params);

			if (jsonStr != null || !jsonStr.equals("")) {
				try {

					jarray = new JSONArray(jsonStr);

					for (int k = 0; k < jarray.length(); k++) {

						JSONObject c2 = jarray.getJSONObject(k);

						String id = DecodeEntity.decodeEntities(c2.getString("EventId"));
						String name = DecodeEntity.decodeEntities(c2.getString("Name"));
						String venue = DecodeEntity.decodeEntities(c2.getString("Venue"));
						String startdate = DecodeEntity.decodeEntities(c2.getString("StartDateTime"));
						String enddate = DecodeEntity.decodeEntities(c2.getString("EndDateTime"));
						String desc = DecodeEntity.decodeEntities(c2.getString("Details"));
						String cover = c2.getString("PropertyURL");

						if (!startdate.equals("")) {
							newdate = startdate.replace(startdate.substring(startdate.length() - 14), "");
							newdate2 = enddate.replace(enddate.substring(enddate.length() - 14), "");
						} else {
							newdate = "";
							newdate2 = "";
						}

						MainDetails cd = new MainDetails();
						cd.setID(id);
						cd.setName(name);
						cd.setLocation(venue);
						cd.setCreatedDate(startdate);
						cd.setArea(enddate);
						cd.setDetails(desc);
						cd.setCover(cover);
						details.add(cd);

					}

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (pDialog.isShowing())
				pDialog.dismiss();

			if (jsonStr != null || !jsonStr.equals("")) {

				if (jarray.length() > 0) {
					lv.setAdapter(new EventAdapter(EventActivity.this, details));
				} else {
					Alert.ShowAlertMessage("No content for this page. We are working on it.", "Information", "Finish",
							null, EventActivity.this, EventActivity.this);
				}
			} else {
				Alert.ShowAlertMessage("No content for this page. We are working on it.", "Information", "Finish", null,
						EventActivity.this, EventActivity.this);
			}

		}

	}

}
