package com.magusapp.property;

import java.io.Serializable;

import org.json.JSONObject;

@SuppressWarnings("serial")
public class MainDetails implements Serializable {

	String location;
	String type;
	String price;
	String project;
	String cover;
	String update;
	String image;
	double latitude;
	double longitude;
	String name;
	String details;
	String createddate;
	String phone;
	String area;
	String buildUp;
	String id;
	String mail;

	JSONObject jsonObject;
	String saleorrent;
	float distance;

	public void setLocation(String location) {
		this.location = location;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public void setCover(String cover) {
		this.cover = cover;
	}

	public void setUpdate(String update) {
		this.update = update;
	}

	public void setID(String id) {
		this.id = id;
	}

	public void setObject(JSONObject jsonObject) {
		this.jsonObject = jsonObject;
	}

	public void setSaleorrent(String saleorrent) {
		this.saleorrent = saleorrent;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public void setEmail(String mail) {
		this.mail = mail;
	}

	public void setDistance(float distance) {
		this.distance = distance;
	}

	public float getDistance() {
		return distance;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public void setCreatedDate(String createddate) {
		this.createddate = createddate;
	}

	public void setBuildUp(String buildUp) {
		this.buildUp = buildUp;
	}

}