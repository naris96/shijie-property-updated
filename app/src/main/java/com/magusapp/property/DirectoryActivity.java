package com.magusapp.property;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.costum.android.widget.LoadMoreListView;
import com.magusapp.property.utility.Alert;
import com.magusapp.property.utility.ConnectionDetector;
import com.magusapp.property.utility.DecodeEntity;
import com.magusapp.property.utility.ServiceHandler;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;

public class DirectoryActivity extends Activity {

	private static final String TAG_type = "type";

	JSONObject object;
	ProgressDialog pDialog;

	LoadMoreListView lv;
	TextView tv;

	ArrayList<MainDetails> details = new ArrayList<MainDetails>();

	JSONArray jarray;

	String jsonStr;
	String type;
	String image;
	String newdate;

	int total;

	Boolean isInternetPresent;
	ConnectionDetector connection_detector;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_list);

		connection_detector = new ConnectionDetector(DirectoryActivity.this);
		isInternetPresent = connection_detector.isConnectingToInternet();

		Intent i = getIntent();
		type = i.getStringExtra("type");

		lv = (LoadMoreListView) findViewById(R.id.myListView);
		tv = (TextView) findViewById(R.id.textView1);

		tv.setText("Property List");

		if (isInternetPresent) {
			new GetContent().execute();
		} else {
			Alert.ShowAlertMessage("No internet connection.", "Information", "Finish", null, DirectoryActivity.this,
					DirectoryActivity.this);
		}
	}

	private class GetContent extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(DirectoryActivity.this);
			pDialog.setMessage("Loading...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@SuppressWarnings("deprecation")
		@Override
		protected Void doInBackground(Void... arg0) {

			ServiceHandler sh = new ServiceHandler();
			HashMap<String, String> params = new HashMap<>();
			params.put(TAG_type, type);
			jsonStr = sh.makeServiceCall("GET", params);

			if (jsonStr != null || !jsonStr.equals("")) {
				try {

					jarray = new JSONArray(jsonStr);

					for (int k = 0; k < jarray.length(); k++) {

						JSONObject c2 = jarray.getJSONObject(k);

						String id = DecodeEntity.decodeEntities(c2.getString("ProjectId"));
						String name = DecodeEntity.decodeEntities(c2.getString("ProjectName"));
						String location = DecodeEntity.decodeEntities(c2.getString("ProjectLocation"));
						String pro_detail = DecodeEntity.decodeEntities(c2.getString("ProjectDetails"));
						String date = DecodeEntity.decodeEntities(c2.getString("CreateDate"));
						image = DecodeEntity.decodeEntities(c2.getString("PropertyURL"));

						if (!date.equals("")) {
							newdate = date.replace(date.substring(date.length() - 14), "");
						} else {
							newdate = "";
						}

						MainDetails cd = new MainDetails();
						cd.setProject(name);
						cd.setLocation(location);
						cd.setDetails(pro_detail);
						cd.setCreatedDate(date);
						cd.setID(id);
						cd.setImage(image);
						details.add(cd);

					}

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (pDialog.isShowing())
				pDialog.dismiss();

			if (jsonStr != null || !jsonStr.equals("")) {

				if (jarray.length() > 0) {
					lv.setAdapter(new DirectoryList(DirectoryActivity.this, details));
				} else {
					Alert.ShowAlertMessage("No content for this page. We are working on it.", "Information", "Finish",
							null, DirectoryActivity.this, DirectoryActivity.this);
				}
			} else {
				Alert.ShowAlertMessage("No content for this page. We are working on it.", "Information", "Finish", null,
						DirectoryActivity.this, DirectoryActivity.this);
			}

		}

	}

}
