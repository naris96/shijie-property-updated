package com.magusapp.property;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.magusapp.property.utility.AlbumStorageDirFactory;
import com.magusapp.property.utility.BaseAlbumDirFactory;
import com.magusapp.property.utility.FroyoAlbumDirFactory;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.Toast;

public class SelectPhotoActivity extends Activity {

	private static int RESULT_LOAD_IMAGE = 1;
	private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
	public static final int MEDIA_TYPE_IMAGE = 1;

	Uri fileUri; // file url to store image/video
	ImageButton select_photo, take_photo, virtual_room;

	private static final String JPEG_FILE_PREFIX = "IMG_";
	private static final String JPEG_FILE_SUFFIX = ".jpg";

	private AlbumStorageDirFactory mAlbumStorageDirFactory = null;

	String mCurrentPhotoPath;
	String picturePath;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_select_photo);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
			mAlbumStorageDirFactory = new FroyoAlbumDirFactory();
		} else {
			mAlbumStorageDirFactory = new BaseAlbumDirFactory();
		}

		select_photo = (ImageButton) findViewById(R.id.selectphoto);
		take_photo = (ImageButton) findViewById(R.id.takephoto);

		select_photo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent();
				intent.setType("image/*");
				intent.setAction(Intent.ACTION_GET_CONTENT);
				startActivityForResult(intent, RESULT_LOAD_IMAGE);

			}

		});

		take_photo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				dispatchTakePictureIntent(CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
			}

		});

		// Checking camera availability
		if (!isDeviceSupportCamera()) {
			Toast.makeText(getApplicationContext(), "Sorry! Your device doesn't support camera", Toast.LENGTH_LONG)
					.show();
			// will close the app if the device does't have camera
			finish();
		}
	}

	// Checking device has camera hardware or not
	private boolean isDeviceSupportCamera() {
		if (getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
			// this device has a camera
			return true;
		} else {
			// no camera on this device
			return false;
		}
	}

	// Here we store the file url as it will be null after returning from camera
	// app
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		// save file url in bundle as it will be null on scren orientation
		// changes
		outState.putParcelable("file_uri", fileUri);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);

		// get the file url
		fileUri = savedInstanceState.getParcelable("file_uri");
	}

	// Receiving activity result method will be called after closing the camera
	@SuppressLint("NewApi")
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// if the result is capturing Image
		if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				// successfully captured the image
				galleryAddPic();

				Log.e("currenet", mCurrentPhotoPath);

				Intent i = new Intent();
				i.putExtra("fileUri", mCurrentPhotoPath);
				setResult(RESULT_OK, i);
				finish();

			} else if (resultCode == RESULT_CANCELED) {
				// user cancelled Image capture
				Toast.makeText(getApplicationContext(), "You have cancelled image capture", Toast.LENGTH_SHORT).show();

				finish();

			} else {
				// failed to capture image
				Toast.makeText(getApplicationContext(), "Sorry! Failed to capture image", Toast.LENGTH_SHORT).show();
			}

		}

		// pick photo
		if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data && data.getData() != null) {

			Uri selectedImage = data.getData();
			Log.e("uri", selectedImage.toString());

			if (Build.VERSION.SDK_INT < 11) {
				picturePath = RealPathUtil.getRealPathFromURI_BelowAPI11(this, selectedImage);
			} else if (Build.VERSION.SDK_INT < 19) {
				picturePath = RealPathUtil.getRealPathFromURI_API11to18(this, selectedImage);
			} else {
				picturePath = RealPathUtil.getRealPathFromURI_API19(this, selectedImage);
			}
			Log.e("path", picturePath);

			Intent i = new Intent();
			i.putExtra("fileUri", picturePath);
			setResult(RESULT_OK, i);
			finish();

		}
	}

	/* Photo album for this application */
	private String getAlbumName() {
		return getString(R.string.app_name);
	}

	private File getAlbumDir() {
		File storageDir = null;

		if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {

			storageDir = mAlbumStorageDirFactory.getAlbumStorageDir(getAlbumName());

			if (storageDir != null) {
				if (!storageDir.mkdirs()) {
					if (!storageDir.exists()) {
						Log.e("CameraSample", "failed to create directory");
						return null;
					}
				}
			}

		} else {
			Log.e(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
		}

		return storageDir;
	}

	private File createImageFile() throws IOException {
		// Create an image file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		String imageFileName = JPEG_FILE_PREFIX + timeStamp + "_";
		File albumF = getAlbumDir();
		File imageF = File.createTempFile(imageFileName, JPEG_FILE_SUFFIX, albumF);
		return imageF;
	}

	private File setUpPhotoFile() throws IOException {

		File f = createImageFile();
		mCurrentPhotoPath = f.getAbsolutePath();

		return f;
	}

	private void galleryAddPic() {
		Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
		File f = new File(mCurrentPhotoPath);
		Uri contentUri = Uri.fromFile(f);
		mediaScanIntent.setData(contentUri);
		this.sendBroadcast(mediaScanIntent);
	}

	private void dispatchTakePictureIntent(int actionCode) {
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		switch (actionCode) {
		case CAMERA_CAPTURE_IMAGE_REQUEST_CODE:
			File f = null;

			try {
				f = setUpPhotoFile();
				mCurrentPhotoPath = f.getAbsolutePath();
				takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
			} catch (IOException e) {
				e.printStackTrace();
				f = null;
				mCurrentPhotoPath = null;
			}
			break;

		default:
			break;
		} // switch
		takePictureIntent.setClassName("com.android.camera", "com.android.camera.Camera");
		startActivityForResult(takePictureIntent, actionCode);

	}

}
