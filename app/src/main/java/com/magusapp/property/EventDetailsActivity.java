package com.magusapp.property;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.magusapp.property.utility.Alert;
import com.magusapp.property.utility.ConnectionDetector;
import com.magusapp.property.utility.ServiceHandler;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.PageIndicator;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class EventDetailsActivity extends Activity {

	String id = "", name, desc, venue, startdate, enddate;

	private static final String TAG_id = "id";
	private static final String TAG_type = "type";

	String json2 = "";

	TextView desc_t, date_t, name_t, venue_t, end_t;
	ImageView image;

	JSONArray jarray;

	ProgressDialog pDialog;

	ImageLoader imageLoader = ImageLoader.getInstance();
	DisplayImageOptions options;
	ViewPager pager;
	PageIndicator mIndicator;
	CirclePageIndicator indicator;

	ArrayList<MainDetails> details = new ArrayList<MainDetails>();

	Boolean isInternetPresent;
	ConnectionDetector connection_detector;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_event_details);

		Intent i = getIntent();
		id = i.getStringExtra("id");
		name = i.getStringExtra("name");
		desc = i.getStringExtra("detail");
		venue = i.getStringExtra("venue");
		startdate = i.getStringExtra("date");
		enddate = i.getStringExtra("enddate");

		options = new DisplayImageOptions.Builder().cacheInMemory(false).cacheOnDisk(true).considerExifParams(true)
				.showImageForEmptyUri(R.drawable.no_image).bitmapConfig(Bitmap.Config.RGB_565).build();

		if (!imageLoader.isInited()) {
			imageLoader.init(ImageLoaderConfiguration.createDefault(this));
		}

		indicator = (CirclePageIndicator) findViewById(R.id.indicator);
		image = (ImageView) findViewById(R.id.imageView1);
		pager = (ViewPager) findViewById(R.id.gallery_pager);

		desc_t = (TextView) findViewById(R.id.details);
		date_t = (TextView) findViewById(R.id.startdate);
		end_t = (TextView) findViewById(R.id.enddate);
		name_t = (TextView) findViewById(R.id.title);
		venue_t = (TextView) findViewById(R.id.venue);

		desc_t.setText(desc);
		date_t.setText(startdate);
		end_t.setText(enddate);
		venue_t.setText(venue);
		name_t.setText(name);

		connection_detector = new ConnectionDetector(EventDetailsActivity.this);
		isInternetPresent = connection_detector.isConnectingToInternet();

		if (isInternetPresent) {
			new GetContent().execute();
		} else {
			Alert.ShowAlertMessage("No internet connection.", "Information", "Finish", null, EventDetailsActivity.this,
					EventDetailsActivity.this);
		}
	}

	private class GetContent extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(EventDetailsActivity.this);
			pDialog.setMessage("Loading...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@SuppressWarnings("deprecation")
		@Override
		protected Void doInBackground(Void... arg0) {

			ServiceHandler sh4 = new ServiceHandler();
			HashMap<String, String> params4 = new HashMap<>();
			params4.put(TAG_type, "picture");
			params4.put(TAG_id, id);

			json2 = sh4.makeServiceCall("GET", params4);

			if (!json2.equals("")) {
				try {

					jarray = new JSONArray(json2);

					if (!json2.contains("@ResultMessage")) {

						for (int j = 0; j < jarray.length(); j++) {
							JSONObject c = jarray.getJSONObject(j);
							String img = c.getString("pictureURL");

							MainDetails md = new MainDetails();
							md.setImage(img);
							md.setCover(json2);
							details.add(md);
						}
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				Log.e("empty", "empty");
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (pDialog.isShowing())
				pDialog.dismiss();

			if (!json2.contains("@ResultMessage") && !json2.equals("") && jarray.length() != 0) {
				pager.setAdapter(new ImagePagerAdapter(details));
				mIndicator = indicator;

				indicator.setViewPager(pager);
				indicator.setSnap(true);

				image.setVisibility(View.GONE);
			} else {
				pager.setVisibility(View.GONE);
				indicator.setVisibility(View.GONE);
				image.setVisibility(View.VISIBLE);
			}
		}

	}

	private class ImagePagerAdapter extends PagerAdapter {

		ArrayList<MainDetails> details;
		private LayoutInflater inflater;

		ImagePagerAdapter(ArrayList<MainDetails> det) {
			this.details = det;
			inflater = getLayoutInflater();
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			((ViewPager) container).removeView((View) object);
		}

		@Override
		public void finishUpdate(View container) {
		}

		@Override
		public int getCount() {
			return details.size();
		}

		@Override
		public Object instantiateItem(ViewGroup view, final int position) {
			final MainDetails content = details.get(position);
			View imageLayout = inflater.inflate(R.layout.pager, view, false);
			ImageView imageView = (ImageView) imageLayout.findViewById(R.id.image);
			imageLoader.displayImage(content.image, imageView, options);

			((ViewPager) view).addView(imageLayout, 0);

			imageLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent i = new Intent(EventDetailsActivity.this, DisplayPhotoActivity.class);
					i.putExtra("position", position);
					i.putExtra("image", content.cover);
					startActivity(i);
				}
			});

			return imageLayout;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view.equals(object);
		}

		@Override
		public void restoreState(Parcelable state, ClassLoader loader) {
		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void startUpdate(View container) {
		}
	}
}