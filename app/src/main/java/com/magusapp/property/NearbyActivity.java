package com.magusapp.property;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.devsmart.android.ui.HorizontalListView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.magusapp.property.utility.Alert;
import com.magusapp.property.utility.ConnectionDetector;
import com.magusapp.property.utility.DecodeEntity;
import com.magusapp.property.utility.GPSTracker;
import com.magusapp.property.utility.ServiceHandler;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import android.*;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class NearbyActivity extends AppCompatActivity implements OnMapReadyCallback {

	private static final String TAG_type = "type";
	private static final String TAG_mode = "mode";
	private static final String TAG_long = "long";
	private static final String TAG_lat = "lat";
	private static final String TAG_dist = "dist";

	private static final String TAG_title = "location";
	private static final String TAG_address = "road";
	private static final String TAG_longitude = "Longitude";
	private static final String TAG_latitude = "Latitude";
	private static final String TAG_project = "Project";
	private static final String TAG_zoning = "modetype";

	ProgressDialog pDialog;

	ArrayList<MainDetails> details1, details2, details3, details4, newDetails;

	JSONObject object;

	Boolean isInternetPresent;

	ConnectionDetector connection_detector;

	private GoogleMap googleMap;

	Marker markers[];

	String[] select = { "SALE", "RENT" };

	String json2, jsonStr2;
	JSONArray listing;

	String mode;
	String setSelected;

	int selected_mode = 0;
	int choose_distance = 1;

	boolean hide = false;

	GPSTracker gps;

	HorizontalListView hlist;
	ImageView arrow;
	LinearLayout linear;
	TextView saleorrent, residential, commercial, land, industry, size, alert;
	SeekBar seekdistance;

	private static final int SPLASH_SHOW_TIME = 1000;

	ImageLoader imageLoader = ImageLoader.getInstance();
	DisplayImageOptions options;

	RelativeLayout layout1, layout2;

	HashMap<Marker, MainDetails> haspMap = new HashMap<Marker, MainDetails>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_nearby);

		connection_detector = new ConnectionDetector(NearbyActivity.this);
		isInternetPresent = connection_detector.isConnectingToInternet();

		mode = "Sale";
		setSelected = "Residential";

		gps = new GPSTracker(this);

		hlist = (HorizontalListView) findViewById(R.id.list);
		linear = (LinearLayout) findViewById(R.id.linear);
		arrow = (ImageView) findViewById(R.id.arrow);
		residential = (TextView) findViewById(R.id.residential);
		commercial = (TextView) findViewById(R.id.commercial);
		land = (TextView) findViewById(R.id.land);
		industry = (TextView) findViewById(R.id.industry);
		saleorrent = (TextView) findViewById(R.id.saleorrent);
		size = (TextView) findViewById(R.id.size);
		alert = (TextView) findViewById(R.id.textView1);
		layout1 = (RelativeLayout) findViewById(R.id.RelativeLayout1);
		layout2 = (RelativeLayout) findViewById(R.id.layout);
		seekdistance = (SeekBar) findViewById(R.id.seekBar2);

		size.setText(String.valueOf(choose_distance) + " KM");

		options = new DisplayImageOptions.Builder().cacheInMemory(false).cacheOnDisk(true).considerExifParams(true)
				.showImageForEmptyUri(R.drawable.no_image).bitmapConfig(Bitmap.Config.RGB_565).build();

		if (!imageLoader.isInited()) {
			imageLoader.init(ImageLoaderConfiguration.createDefault(this));
		}

		onClick();

		seekdistance.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				choose_distance = progress + 1;
				size.setText(String.valueOf(choose_distance) + " KM");
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

				new GetContent().execute();
			}
		});



		if (!gps.canGetLocation()) {
			showSettingsAlert();
		} else {
			MapFragment mapFragment =
					(MapFragment) getFragmentManager().findFragmentById(R.id.map);
			mapFragment.getMapAsync(this);

			if (isInternetPresent) {

				new GetContent().execute();
			} else {
				Alert.ShowAlertMessage("No internet connection.", "Information", "Finish", null, NearbyActivity.this,
						NearbyActivity.this);
			}
		}
	}

	@Override
	public void onMapReady(GoogleMap map) {
		googleMap = map;

		MarkerOptions marker = new MarkerOptions()
				.position(new LatLng(gps.getLatitude(), gps.getLongitude()));
		//	marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.map_mark));
		map.addMarker(marker);

		if (ContextCompat.checkSelfPermission(this,  android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
			googleMap.setMyLocationEnabled(true);
		} else{

			ActivityCompat.requestPermissions(this, new String[]{ android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);
		}

		map.moveCamera(CameraUpdateFactory.newLatLngZoom(
				new LatLng(gps.getLatitude(), gps.getLongitude()), 15));

	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		switch (requestCode) {
			case 1:
			if (grantResults.length <= 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
				if (ContextCompat.checkSelfPermission(this,  android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
					googleMap.setMyLocationEnabled(true);
				}
			}
			break;
		}
	}

	private void resetButton() {
		residential.setBackgroundResource(R.drawable.background_grey);
		residential.setTextColor(Color.BLACK);
		commercial.setBackgroundResource(R.drawable.background_grey);
		commercial.setTextColor(Color.BLACK);
		land.setBackgroundResource(R.drawable.background_grey);
		land.setTextColor(Color.BLACK);
		industry.setBackgroundResource(R.drawable.background_grey);
		industry.setTextColor(Color.BLACK);
	}

	public void onClick() {
		residential.setBackgroundResource(R.drawable.background_greyfull);
		residential.setTextColor(Color.WHITE);

		residential.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				resetButton();
				if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
					residential.setBackgroundDrawable(ContextCompat.getDrawable(NearbyActivity.this, R.drawable.background_greyfull));
				} else {
					residential.setBackground(ContextCompat.getDrawable(NearbyActivity.this, R.drawable.background_greyfull));
				}
				residential.setTextColor(Color.WHITE);
				setSelected = "Residential";

				if (googleMap != null) {
					googleMap.clear();

				}

				hide = false;
				arrow.setImageResource(R.drawable.down);

				if (!details1.isEmpty()) {
					alert.setVisibility(View.GONE);
					hlist.setVisibility(View.VISIBLE);
					setMarkers(details1);
					hlist.setAdapter(new GalleryImageAdapter(details1));
				} else {
					alert.setVisibility(View.VISIBLE);
					hlist.setVisibility(View.GONE);
				}
			}
		});

		commercial.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				resetButton();
				if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
					commercial.setBackgroundDrawable(ContextCompat.getDrawable(NearbyActivity.this, R.drawable.background_greyfull));
				} else {
					commercial.setBackground(ContextCompat.getDrawable(NearbyActivity.this, R.drawable.background_greyfull));
				}
				commercial.setTextColor(Color.WHITE);
				setSelected = "Commercial";

				if (googleMap != null) {
					googleMap.clear();
				}

				hide = false;
				arrow.setImageResource(R.drawable.down);

				if (!details2.isEmpty()) {
					alert.setVisibility(View.GONE);
					hlist.setVisibility(View.VISIBLE);
					setMarkers(details2);
					hlist.setAdapter(new GalleryImageAdapter(details2));
				} else {
					alert.setVisibility(View.VISIBLE);
					hlist.setVisibility(View.GONE);
				}
			}
		});

		land.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				resetButton();
				land.setBackgroundResource(R.drawable.background_greyfull);
				land.setTextColor(Color.WHITE);
				setSelected = "Land";

				if (googleMap != null) {
					googleMap.clear();
				}

				hide = false;
				arrow.setImageResource(R.drawable.down);

				if (!details3.isEmpty()) {
					alert.setVisibility(View.GONE);
					hlist.setVisibility(View.VISIBLE);
					setMarkers(details3);
					hlist.setAdapter(new GalleryImageAdapter(details3));
				} else {
					alert.setVisibility(View.VISIBLE);
					hlist.setVisibility(View.GONE);
				}
			}
		});

		industry.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				resetButton();
				industry.setBackgroundResource(R.drawable.background_greyfull);
				industry.setTextColor(Color.WHITE);
				setSelected = "Industry";

				if (googleMap != null) {
					googleMap.clear();
				}

				hide = false;
				arrow.setImageResource(R.drawable.down);

				if (!details4.isEmpty()) {
					alert.setVisibility(View.GONE);
					hlist.setVisibility(View.VISIBLE);
					setMarkers(details4);
					hlist.setAdapter(new GalleryImageAdapter(details4));
				} else {
					alert.setVisibility(View.VISIBLE);
					hlist.setVisibility(View.GONE);
				}

			}
		});

		arrow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (alert.getVisibility() == View.GONE) {
					if (!hide) {
						hlist.setVisibility(View.GONE);
						hide = true;
						arrow.setImageResource(R.drawable.up);
					} else {
						hlist.setVisibility(View.VISIBLE);
						hide = false;
						arrow.setImageResource(R.drawable.down);
					}
				}

			}

		});

		saleorrent.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(NearbyActivity.this);
				builder.setTitle("Select");

				builder.setSingleChoiceItems(select, selected_mode, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						selected_mode = which;
					}
				});
				builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						mode = select[selected_mode];
						saleorrent.setText(mode);

						// resetButton();
						// residential.setBackgroundResource(R.drawable.background_greyfull);
						// residential.setTextColor(Color.WHITE);
						new GetContent().execute();
					}
				});
				AlertDialog alert = builder.create();
				alert.show();
			}

		});
	}

	/**
	 * Async task class to get json by making HTTP call
	 */
	private class GetContent extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
			pDialog = new ProgressDialog(NearbyActivity.this);
			pDialog.setMessage("Loading...");
			pDialog.setCancelable(false);
			pDialog.show();

		}

		@Override
		protected Void doInBackground(Void... arg0) {

			ServiceHandler sh2 = new ServiceHandler();
			HashMap<String, String> params2 = new HashMap<>();
			params2.put(TAG_type, "nearby");
			// params2.put(TAG_lat, "5.413773");
			// params2.put(TAG_long, "100.313376");
			params2.put(TAG_lat, String.valueOf(gps.getLatitude()));
			params2.put(TAG_long, String.valueOf(gps.getLongitude()));
			params2.put(TAG_dist, String.valueOf(choose_distance));
			// params2.put(TAG_dist, "1");
			params2.put(TAG_mode, mode);

			json2 = sh2.makeServiceCall("GET", params2);

			details1 = new ArrayList<MainDetails>();
			details2 = new ArrayList<MainDetails>();
			details3 = new ArrayList<MainDetails>();
			details4 = new ArrayList<MainDetails>();

			details1.clear();
			details2.clear();
			details3.clear();
			details4.clear();

			if (json2 != null && !json2.equals("")) {
				try {
					// Getting JSON Array node
					object = new JSONObject(json2);
					listing = object.getJSONArray("listing");

					for (int i = 0; i < listing.length(); i++) {
						JSONObject c = listing.getJSONObject(i);

						String title = DecodeEntity.decodeEntities(c.getString(TAG_title));
						String address = DecodeEntity.decodeEntities(c.getString(TAG_address));
						double latitude = c.getDouble(TAG_latitude);
						double longitude = c.getDouble(TAG_longitude);
						String type = DecodeEntity.decodeEntities(c.getString(TAG_zoning));
						String project = DecodeEntity.decodeEntities(c.getString(TAG_project));
						String img = DecodeEntity.decodeEntities(c.getString("PropertyURL"));

						MainDetails md = new MainDetails();
						md.setLatitude(latitude);
						md.setLocation(address);
						md.setLongitude(longitude);
						md.setName(title);
						md.setObject(c);
						md.setProject(project);
						md.setImage(img);
						md.setType(type);

						if (type.endsWith("R")) {
							details1.add(md);
						}

						if (type.endsWith("C")) {
							details2.add(md);
						}

						if (type.endsWith("L")) {
							details3.add(md);
						}

						if (type.endsWith("I")) {
							details4.add(md);
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			} else {
				Log.e("ServiceHandler", "Couldn't get any data from the url");
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			// Dismiss the progress dialog
			if (pDialog.isShowing())
				pDialog.dismiss();

			if (json2 != null && !json2.equals("")) {
				hide = false;
				arrow.setImageResource(R.drawable.down);

				if (listing.length() > 0) {

					if (googleMap != null) {
						googleMap.clear();

						if (setSelected.equals("Residential")) {
							setMarkers(details1);
						}

						if (setSelected.equals("Commercial")) {
							setMarkers(details2);
						}

						if (setSelected.equals("Land")) {
							setMarkers(details3);
						}

						if (setSelected.equals("Industry")) {
							setMarkers(details4);
						}

					} else {
						if (setSelected.equals("Residential")) {
							try {
								// Loading map
								setMarkers(details1);

							} catch (Exception e) {
								e.printStackTrace();
							}
						}

						if (setSelected.equals("Commercial")) {
							try {
								// Loading map
								setMarkers(details2);

							} catch (Exception e) {
								e.printStackTrace();
							}
						}

						if (setSelected.equals("Land")) {
							try {
								// Loading map
								setMarkers(details3);

							} catch (Exception e) {
								e.printStackTrace();
							}
						}

						if (setSelected.equals("Industry")) {
							try {
								// Loading map
								setMarkers(details4);

							} catch (Exception e) {
								e.printStackTrace();
							}
						}

					}

					if (setSelected.equals("Residential")) {
						if (!details1.isEmpty()) {
							alert.setVisibility(View.GONE);
							hlist.setVisibility(View.VISIBLE);
							hlist.setAdapter(new GalleryImageAdapter(details1));
						} else {
							alert.setVisibility(View.VISIBLE);
							hlist.setVisibility(View.GONE);
						}
					}

					if (setSelected.equals("Commercial")) {
						if (!details2.isEmpty()) {
							alert.setVisibility(View.GONE);
							hlist.setVisibility(View.VISIBLE);
							hlist.setAdapter(new GalleryImageAdapter(details2));
						} else {
							alert.setVisibility(View.VISIBLE);
							hlist.setVisibility(View.GONE);
						}
					}

					if (setSelected.equals("Land")) {
						if (!details3.isEmpty()) {
							alert.setVisibility(View.GONE);
							hlist.setVisibility(View.VISIBLE);
							hlist.setAdapter(new GalleryImageAdapter(details3));
						} else {
							alert.setVisibility(View.VISIBLE);
							hlist.setVisibility(View.GONE);
						}
					}

					if (setSelected.equals("Industry")) {
						if (!details4.isEmpty()) {
							alert.setVisibility(View.GONE);
							hlist.setVisibility(View.VISIBLE);
							hlist.setAdapter(new GalleryImageAdapter(details4));
						} else {
							alert.setVisibility(View.VISIBLE);
							hlist.setVisibility(View.GONE);
						}
					}

				} else {
					if (googleMap != null) {
						googleMap.clear();
					}
					alert.setVisibility(View.VISIBLE);
					hlist.setVisibility(View.GONE);
				}

			} else {
				if (googleMap != null) {
					googleMap.clear();
				}
				alert.setVisibility(View.VISIBLE);
				hlist.setVisibility(View.GONE);
				hide = false;
				arrow.setImageResource(R.drawable.down);
			}

		}

	}


	// turn on gps alert
	public void showSettingsAlert() {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(NearbyActivity.this);

		alertDialog.setCancelable(false);

		alertDialog.setTitle("Location not found");

		alertDialog.setMessage("Unable to get your location. Do you want to turn on GPS?");

		alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
				startActivity(intent);
				finish();
			}
		});

		alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
				new GetContent().execute();
			}
		});

		alertDialog.show();
	}

	// horizontal listview adapter
	public class GalleryImageAdapter extends BaseAdapter {

		ArrayList<MainDetails> image;

		public GalleryImageAdapter(ArrayList<MainDetails> img) {
			image = img;
		}

		@Override
		public int getCount() {
			return image.size();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			View view = convertView;

			LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			if (view == null) {
				view = vi.inflate(R.layout.image, null);
			}

			ImageView img = (ImageView) view.findViewById(R.id.imageView1);
			TextView tv = (TextView) view.findViewById(R.id.textView1);
			ImageView img2 = (ImageView) view.findViewById(R.id.imageView2);
			ImageView img3 = (ImageView) view.findViewById(R.id.imageView3);

			final MainDetails content = image.get(position);

			imageLoader.displayImage(content.image, img, options);
			tv.setText(content.project);

			if (mode.equalsIgnoreCase("Sale")) {
				img2.setImageResource(R.drawable.sale);
			} else {
				img2.setImageResource(R.drawable.rent);
			}

			img3.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (setSelected.equals("Residential")) {
						Intent i = new Intent(NearbyActivity.this, ResidentialDetailsActivity.class);
						i.putExtra("mode", mode);
						i.putExtra("object", content.jsonObject.toString());
						startActivity(i);

					} else if (setSelected.equals("Commercial")) {
						Intent i = new Intent(NearbyActivity.this, CommercialDetailsActivity.class);
						Log.e("mode", mode);
						i.putExtra("mode", mode);
						i.putExtra("object", content.jsonObject.toString());
						startActivity(i);

					} else if (setSelected.equals("Land")) {
						Intent i = new Intent(NearbyActivity.this, LandDetailsActivity.class);
						i.putExtra("mode", mode);
						i.putExtra("object", content.jsonObject.toString());
						startActivity(i);

					} else if (setSelected.equals("Industry")) {
						Intent i = new Intent(NearbyActivity.this, IndustryDetailsActivity.class);
						i.putExtra("mode", mode);
						i.putExtra("object", content.jsonObject.toString());
						startActivity(i);
					}
				}

			});

			return view;
		}
	}

	// set marker on google map and onclick function
	public void setMarkers(ArrayList<MainDetails> detail) {

		if (detail.size() > 0) {
			markers = new Marker[detail.size()];

			// load branch location into marker
			for (int x = 0; x < detail.size(); x++) {

				MainDetails content = detail.get(x);
				// latitude and longitude
				double latitude = content.latitude;
				double longitude = content.longitude;

				// create marker
				MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longitude)).title(content.name)
						.snippet(content.location);

				// Changing marker icon
				if (mode.equalsIgnoreCase("RENT")) {
					marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.map_rent));
				} else {
					marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.map_sale));
				}

				MainDetails md = new MainDetails();
				md.setObject(content.jsonObject);

				// adding marker
				markers[x] = googleMap.addMarker(marker);

				haspMap.put(markers[x], md);

			}

			// Zoom fit all the markers
			LatLngBounds.Builder builder = new LatLngBounds.Builder();

			for (Marker m : markers) {
				builder.include(m.getPosition());
			}

			LatLngBounds bounds = builder.build();

			if (markers.length > 1) {

				CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 300, 300, 10);
				// animations
				googleMap.animateCamera(cu);
			} else {
				CameraPosition cameraPosition = new CameraPosition.Builder()
						.target(new LatLng(detail.get(0).latitude, detail.get(0).longitude)).zoom(15).build();

				googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
			}

			googleMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
				@Override
				public void onInfoWindowClick(Marker marker) {

					MainDetails info = haspMap.get(marker);

					if (setSelected.equals("Residential")) {
						Log.e("idd", String.valueOf(marker));
						Intent i = new Intent(NearbyActivity.this, ResidentialDetailsActivity.class);
						i.putExtra("mode", mode);
						i.putExtra("object", info.jsonObject.toString());
						startActivity(i);

					} else if (setSelected.equals("Commercial")) {
						Intent i = new Intent(NearbyActivity.this, CommercialDetailsActivity.class);
						i.putExtra("mode", mode);
						i.putExtra("object", info.jsonObject.toString());
						startActivity(i);

					} else if (setSelected.equals("Land")) {
						Intent i = new Intent(NearbyActivity.this, LandDetailsActivity.class);
						i.putExtra("mode", mode);
						i.putExtra("object", info.jsonObject.toString());
						startActivity(i);

					} else if (setSelected.equals("Industry")) {
						Intent i = new Intent(NearbyActivity.this, IndustryDetailsActivity.class);
						i.putExtra("mode", mode);
						i.putExtra("object", info.jsonObject.toString());
						startActivity(i);
					}
				}
			});
		}
	}

}
