package com.magusapp.property;

import java.util.HashMap;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.magusapp.property.utility.Alert;
import com.magusapp.property.utility.AppPreferences;
import com.magusapp.property.utility.ConnectionDetector;
import com.magusapp.property.utility.ServiceHandler;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

public class JoinActivity extends Activity {

	private static final String TAG_type = "type";

	TextView submit;
	TextView txttitle;
	EditText txtphone, txtaddress, txtIC, txtname;
	Spinner spnr;
	RadioGroup radioGroup;
	RadioButton radiobutton;

	String jsonStr;
	String json;

	JSONArray positionArray;

	JSONObject object;

	ProgressDialog pDialog;

	String[] position_type = null;

	String positionType = "", name, phone, ic, address, gender;
	String result_type, result_message;

	AppPreferences app_pref;

	Boolean isInternetPresent;
	ConnectionDetector connection_detector;

	String title;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_join);
		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		Intent i = getIntent();
		title = i.getStringExtra("title");

		connection_detector = new ConnectionDetector(JoinActivity.this);
		isInternetPresent = connection_detector.isConnectingToInternet();

		txtname = (EditText) findViewById(R.id.editName);
		txtphone = (EditText) findViewById(R.id.editPhone);
		radioGroup = (RadioGroup) findViewById(R.id.radioGender);
		txtIC = (EditText) findViewById(R.id.editIC);
		txtaddress = (EditText) findViewById(R.id.editAddress);
		spnr = (Spinner) findViewById(R.id.spinnerPosition);
		submit = (TextView) findViewById(R.id.submit);

		txttitle = (TextView) findViewById(R.id.textView1);

		txttitle.setText(title);

		if (isInternetPresent) {
			new GetPosition().execute();
		} else {
			Alert.ShowAlertMessage("No internet connection.", "Information", "Finish", null, JoinActivity.this,
					JoinActivity.this);
		}

		spnr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

				int position = spnr.getSelectedItemPosition();
				positionType = position_type[position];
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}

		});

		onClick();
	}

	public void onClick() {
		submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				int selectedId = radioGroup.getCheckedRadioButtonId();

				radiobutton = (RadioButton) findViewById(selectedId);
				if (radiobutton.getText().toString().equals("Male")) {
					gender = "m";
				} else {
					gender = "f";
				}

				if (validation()) {
					if (isInternetPresent) {
						new Submit().execute();
					} else {
						Alert.ShowAlertMessage("No internet connection.", "Information", null, null, JoinActivity.this,
								JoinActivity.this);
					}
				}

			}
		});
	}

	private class GetPosition extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(JoinActivity.this);
			pDialog.setMessage("Loading...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {

			ServiceHandler sh = new ServiceHandler();
			HashMap<String, String> params = new HashMap<>();
			params.put(TAG_type, "getJobPositions");

			json = sh.makeServiceCall("GET", params);

			if (json != null || !json.equals("")) {
				try {

					positionArray = new JSONArray(json);

					position_type = new String[positionArray.length()];

					for (int i = 0; i < positionArray.length(); i++) {

						JSONObject c2 = positionArray.getJSONObject(i);

						position_type[i] = c2.getString("JobName");

					}

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (pDialog.isShowing())
				pDialog.dismiss();

			ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(JoinActivity.this,
					android.R.layout.simple_list_item_1, position_type);

			spnr.setAdapter(adapter2);
		}

	}

	private class Submit extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(JoinActivity.this);
			pDialog.setMessage("Loading...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			name = txtname.getText().toString();
			phone = txtphone.getText().toString();
			ic = txtIC.getText().toString();
			address = txtaddress.getText().toString();

			ServiceHandler sh = new ServiceHandler();
			HashMap<String, String> params = new HashMap<>();
			params.put(TAG_type, "insertjobcandidate");
			params.put("name", name);
			params.put("phone", phone);
			params.put("gender", gender);
			params.put("icno", ic);
			params.put("homeaddress", address);
			params.put("position", positionType);
			jsonStr = sh.makeServiceCall("POST", params);

			try {

				JSONArray listing = new JSONArray(jsonStr);

				for (int i = 0; i < listing.length(); i++) {

					JSONObject c2 = listing.getJSONObject(i);

					if (jsonStr.contains("@ResultMessage")) {
						result_message = c2.getString("@ResultMessage");
					} else {
						result_message = c2.getString("@RetID");
					}

				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (pDialog.isShowing())
				pDialog.dismiss();

			if (!jsonStr.contains("@ResultMessage")) {
				Alert.ShowAlertMessage(
						"Thanks for your interest to join us and we shall contact you soonest for interview 谢谢你有意加入世界地产，我们会尽快联络你",
						"Thank You", null, null, JoinActivity.this, JoinActivity.this);
				recreate();
			} else {

				Alert.ShowAlertMessage(result_message, "Information", null, null, JoinActivity.this, JoinActivity.this);

			}
		}

	}

	private boolean validation() {
		// LEVEL 1 - CHECK EMPTY
		String checkEmpty = "";
		txtname.setText(txtname.getText().toString().replaceAll(" ", "").trim());
		txtphone.setText(txtphone.getText().toString().replaceAll(" ", "").trim());
		txtIC.setText(txtIC.getText().toString().replaceAll(" ", "").trim());
		txtaddress.setText(txtaddress.getText().toString().replaceAll(" ", "").trim());

		if (txtname.getText().toString().matches(""))
			checkEmpty = "-Name Required\n";
		if (txtphone.getText().toString().matches(""))
			checkEmpty += "-Handphone Required\n";
		if (txtIC.getText().toString().matches(""))
			checkEmpty += "-IC Number Required\n";
		if (txtaddress.getText().toString().matches(""))
			checkEmpty += "-Home Address Required\n";
		if (position_type.equals(""))
			checkEmpty += "-Position Required";

		if (!checkEmpty.matches("")) {
			showMessageForValidation(checkEmpty, "Complete these fields");
			return false;
		} else {
			// LEVEL 2 - CHECK FIELDS
			String fieldsError = "";

			if (!validPhone(txtphone.getText().toString()))
				fieldsError = "-Phone Number not valid";
			if (!isNumeric(txtIC.getText().toString()))
				fieldsError = "-Only numbers allowed for ICNo\n";
			if (txtIC.getText().length() > 14)
				fieldsError += "-Only 14 charactes allowed for ICNo\n";

			if (!fieldsError.matches("")) {
				showMessageForValidation(fieldsError, "Fields Error");
				return false;
			} else {
				// Log.i("SUCESS", "SUCCESS");
				return true;
			}
		}

	}

	public static boolean isNumeric(String str) {
		return str.matches("-?\\d+(\\.\\d+)?"); // match a number with optional
		// '-' and decimal.
	}

	private boolean validPhone(String phone) {
		Pattern pattern = Patterns.PHONE;
		return pattern.matcher(phone).matches();
	}

	private void showMessageForValidation(String theMessage, String theTitle) {
		Alert.ShowAlertMessage(theMessage, theTitle, null, null, JoinActivity.this, JoinActivity.this);
	}
}
