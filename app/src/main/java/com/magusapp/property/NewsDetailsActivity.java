package com.magusapp.property;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.magusapp.property.utility.Alert;
import com.magusapp.property.utility.ConnectionDetector;
import com.magusapp.property.utility.ServiceHandler;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.PageIndicator;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class NewsDetailsActivity extends Activity {

	private static final String TAG_id = "id";
	private static final String TAG_type = "type";

	String source, desc, date, name, id = "", type;

	String json2 = "";

	TextView desc_t, date_t, name_t, source_t, date_title;
	ImageView image;

	JSONArray jarray;

	ProgressDialog pDialog;

	ImageLoader imageLoader = ImageLoader.getInstance();
	DisplayImageOptions options;
	ViewPager pager;
	PageIndicator mIndicator;
	CirclePageIndicator indicator;
	ImageButton choose;

	ArrayList<MainDetails> details = new ArrayList<MainDetails>();

	Boolean isInternetPresent;
	ConnectionDetector connection_detector;
	File file;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_news_details);

		Intent i = getIntent();
		id = i.getStringExtra("id");
		name = i.getStringExtra("name");
		desc = i.getStringExtra("detail");
		source = i.getStringExtra("source");
		date = i.getStringExtra("date");
		type = i.getStringExtra("type");

		options = new DisplayImageOptions.Builder().cacheInMemory(false).cacheOnDisk(true).considerExifParams(true)
				.showImageForEmptyUri(R.drawable.no_image).bitmapConfig(Bitmap.Config.RGB_565).build();

		if (!imageLoader.isInited()) {
			imageLoader.init(ImageLoaderConfiguration.createDefault(this));
		}

		indicator = (CirclePageIndicator) findViewById(R.id.indicator);
		image = (ImageView) findViewById(R.id.imageView1);
		pager = (ViewPager) findViewById(R.id.gallery_pager);
		desc_t = (TextView) findViewById(R.id.details);
		date_t = (TextView) findViewById(R.id.date);
		source_t = (TextView) findViewById(R.id.source);
		name_t = (TextView) findViewById(R.id.title);
		date_title = (TextView) findViewById(R.id.dateTitle);

		desc_t.setText(desc);
		date_t.setText(date);
		source_t.setText(source);
		name_t.setText(name);

		if (type.equals("info")) {
			date_title.setText("Created Date: ");
		}

		connection_detector = new ConnectionDetector(NewsDetailsActivity.this);
		isInternetPresent = connection_detector.isConnectingToInternet();

		if (isInternetPresent) {
			new GetContent().execute();
		} else {
			Alert.ShowAlertMessage("No internet connection.", "Information", "Finish", null, NewsDetailsActivity.this,
					NewsDetailsActivity.this);
		}

		onClick();

	}

	public void onClick() {
		choose = (ImageButton) findViewById(R.id.SMSIcon);
		choose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.putExtra("address", "0144817777");
				intent.putExtra("sms_body", "Title: " + name + "\n" + desc);
				intent.setType("vnd.android-dir/mms-sms");
				startActivity(intent);

			}
		});
		choose = (ImageButton) findViewById(R.id.callIcon);
		choose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent = new Intent(Intent.ACTION_DIAL);
				intent.setData(Uri.parse("tel:" + "0125001048"));
				startActivity(intent);

			}
		});

		choose = (ImageButton) findViewById(R.id.emailIcon);
		choose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent i = new Intent(Intent.ACTION_SEND);
				i.setType("message/rfc822");
				i.putExtra(Intent.EXTRA_EMAIL, new String[] { "enquiries@lskworld.com" });
				i.putExtra(Intent.EXTRA_SUBJECT, "Enquiry From APP");
				i.putExtra(Intent.EXTRA_TEXT, "Title: " + name + "\n" + desc);
				startActivity(Intent.createChooser(i, "Send Email"));

			}
		});
		choose = (ImageButton) findViewById(R.id.shareIcon);
		choose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				final ProgressDialog saveProgressDialog = new ProgressDialog(NewsDetailsActivity.this);
				saveProgressDialog.setMessage("Loading ...");
				saveProgressDialog.isIndeterminate();
				saveProgressDialog.setCancelable(false);

				saveProgressDialog.show();
				new Thread(new Runnable() {
					@Override
					public void run() {

						if (details.size() > 0) {

							try {
								int pos = pager.getCurrentItem();
								URL imageurl = new URL(details.get(0).image);
								Bitmap bitmap = BitmapFactory.decodeStream(imageurl.openConnection().getInputStream());

								String path = Environment.getExternalStorageDirectory().getAbsolutePath()
										+ "/penangrealty.png";
								file = new File(path);

								FileOutputStream out = new FileOutputStream(file);
								Log.d("in save()", "after outputstream");

								getResizedBitmap(bitmap).compress(Bitmap.CompressFormat.PNG, 50, out);
								out.flush();
								out.close();

							} catch (IOException e) {

							}

						} else {
							Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.no_image);
							String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
							file = new File(extStorageDirectory, "penangrealty.png");
							FileOutputStream outStream;
							try {
								outStream = new FileOutputStream(file);
								bm.compress(Bitmap.CompressFormat.PNG, 50, outStream);
								outStream.flush();
								outStream.close();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

						Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
						whatsappIntent.setType("text/plain");
						whatsappIntent.putExtra(Intent.EXTRA_TEXT,
								"Title: " + name + "\n" + desc + "\n" + "http://onelink.to/hhx3s7");
						whatsappIntent.setType("image/png");
						whatsappIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
						startActivity(Intent.createChooser(whatsappIntent, "Share"));

						handler.sendEmptyMessage(0);
					}

					private Handler handler = new Handler() {
						@Override
						public void handleMessage(Message msg) {
							saveProgressDialog.dismiss();
						}
					};
				}).start();

			}
		});
	}

	public static Bitmap getResizedBitmap(Bitmap bm) {
		int width = bm.getWidth();
		int height = bm.getHeight();
		int newWidth = width * 50 / 100;
		int newHeight = height * 50 / 100;
		float scaleWidth = ((float) newWidth) / width;
		float scaleHeight = ((float) newHeight) / height;
		Matrix matrix = new Matrix();
		// RESIZE THE BIT MAP
		matrix.postScale(scaleWidth, scaleHeight);
		// RECREATE THE NEW BITMAP
		Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
		return resizedBitmap;
	}

	private class GetContent extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(NewsDetailsActivity.this);
			pDialog.setMessage("Loading...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {

			ServiceHandler sh4 = new ServiceHandler();
			HashMap<String, String> params4 = new HashMap<>();
			params4.put(TAG_type, "picture");
			params4.put(TAG_id, id);

			json2 = sh4.makeServiceCall("GET", params4);

			if (!json2.equals("")) {
				try {

					jarray = new JSONArray(json2);

					if (!json2.contains("@ResultMessage")) {

						for (int j = 0; j < jarray.length(); j++) {
							JSONObject c = jarray.getJSONObject(j);
							String img = c.getString("pictureURL");

							MainDetails md = new MainDetails();
							md.setImage(img);
							md.setCover(json2);
							details.add(md);
						}
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				Log.e("empty", "empty");
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (pDialog.isShowing())
				pDialog.dismiss();

			if (!json2.contains("@ResultMessage") && !json2.equals("") && jarray.length() != 0) {
				pager.setAdapter(new ImagePagerAdapter(details));
				mIndicator = indicator;
				indicator.setViewPager(pager);
				indicator.setSnap(true);

				image.setVisibility(View.GONE);
			} else {
				pager.setVisibility(View.GONE);
				indicator.setVisibility(View.GONE);
				image.setVisibility(View.VISIBLE);
			}
		}

	}

	private class ImagePagerAdapter extends PagerAdapter {

		ArrayList<MainDetails> details;
		private LayoutInflater inflater;

		ImagePagerAdapter(ArrayList<MainDetails> det) {
			this.details = det;
			inflater = getLayoutInflater();
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			((ViewPager) container).removeView((View) object);
		}

		@Override
		public void finishUpdate(View container) {
		}

		@Override
		public int getCount() {
			return details.size();
		}

		@Override
		public Object instantiateItem(ViewGroup view, final int position) {
			final MainDetails content = details.get(position);
			View imageLayout = inflater.inflate(R.layout.pager, view, false);
			ImageView imageView = (ImageView) imageLayout.findViewById(R.id.image);
			imageLoader.displayImage(content.image, imageView, options);
			((ViewPager) view).addView(imageLayout, 0);

			imageLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent i = new Intent(NewsDetailsActivity.this, DisplayPhotoActivity.class);
					i.putExtra("position", position);
					i.putExtra("image", content.cover);
					startActivity(i);
				}
			});

			return imageLayout;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view.equals(object);
		}

		@Override
		public void restoreState(Parcelable state, ClassLoader loader) {
		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void startUpdate(View container) {
		}
	}
}
