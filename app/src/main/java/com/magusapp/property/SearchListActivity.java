package com.magusapp.property;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.costum.android.widget.LoadMoreListView;
import com.costum.android.widget.LoadMoreListView.OnLoadMoreListener;
import com.magusapp.property.utility.Alert;
import com.magusapp.property.utility.ConnectionDetector;
import com.magusapp.property.utility.DecodeEntity;
import com.magusapp.property.utility.ServiceHandler;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;

public class SearchListActivity extends Activity {

	private static final String TAG_type = "type";
	private static final String TAG_mode = "mode";
	private static final String TAG_project = "project";
	private static final String TAG_min = "min";
	private static final String TAG_max = "max";
	private static final String TAG_size = "size";
	private static final String TAG_location = "location";
	private static final String TAG_page = "page";
	private static final String TAG_size_max = "minsize";
	private static final String TAG_size_min = "maxsize";
	private static final String TAG_land_min = "minlandsize";
	private static final String TAG_land_max = "maxlandsize";

	String jsonStr, jsonStr2;
	JSONObject object;
	JSONArray jarray;
	ProgressDialog pDialog;

	LoadMoreListView lv;
	TextView tv, txtcount;

	int count = 0;

	SearchListAdapter adapter;

	ArrayList<MainDetails> details = new ArrayList<MainDetails>();

	String mode, min, max, location, saleorrent, url, project, newdate, size_min, size_max, land_min, land_max;

	int total = 0, gettotal = 0;

	String price;

	Boolean isInternetPresent;
	ConnectionDetector connection_detector;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_list);

		connection_detector = new ConnectionDetector(SearchListActivity.this);
		isInternetPresent = connection_detector.isConnectingToInternet();

		Intent i = getIntent();

		saleorrent = i.getStringExtra(TAG_type);
		location = i.getStringExtra(TAG_location);
		min = i.getStringExtra(TAG_min);
		max = i.getStringExtra(TAG_max);
		mode = i.getStringExtra(TAG_mode);
		project = i.getStringExtra(TAG_project);
		size_min = i.getStringExtra(TAG_size_min);
		size_max = i.getStringExtra(TAG_size_max);
		land_min = i.getStringExtra(TAG_land_min);
		land_max = i.getStringExtra(TAG_land_max);

		// Log.e("saleorent", saleorrent);
		// Log.e("location", location);
		// Log.e("min", min);
		// Log.e("max", max);
		// Log.e("size", size_min);
		// Log.e("size2", size_max);
		// Log.e("mode", mode);
		// Log.e("project", project);
		// Log.e("land", land_min);
		// Log.e("land2", land_max);

		tv = (TextView) findViewById(R.id.textView1);

		tv.setText("Property List");

		if (isInternetPresent) {
			new GetContent().execute();
		} else {
			Alert.ShowAlertMessage("No internet connection.", "Information", "Finish", null, SearchListActivity.this,
					SearchListActivity.this);
		}

		lv = (LoadMoreListView) findViewById(R.id.myListView);
		txtcount = (TextView) findViewById(R.id.count);

		adapter = new SearchListAdapter(this, details);

		lv.setOnLoadMoreListener(new OnLoadMoreListener() {
			@Override
			public void onLoadMore() {
			
				if (count > 0) {
				
					new LoadDataTask().execute();
				}
			
			}
		});

	}

	private class LoadDataTask extends AsyncTask<Void, Void, Void> {

		@SuppressWarnings("deprecation")
		@Override
		protected Void doInBackground(Void... params) {

			if (isCancelled()) {
				return null;
			}

			ServiceHandler sh2 = new ServiceHandler();
			HashMap<String, String> params2 = new HashMap<>();
			params2.put(TAG_type, saleorrent);
			params2.put(TAG_mode, mode);
			params2.put(TAG_max, max);
			params2.put(TAG_min, min);
			params2.put(TAG_project, project);
			params2.put(TAG_location, location);
			params2.put(TAG_size_min, size_min);
			params2.put(TAG_size_max, size_max);
			params2.put(TAG_land_min, land_min);
			params2.put(TAG_land_max, land_max);
			params2.put(TAG_page, String.valueOf(count));

			jsonStr = sh2.makeServiceCall("GET", params2);

			if (jsonStr != null || !jsonStr.equals("")) {
				try {

					// Getting JSON Array node
					object = new JSONObject(jsonStr);

					JSONArray listing = object.getJSONArray("listing");

					for (int k = 0; k < listing.length(); k++) {

						JSONObject c2 = listing.getJSONObject(k);

						String location = DecodeEntity.decodeEntities(c2.getString("location"));
						String type = DecodeEntity.decodeEntities(c2.getString("type"));

						if (saleorrent.equals("sales_property")) {
							price = DecodeEntity.decodeEntities(c2.getString("SalePrice"));
						} else {
							price = DecodeEntity.decodeEntities(c2.getString("RentPrice"));
						}

						String project = DecodeEntity.decodeEntities(c2.getString("Project"));
						String update = DecodeEntity.decodeEntities(c2.getString("UpdatedDate"));
						String img = DecodeEntity.decodeEntities(c2.getString("PropertyURL"));

						if (!update.equals("")) {
							newdate = update.replace(update.substring(update.length() - 14), "");
						} else {
							newdate = "";
						}

						MainDetails cd;
						cd = new MainDetails();
						cd.setLocation(location);
						cd.setType(type);
						cd.setPrice(price);
						cd.setProject(project);
						cd.setUpdate(newdate);
						cd.setObject(c2);
						cd.setSaleorrent(mode);
						cd.setImage(img);
						details.add(cd);

					}

				} catch (JSONException e) {
					e.printStackTrace();

				}

				if (total > 0) {
					count += 1;
					
						int getcount = total-gettotal;
						if(getcount>20){
							gettotal = gettotal+20;
						}else{
							gettotal = total;
						}
					
				}
			}
			return null;

		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

			adapter.notifyDataSetChanged();
			// Call onLoadMoreComplete when the LoadMore task, has finished
			txtcount.setText(String.valueOf(gettotal) + " of " + String.valueOf(total)  + " results");
			lv.onLoadMoreComplete();
		}

		@Override
		protected void onCancelled() {
			// Notify the loading more operation has finished
			lv.onLoadMoreComplete();
		}
	}

	/**
	 * Async task class to get json by making HTTP call
	 */
	private class GetContent extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(SearchListActivity.this);
			pDialog.setMessage("Loading...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@SuppressWarnings("deprecation")
		@Override
		protected Void doInBackground(Void... arg0) {

			ServiceHandler sh = new ServiceHandler();
			HashMap<String, String> params = new HashMap<>();
			params.put(TAG_type, saleorrent);
			params.put(TAG_mode, mode);
			params.put(TAG_max, max);
			params.put(TAG_min, min);
			params.put(TAG_project, project);
			params.put(TAG_location, location);
			params.put(TAG_size_min, size_min);
			params.put(TAG_size_max, size_max);
			params.put(TAG_land_min, land_min);
			params.put(TAG_land_max, land_max);

			params.put(TAG_page, String.valueOf(count));

			jsonStr = sh.makeServiceCall("GET", params);

			if (jsonStr != null || !jsonStr.equals("")) {
				try {

					// Getting JSON Array node
					object = new JSONObject(jsonStr);
					total = Integer.parseInt(object.getString("count"));
					
					JSONArray listing = object.getJSONArray("listing");

					for (int k = 0; k < listing.length(); k++) {

						JSONObject c2 = listing.getJSONObject(k);

						String location = DecodeEntity.decodeEntities(c2.getString("location"));
						String type = DecodeEntity.decodeEntities(c2.getString("type"));

						if (saleorrent.equals("sales_property")) {
							price = DecodeEntity.decodeEntities(c2.getString("SalePrice"));
						} else {
							price = DecodeEntity.decodeEntities(c2.getString("RentPrice"));
						}

						String project = DecodeEntity.decodeEntities(c2.getString("Project"));
						String update = DecodeEntity.decodeEntities(c2.getString("UpdatedDate"));
						String img = DecodeEntity.decodeEntities(c2.getString("PropertyURL"));

						if (!update.equals("")) {
							newdate = update.replace(update.substring(update.length() - 14), "");
						} else {
							newdate = "";
						}

						MainDetails cd;
						cd = new MainDetails();
						cd.setLocation(location);
						cd.setType(type);
						cd.setPrice(price);
						cd.setProject(project);
						cd.setUpdate(newdate);
						cd.setObject(c2);
						cd.setSaleorrent(mode);
						cd.setImage(img);
						details.add(cd);

					}

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (pDialog.isShowing())
				pDialog.dismiss();

			if (jsonStr != null || !jsonStr.equals("")) {

				if (total > 0) {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							lv.setAdapter(new SearchListAdapter(SearchListActivity.this, details));
							count += 1;
							if(total<20){
								gettotal = total;

							}else{
								gettotal = 20;

							}
							txtcount.setText(String.valueOf(gettotal) + " of " + String.valueOf(total) + " results");

						}
					});
				} else {
					Alert.ShowAlertMessage(
							"Please submit your requirement to us at buyer/tenant submission. Our dedicated REN will contact you soonest. \n世界地产谢谢您，请在 buyer/tenant 登记您的要求，我们专业的仲介员会尽快联络您。",
							"NO LISTING MATCH YOUR SEARCH", "Finish", null, SearchListActivity.this,
							SearchListActivity.this);
				}
			} else {
				Alert.ShowAlertMessage(
						"Please submit your requirement to us at buyer/tenant submission. Our dedicated REN will contact you soonest. \n世界地产谢谢您，请在 buyer/tenant 登记您的要求，我们专业的仲介员会尽快联络您。",
						"NO LISTING MATCH YOUR SEARCH", "Finish", null, SearchListActivity.this,
						SearchListActivity.this);
			}

		}

	}

}
