package com.magusapp.property;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import com.magusapp.property.utility.Bookmark;
import com.magusapp.property.utility.DatabaseAdapter;
import com.magusapp.property.utility.DecodeEntity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class BookmarkActivity extends Activity {

	ArrayList<Bookmark> mark = new ArrayList<Bookmark>();
	ArrayList<MainDetails> details = new ArrayList<MainDetails>();

	DatabaseAdapter db;

	JSONObject object;

	ProgressDialog pDialog;

	SearchListAdapter adapter;

	String price, newdate;

	boolean value;

	ImageButton clear;
	TextView txt;
	ListView lv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bookmark);

		db = new DatabaseAdapter(this);

		clear = (ImageButton) findViewById(R.id.clear);
		txt = (TextView) findViewById(R.id.textView2);
		lv = (ListView) findViewById(R.id.myListView);

		if (db.checkForTables()) {
			// value = db.checkBookmark(categoryID, id);
			mark = db.getAllBookmark();
			new GetContent().execute();
		} else {
			clear.setVisibility(View.GONE);
			txt.setVisibility(View.VISIBLE);
			lv.setVisibility(View.GONE);
		}

		adapter = new SearchListAdapter(this, details);

		clear.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				AlertDialog.Builder alertDialog = new AlertDialog.Builder(BookmarkActivity.this);
				alertDialog.setTitle("Confirmation");
				alertDialog.setCancelable(false);
				alertDialog.setMessage("Do you wish to delete all?");

				alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						db.clearTable();
						clear.setVisibility(View.GONE);
						txt.setVisibility(View.VISIBLE);
						lv.setVisibility(View.GONE);
					}
				});
				alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {

						dialog.cancel();
					}
				});

				alertDialog.show();

			}

		});

	}

	/**
	 * Async task class to get json by making HTTP call
	 */
	private class GetContent extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(BookmarkActivity.this);
			pDialog.setMessage("Loading...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {

			try {

				for (int i = 0; i < mark.size(); i++) {

					Bookmark bookmark = mark.get(i);
					JSONObject c2 = new JSONObject(bookmark.getJSON());

					String location = DecodeEntity.decodeEntities(c2.getString("location"));
					String type = DecodeEntity.decodeEntities(c2.getString("type"));

					if (bookmark.getJSON().contains("SalePrice")) {
						price = DecodeEntity.decodeEntities(c2.getString("SalePrice"));
					} else {
						price = DecodeEntity.decodeEntities(c2.getString("RentPrice"));
					}

					String project = DecodeEntity.decodeEntities(c2.getString("Project"));
					String update = DecodeEntity.decodeEntities(c2.getString("UpdateDate"));
					String img = DecodeEntity.decodeEntities(c2.getString("PropertyURL"));

					if (!update.equals("")) {
						newdate = update.replace(update.substring(update.length() - 14), "");
					} else {
						newdate = "";
					}

					MainDetails cd;
					cd = new MainDetails();
					cd.setLocation(location);
					cd.setType(type);
					cd.setPrice(price);
					cd.setProject(project);
					cd.setUpdate(newdate);
					cd.setObject(c2);
					cd.setSaleorrent(bookmark.getMode());
					cd.setImage(img);
					details.add(cd);

				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (pDialog.isShowing())
				pDialog.dismiss();

			lv.setAdapter(new SearchListAdapter(BookmarkActivity.this, details));

		}

	}

}