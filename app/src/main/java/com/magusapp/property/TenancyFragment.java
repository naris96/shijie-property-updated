package com.magusapp.property;

import java.text.DecimalFormat;

import com.magusapp.property.utility.NumberTextWatcher;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class TenancyFragment extends Fragment {

	TextView txt_total, txt_charge, txt_stamp_duty;
	EditText rental, year, additional;
	ImageButton calculate;

	double rental_fee = 0, additional_copy = 0, total_annual_rental = 0, annual_rental = 0, total_charge = 0,
			total_fee = 0, total_agreement = 0, add_total = 0;
	int tenancy_period = 0;

	DecimalFormat df;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.activity_tenancy_fragment, container, false);

		getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		rental = (EditText) rootView.findViewById(R.id.txt_fee);
		year = (EditText) rootView.findViewById(R.id.txt_year);
		additional = (EditText) rootView.findViewById(R.id.txt_additional);

		txt_total = (TextView) rootView.findViewById(R.id.txt_total);
		txt_charge = (TextView) rootView.findViewById(R.id.txt_charged);
		txt_stamp_duty = (TextView) rootView.findViewById(R.id.txt_stamp_duty);
		calculate = (ImageButton) rootView.findViewById(R.id.calculate);

		rental.addTextChangedListener(new NumberTextWatcher(rental));

		df = new DecimalFormat("#,###,##0.00");

		calculate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (!rental.getText().toString().matches("")) {
					rental_fee = Double.valueOf(rental.getText().toString().replace(",", ""));
				} else {
					rental_fee = 0;
				}

				total_annual_rental = rental_fee * 12;
				annual_rental = total_annual_rental - 2400;

				if (!year.getText().toString().matches("")) {
					tenancy_period = Integer.parseInt(year.getText().toString());
				} else {
					tenancy_period = 0;
				}

				if (annual_rental != 0) {
					if (tenancy_period <= 1) {
						total_charge = 1;
					} else if (tenancy_period > 1 && tenancy_period <= 3) {
						total_charge = 2;
					} else if (tenancy_period > 3) {
						total_charge = 4;
					}
				}

				if (rental_fee >= 10000) {
					total_agreement = rental_fee * 10 / 100;
				} else {
					total_agreement = rental_fee * 25 / 100;
				}

				if (!additional.getText().toString().matches("")) {
					additional_copy = Double.valueOf(additional.getText().toString()) * 10;
					total_fee = annual_rental / 250 * total_charge + additional_copy;
				} else {
					total_fee = annual_rental / 250 * total_charge;
				}

				total_agreement = Math.ceil(total_agreement);

				if (total_agreement % 2 != 0) {
					total_agreement = total_agreement + 1;
				}

				total_fee = Math.ceil(total_fee);

				if (total_fee % 2 != 0) {
					total_fee = total_fee + 1;
				}

				add_total = total_fee + total_agreement;

				txt_charge.setText("RM " + df.format(total_agreement));
				txt_stamp_duty.setText("RM " + df.format(total_fee));
				txt_total.setText("RM " + df.format(add_total));
			}
		});

		return rootView;
	}

}
