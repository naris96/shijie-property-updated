package com.magusapp.property;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.magusapp.property.utility.CommonUtilities;
import com.magusapp.property.utility.ServiceHandler;
import com.magusapp.property.utility.QuickstartPreferences;

import java.io.IOException;
import java.util.HashMap;

public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegistrationIntentService";

    public RegistrationIntentService() {
        super(TAG);
    }


    @SuppressLint("LongLogTag")
    @Override
    protected void onHandleIntent(Intent intent) {

        LocalBroadcastManager.getInstance(this)
                .sendBroadcast(new Intent(QuickstartPreferences.REGISTRATION_GENERATING));

        InstanceID instanceID = InstanceID.getInstance(this);
        String token = null;
        try {
            synchronized (TAG) {
                String default_senderId = CommonUtilities.SENDER_ID;
                String scope = GoogleCloudMessaging.INSTANCE_ID_SCOPE;
                token = instanceID.getToken(default_senderId, scope, null);
                sendRegistrationToServer(token);
                Log.i(TAG, "GCM Registration Token: " + token);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Intent registrationComplete = new Intent(QuickstartPreferences.REGISTRATION_COMPLETE);
        registrationComplete.putExtra("token", token);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void sendRegistrationToServer(String token) {
        ServiceHandler sh = new ServiceHandler();
        HashMap<String, String> params = new HashMap<>();
        params.put("type", "android");
        params.put("app_id", "15");
        params.put("user_token", token);

        sh.makeAppCall("POST", params);

    }
}