package com.magusapp.property;

import com.magusapp.property.utility.Alert;
import com.magusapp.property.utility.NumberTextWatcher;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Switch;

public class LoanFragment extends Fragment {

	EditText property_price, down_payment, mrta, lengthMortgage, annual;
	ImageButton calculate;
	Switch mySwitch;

	int count, property;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.activity_loan_fragment, container, false);

		getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		property_price = (EditText) rootView.findViewById(R.id.txt_property_price);
		down_payment = (EditText) rootView.findViewById(R.id.txt_down_payment);
		mrta = (EditText) rootView.findViewById(R.id.txt_mrta);
		lengthMortgage = (EditText) rootView.findViewById(R.id.txt_length);
		annual = (EditText) rootView.findViewById(R.id.txt_annual);
		calculate = (ImageButton) rootView.findViewById(R.id.calculate);
		mySwitch = (Switch) rootView.findViewById(R.id.switch1);

		property_price.addTextChangedListener(new NumberTextWatcher(property_price));
		down_payment.addTextChangedListener(new NumberTextWatcher(down_payment));
		mrta.addTextChangedListener(new NumberTextWatcher(mrta));

		// attach a listener to check for changes in state
		mySwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				property_price.setText(property_price.getText().toString().replaceAll(" ", "").trim());
				if (property_price.getText().toString().matches("")) {
					Alert.ShowAlertMessage("Please key in Property Price", "Information", null, null, getActivity(),
							getActivity());
					mySwitch.setChecked(false);
				} else {
					if (isChecked) {
						property = Integer.valueOf(property_price.getText().toString().replace(",", ""));
						count = property * 106 / 100;
						property_price.setText(String.valueOf(count));
					} else {
						property_price.setText(String.valueOf(property));
					}
				}

			}
		});

		onClick();

		return rootView;
	}

	public void onClick() {
		calculate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (validation()) {

					if (mrta.getText().toString().matches("")) {
						mrta.setText("0");
					}

					Intent i = new Intent(getActivity(), LoanResultActivity.class);
					i.putExtra("property_price", property_price.getText().toString().replace(",", ""));
					i.putExtra("down_payment", down_payment.getText().toString().replace(",", ""));
					i.putExtra("mrta", mrta.getText().toString().replace(",", ""));
					i.putExtra("length", lengthMortgage.getText().toString());
					i.putExtra("annual", annual.getText().toString());
					startActivity(i);
				}

			}
		});
	}

	public boolean validation() {
		String checkEmpty = "";
		property_price.setText(property_price.getText().toString().replaceAll(" ", "").trim());
		down_payment.setText(down_payment.getText().toString().replaceAll(" ", "").trim());
		lengthMortgage.setText(lengthMortgage.getText().toString().replaceAll(" ", "").trim());
		annual.setText(annual.getText().toString().replaceAll(" ", "").trim());

		if (property_price.getText().toString().matches(""))
			checkEmpty = "-Property Price Required\n";
		if (down_payment.getText().toString().matches(""))
			checkEmpty += "-Down Payment Required\n";
		if (lengthMortgage.getText().toString().matches(""))
			checkEmpty += "-Length of Mortgage Required\n";
		if (annual.getText().toString().matches(""))
			checkEmpty += "-Annual Interest Rate Required";

		if (!checkEmpty.matches("")) {
			Alert.ShowAlertMessage(checkEmpty, "Complete these fields", null, null, getActivity(), getActivity());
			return false;
		} else {
			return true;
		}
	}

}
